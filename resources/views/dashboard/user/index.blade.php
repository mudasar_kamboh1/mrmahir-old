
@extends('layouts.master')
@section('content')
    <style>
        #modalRegisterForm .modal-header {
            background: #0f3e7c !important;
            color: white !important;
        }
        #modalRegisterForm .modal-header span{
            position: relative;
            top: -12px;
            right: -7px;
            color: white;
        }
        .submitting{
            background: #0f3e7c !important;
            color: white !important;
            border: none !important;
            padding: 9px 27px;
        }
        .creating{
            background: #0f3e7c !important;
            color: white !important;
            border: none !important;
            padding: 9px 27px;
        }
        #example1 tbody th{
            background: #0f3e7c !important;
            color: white !important;
        }

    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <form action="{{route('users.index')}}" method="get" class="example"  style="max-width:300px;margin-left: 9px;margin-top: -15px;">
                            {{--@csrf--}}
                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        {{--<h1 class="m-0 text-dark">User</h1>--}}
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>

            </div><!-- /.container-fluid -->
        </div>
        </div>
        <!-- /.content-header -->

            {{--<<Serach function--}}
            <div class="col-md-4" style="padding-left: -90px">

            </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            {{--<!-- Submit form with a button -->--}}
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title w-100 font-weight-bold">Create User</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{route('users.store')}}" method="post">
                                                @csrf
                                            <div class="modal-body mx-3">
                                                <div class="md-form mb-5">
                                                    <i class="fas fa-user prefix grey-text"></i>
                                                    <input type="text" id="orangeForm-name" name="name" value="{{old('name')}}" class="form-control validate" style="float: left;" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                                                </div>
                                                <div class="md-form mb-5">
                                                    <i class="fas fa-envelope prefix grey-text"></i>
                                                    <input type="email" id="orangeForm-email" name="email"  value="{{old('email')}}" class="form-control validate" style="float: left;" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                                                </div>

                                                <div class="md-form mb-4">
                                                    <i class="fas fa-phone prefix grey-text"></i>
                                                    <input type="text" id="orangeForm-pass" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" name="mobile" value="{{old('mobile')}}"  class="form-control validate" style="float: left" required>
                                                    <label style="font-size: 14px;" data-error="wrong" data-success="right" for="orangeForm-pass">Your Phone</label>
                                                </div>
                                                <div class="md-form mb-4">
                                                    <i class="fas fa-lock prefix grey-text"></i>
                                                    <input type="password" id="orangeForm-pass" name="password" value="" class="form-control validate" style="float: left" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                                                </div>

                                                <div class="md-form mb-4">
                                                    <i class="fas fa-address-book prefix grey-text"></i>
                                                    <input type="text" id="orangeForm-pass" name="address" value="{{old('address')}}" class="form-control validate" style="float: left" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-pass">Your address</label>
                                                </div>
                                            </div>
                                            <div class="modal-footer d-flex justify-content-center">
                                                <button type="submit" class="btn btn-deep-orange btn-outline-success submitting">Submit</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <div class="text-center" style="margin-top: -10px">
                                    <a href="" style="float: right;padding: 7px 40px;" class="btn btn-outline-success btn-rounded mb-4 creating" data-toggle="modal" data-target="#modalRegisterForm">Create
                                    </a>
                                </div>

                                <tr role="row">
                                    <th class="sorting_asc custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">#</th>
                                    <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Name
                                    </th>
                                    <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Email
                                    </th>
                                    <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">phone
                                    </th>
                                    <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Address
                                    </th>
                                    <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Action
                                    </th>
                                </tr>

                                <tbody>
                                <?php
                                $x=1
                                ?>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="custom-card">{{$x++}}</td>
                                        {{--<td>{{$user->count()}}</td>--}}
                                        <td class="custom-card">{{$user->name}}</td>
                                        <td class="custom-card">{{$user->email}}</td>
                                        <td class="custom-card">{{$user->mobile}}</td>
                                        <td class="custom-card">{{$user->address}}</td>
                                        <td class="custom-card">
                                            <form action="{{route('users.edit',$user->id)}}" method="get">
                                                <button style="float: left; margin-right: 5px;" type="submit" class="btn btn-info js-sweetalert" title="edit"><i class="fa fa-edit"></i></button>
                                            </form>
                                            <form action="{{route('users.destroy',$user->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button  type="submit"  onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>


                @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{$users->links()}}
                @endif
            </div>

        </section>
    </div>

@endsection

@section('javascript')
@stop

