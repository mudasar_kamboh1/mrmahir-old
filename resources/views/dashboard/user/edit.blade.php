@extends('layouts.master')
@section('content')
<style>
    .user-text{
        font-weight: 600;
        background: #0f3e7c;
        color: white;
        padding: 13px 0;
        font-size: 30px !important;
    }
    .form-group{
        font-weight: 600;
    }
    .savings{
        display: block;
        margin: 0 auto;
        margin-top: 35px !important;
        background: #0f3e7c;
        color: white;
        font-size: 18px;
        border: none !important;
        padding: 9px 55px;
    }
    .savings:hover{
        background: #0f3e7c;
        color: white;
    }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="text-center user-text">User Edit</h1>
                    </div><!-- /.col -->

                </div>
            </div>
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div></div></div>
                <form action="{{route('users.update',$user->id)}}" method="post">
                    @csrf
                    @method('PUT')
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        Name:<input  style="height: 38px;width: 80%;" type="text" class="form-control"  value="{{$user->name}}" name="name" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                        Email:<input style="height: 38px;width: 80%;" type="email"  class="form-control" placeholder="name@email.com"  value="{{$user->email}}" name="email" required="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                        Phone:<input  style="height: 38px;width: 80%; " type="text" class="form-control" maxlength="11" minlength="11" name="mobile" value="{{$user->mobile}}" required>
                        </div>
                    </div>

                    <div class="col-md-4" style="padding: 15px 9px;">
                        <div class="form-group">
                        Address:<input style="height: 38px;width: 80%;" type="text"  class="form-control cstm-input"  name="address" value="{{$user->address}}" required>
                        </div>
                    </div>

                    <div class="col-md-4" style="padding: 15px 9px;">
                        <div class="form-group">
                        Password:<input style="height: 38px;   width: 80%;" type="password"  class="form-control cstm-input" minlength="6"  name="password" value="{{!is_null($user->staff)?$user->staff->card_id:''}}" >
                        </div>
                    </div>
                </div>
                <button style="display: block;margin: 0 auto;padding: 8px 50px;margin-top: 40px;" type="submit" class="btn btn-success js-sweetalert savings" title="edit">Save<i class=""></i></button>
                </form>
            </div>

        </section>
    </div>
@endsection

@section('javascript')

@stop