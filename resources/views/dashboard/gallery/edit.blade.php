<style>
    .form-control{
        padding: 0px !important;
        height: 33px !important;
    }
    .form-group label{
        font-weight: 600 !important;
    }
    .chosen-container-single .chosen-single{
        height: 33px !important;
        padding-top: 3px !important;
    }
</style>


{{-- Extends Layout --}}
@extends('layouts.master')

{{-- Page Title --}}
@section('page-title', 'Orders')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
    <div style="background-color: #fff;">
        <div class="table-responsive list-records">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background: #0f3e7c !important;color: white !important;padding: 9px 0;">
                    <h4 class="modal-title" style="font-size: 30px;font-weight: 600;display: block;margin: 0 auto;">Edit Gallery Data</h4>
                </div>
                <form action="{{ route('galleries.update',$post->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr"> Category :</label>
                                   <select name="category" id="category" class="form-control">
                            <option>Select</option>

                            @foreach(App\Category::all() as $options)
                                <option value="{{ $options->id }}"
                                        {{ (!is_null($post->sub_category) && !is_null($post->sub_category->category)) ? $post->sub_category->category->id == $options->id ? 'selected' : '' : ''}}>{{ $options->name }}</option>
                            @endforeach
                        </select>
                           </div>
                         </div>

                        <div class="col-md-6">
                             <div class="form-group">
                        <label for="usr">Sub-Category:</label>
                                 <select name="subCategory_id" id="subcategory" class="form-control">
                            @foreach(\App\SubCategory::all() as $options1)
                                <option value="{{ $options1->id }}"
                                        {{ !is_null($post->sub_category) ? $post->sub_category->id == $options1->id ? 'selected' : '' : ''}}>{{ $options1->name }}</option>
                            @endforeach
                        </select>
                              </div>
                        </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr">Title:</label>
                                    <input type="text" name="name" class="form-control" value="{{ $post->name }}">

                                </div>
                            </div>

                        <div class="col-md-6">
                                <div class="form-group">
                            <label for="usr">Meta Title:</label>
                            <input  value="{{ $post->meta_title }}" type="text" class="form-control" id="meta_title" name="meta_title" />
                                </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usr">Image :</label>
                                <input type="hidden" name="old_image" id="old_image" value="{{$post->image}}" />
                        <img src="{{asset('assets/uploads')}}{{ '/'.$post->image }}" alt="" style="width: 70px;height: 70px">
                        <input type="file" name="image" class="form-control">

                        @php
                            $post->images = explode(',',$post->images);
                        @endphp
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group" style="padding: 20px 0;">
                                <label for="usr">Images :</label>
                        @foreach($post->images as $image)
                            <div id="{{$image}}" style="display:inline">
                                <img src="{{asset('assets/uploads')}}{{ '/'.$image }}" class="images" alt="" data-src="{{$image}}" width="50px" height="50px">
                                <span style="color: red" onclick="removeImage($(this))" class="glyphicon glyphicon-remove"></span>
                            </div>
                        @endforeach
                        <input type="file" class="form-control" id="images" name="images[]" placeholder="address" multiple="">
                        <input type="hidden" id="new_image" name="edit_images" />
                        @php
                            $post->images = implode(',',$post->images);
                        @endphp
                        <input type="hidden" name="old_images" value="{{$post->images}}" />
                            </div>
                        </div>

                            <div class="col-md-6">
                        <div class="form-group" style="margin-top: -21px;">
                            <label for="usr">Youtube url:</label>
                            <input  value="{{ $post->url}}" type="text" class="form-control" id="url" name="url" />
                        </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="margin-top: -21px;">
                                    <label for="usr">Banner Number:</label>
                                    <input  value="{{ $post->sort}}" type="number" class="form-control" id="sort" name="sort" />
                                </div>
                            </div>

                            <div class="col-md-6">
                        <div class="form-group" style="margin-top: 1px;">
                            <label for="usr">Meta Description:</label>
                            <input  value="{{ $post->meta_description }}" type="text" class="form-control" id="meta_description" name="meta_description" />
                        </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr">Meta keywords:</label>
                                    <input value="{{ $post->meta_keywords }}" type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                                </div>
                            </div>


                            <div class="col-6">
                                <div class="form-group">
                                    <label for="usr">ALT:</label>
                                    <input type="text" class="form-control" id="alt" value="{{$post->alt}}" name="alt" required/>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr">Description :</label>
                                    <textarea id="textarea" type="message" name="description" class="form-control" >{{ $post->description }}</textarea>
                                </div>
                            </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <input style="display: block;margin: 0 auto;    background: #0f3e7c !important;color: white !important;border: none !important;" type="submit" class="btn btn-success" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
        </div>
    </div>

@endsection

@section('footer-extras')

    <script type="application/javascript">


        $(document).ready(function(){
            $('#textarea').summernote();
        });
        //----Show Sub Category

        //remove image
        function removeImage($this)
        {
            $this.parent().remove();
            var images = '';
            $.each($('.images'), function (i, val) {
                images += $(this).data('src')+',';
            });
            $('#new_image').val(images);

        }


        $('#category').on('change',function (e) {
            var cat_id = e.target.value;
            //Ajax
            $.ajax({
                url : "{{ url('admin/subCat') }}/"+cat_id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function (result) {
                    $('#subcategory').empty();
                    $.each(result.data, function (index, SubcatObj) {
                            $('#subcategory').append('<option value="' + SubcatObj.id + '">' + SubcatObj.name + '</option>');

                    });
                }
            });
        });
    </script>
@endsection
