{{-- Extends Layout --}}
@extends('layouts.master')

@section('content')
    <style>
        textarea {
            overflow: auto;
            resize: none !important;
            width: 100%;
        }
        .modal-footer{
            text-align: center !important;
            display: block !important;
        }
        .form-control{
            padding: 0px !important;
            height: 33px !important;
        }
        .chosen-default, .chosen-single{
            height: 33px !important;
            padding-top: 5px !important;
        }
        .text{
            font-weight: 600;
        }
        .modal-title{
            font-weight: bold !important;
            margin:0 auto !important;
            font-size: 30px !important;
        }
        .btn-success{
            padding: 5px 35px !important;
            background: #0f3e7c !important;
            color: white !important;
            border: none !important;
        }

        .table-responsive .modal-header{
            background: #0f3e7c !important;
            color: white !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
    <div style="background-color: #fff;margin: -39px 0;">
        <div class="row">
            <div class="col-md-4">
            </div>
        </div>
        <br>
        <div class="table-responsive list-records">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Insert Data</h4>
                </div>
                <div class="model-body">
                    <form action="{{ route('galleries.store')}}" method="POST" id="form-data" enctype="multipart/form-data" >
                        @csrf
                        <div class="modal-body">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="text">
                            Category :<select name="category" id="category" class="form-control">
                                <option>Select</option>
                                @foreach(App\Category::all() as $options)
                                    <option value="{{ $options->id }}">{{ $options->name }}</option>
                                @endforeach
                                    </select>
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="text">
                            Subcategory :<select name="subCategory_id" id="subcategory" class="form-control"></select>
                             </div>
                         </div>
                     </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text" style="padding: 16px 0;">
                                    Images :<input required type="file" class="form-control" name="images[]" placeholder="address" multiple>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text" style="padding: 16px 0;">
                            Post :<input type="file" name="image" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-3">
                                    <div class="text">
                                        Title :<input type="text" name="name" class="form-control">
                                    </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text">
                                            Banner-Number :<input type="number" name="sort" class="form-control">
                                        </div>
                                    </div>

                                <div class="col-md-6">
                                    <div class="form-group" style="margin-top: -8px;">
                                        <label for="usr">Meta keywords:</label>
                                        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="usr">Meta Title:</label>
                                <input type="text" class="form-control" id="meta_title" name="meta_title" />
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="usr">Meta Description:</label>
                                <input type="text" class="form-control" id="meta_description" name="meta_description" />
                            </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr">URL:</label>
                                    <input type="text" class="form-control" id="url" name="url" />
                                </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="usr">ALT:</label>
                                        <input type="text" class="form-control" id="alt" name="alt" required/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text" style="padding: 9px 0;">
                                        Description :<textarea type="text" class="form-control" id="textarea"  name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Insert">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('select[name="category"]').change(function (e) {
                var cat_id = $(this).val();
                //Ajax
                $.ajax({
                    url : "{{ url('admin/sub_categorie') }}/"+cat_id,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function (result) {
                        $('select[name="subCategory_id"]').empty();
                        $.each(result.data, function (i, value) {
                            $('select[name="subCategory_id"]').append('<option value="'+ value.id+'">'+value.name+'</option>');
                        });
                        $('select[name="subCategory_id"]').trigger("chosen:updated");
                    }

                });
            });
        });
    </script>
@endsection
