{{-- Extends Layout --}}
@extends('layouts.master')

@section('content')
    <style>
        .table thead{
            background: #0f3e7c !important;
            color: white !important;
        }
        th {
            white-space: nowrap;
        }
        .btn-info{
            background: #0f3e7c;
        }
        .btn-info:hover{
            background: #ce171f !important;
            color: #fff;
            border-color: #ce171f !important;
        }
    </style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('galleries.create') }}" type="button" class="btn btn-info btn-lg">Add New</a>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
<div style="background-color: #fff; padding: 15px 15px 0">
    <div class="row">
        <div class="col-md-4">
        </div>
    </div>
    <br>
    <div class="table-responsive list-records">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th scope="col">Image</th>
                <th scope="col">Images</th>
                <th scope="col" style="padding-right: 117px;">Sub Category | Category</th>
                <th scope="col">Name</th>
                <th scope="col">Image Number</th>
                <th scope="col">Description</th>
                <th scope="col">Meta Title</th>
                <th scope="col">Meta Description</th>
                <th scope="col">Links</th>
                <th scope="col">ALT</th>
                <th scope="col">Meta keywords</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
             <?php
                $x=1
             ?>
            @foreach($lists as $list)

                <tr>
                    <td>{{ $x++ }}</td>
                    <td><img alt="" src="{{asset('assets/uploads')}}{{ '/'.$list->image }}" style="width: 70px;height: 70px"></td>

                    @if($list->images)
                        @php
                            $list->images = explode(',',$list->images);
                        @endphp
                        <td>
                            @foreach($list->images as $image)
                                 <img alt="" src="{{asset('assets/uploads')}}{{ '/'.$image }}" width="30px" height="30px">
                            @endforeach
                        </td>
                    @endif

                    <td>
                        {{  !is_null($list->sub_category) ? $list->sub_category->name : ''}} | {{  !is_null($list->sub_category) && !is_null($list->sub_category->category) ? $list->sub_category->category->name : ''}}
                    </td>
                    <td>{{ $list->name }}</td>
                    <td>{{$list->sort}}</td>
                    <td>{!! \Illuminate\Support\Str::words($list->description, $words = 25, $end = '...') !!}</td>
                    <td>{{ $list->meta_title }}</td>
                    <td>{{ $list->meta_description }}</td>
                    <td>{{$list->url}}</td>
                    <td>{{$list->ALT}}</td>
                    <td>{{ $list->meta_keywords }}</td>
                    <td>
                        <div class="btn-group">
                            <form action="{{ route('galleries.show',$list->id) }}" method="get">
                                <input style="padding: 4px 20px;" type="submit" class="btn btn-primary btn-sm" value="Edit" ></input>
                            </form>
                            <form id="formDeleteModel_1" action="{{ route('galleries.destroy',$list->id) }}" method="POST" class="form-inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete"></input>
                            </form>
                        </div>
                        <!-- Delete Record Form -->
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

        </div>
    </div>


@endsection

@section('footer-extras')

    <script type="application/javascript">
        //Tiny Editor----Start

        $(document).ready(function(){
            $('#textarea').summernote();
        });
        //Tiny Editor----End
        //----Show Sub Category
        $('#category').on('change',function (e) {
            var cat_id = e.target.value;
            //Ajax
            $.ajax({
                url : "{{ url('admin/subCat') }}/"+cat_id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function (result) {
                    $('#subcategory').empty();
                    $.each(result.data, function (index, SubcatObj) {
//                        alert(SubcatObj.categories_id);
                        if(SubcatObj.category_id == cat_id)
                        {
                            $('#subcategory').append('<option value="'+ SubcatObj.id+'">'+SubcatObj.name+'</option>');
                        }
                    });
                }
            });
        });
    </script>
@endsection
