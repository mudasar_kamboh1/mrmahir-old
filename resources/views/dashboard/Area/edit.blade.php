@extends('layouts.master')
@section('content')
<style>
    #example1 .bg-clr{
        background: #0f3e7c;color: white;
    }
    .area-text{
        font-weight: 600;
        font-size: 30px !important;
    }
    .savings{
        padding: 9px 50px;
        background: #0f3e7c !important;
        color: white !important;
        border: none !important;
    }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="text-center area-text">Area Edit</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-body">

        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <!-- Submit form with a button -->
                            <tr role="row" class="bg-clr">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">Id</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Action
                                </th>
                            </tr>
                            <tbody>
                                <tr role="row" class="odd">
                                    <form action="{{route('areas.update',$area->id)}}" method="post">
                                        @csrf
                                        @method('PUT')
                                    <td class="sorting_1">{{$area->id}}</td>
                                    <td><input type="text" class="form-control" style="width: 160px" value="{{$area->name}}" name="name"></td>
                                    <td>
                                        <button style="    float: left; margin-right: 5px;" type="submit" class="btn btn-success js-sweetalert savings" title="edit">save<i class=""></i></button>
                                    </td>
                                    </form>
                                </tr>
                            </tbody>

                        </table>
                    </div></div>
            </div>

        </section>
    </div>
    <!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('javascript')

@stop