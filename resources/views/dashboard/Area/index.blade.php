@extends('layouts.master')
@section('content')
<style>
    #modalLoginForm .modal-header{
        background: #0f3e7c;
        color: white;
    }

    #modalLoginForm .modal-header span{
       color: white;
        position: relative;
        top: -12px;
        right: -3px;
    }

    .modal-footer button{
        background: #0f3e7c;
        color: white;
    }
    .modal-footer button:hover {
        background: #0f3e7c;
    }
    #example1 tbody th{
        background: #0f3e7c;
        color: white;
    }
</style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <form action="{{route('areas.index')}}" method="get" class="example"  style="margin:auto;max-width:300px;margin-left: 0px;margin-top: -15px;">
                            {{--@csrf--}}

                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
            </div>
        </div>

            <div class="col-md-4" style="padding-left: -90px">


            </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">

                            <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                 aria-hidden="true">

                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title w-100 font-weight-bold">Area Add</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>

                                        </div>


                                        <form action="{{route('areas.store')}}" method="post">
                                            @csrf
                                            <div class="modal-body mx-3">
                                                <div class="md-form mb-3" style="text-align: center;font-size: 15px;">
                                                    <i class="fas fa-area-chart prefix grey-text"></i>
                                                    <label data-error="wrong" data-success="right" for="defaultForm-email">Your Area</label>
                                                    <input type="text" name="name" id="defaultForm-email" class="form-control validate" style="width: 250px;
    display: block;
    margin: 0 auto;" required>
                                                </div>

                                            </div>
                                            <div class="modal-footer d-flex justify-content-center">
                                                <button class="btn btn-default">Submit</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="text-center" style="margin-top: -1px">

                            <a href="" style="float: right;border:none !important;padding: 7px 40px;background: #0f3e7c;color: white;" class="btn btn-outline-success btn-rounded mb-4  " data-toggle="modal" data-target="#modalLoginForm">Create
                                </a>
                            </div>

                            <tr role="row">
                                <th class="sorting_asc  custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                    aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">#</th>
                                <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Name
                                </th>
                                <th class="sorting  custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Action
                                </th>

                            </tr>

                            <tbody>
                            <?php
                            $x=1
                            ?>
                            @foreach( $area as  $areas)
                                <tr role="row" class="odd">
                                    <td class="sorting_1  custom-card">{{$x++}}</td>
                                    <td class=" custom-card">{{$areas->name}}</td>
                                    <td class=" custom-card">
                                        <form action="{{route('areas.edit',$areas->id)}}" method="get">
                                            <button style="    float: left; margin-right: 5px;" type="submit" class="btn btn-info js-sweetalert" title="edit"><i class="fa fa-edit"></i></button>
                                        </form>

                                        <form action="{{route('areas.destroy',$areas->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button  type="submit"  onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>

                                        </form>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table></div></div>
                {{$area->links()}}

            </div>

        </section>
    </div>
    </section>

    </div>

@endsection

@section('javascript')

@stop
