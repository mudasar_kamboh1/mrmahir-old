@extends('layouts.master')
@section('content')
<style>
    #example1 tr th{
        background: #0f3e7c;
        color: white;
    }

    #modalLoginForm .modal-header{
        background: #0f3e7c;
        color: white;
    }

    #modalLoginForm .modal-header .close{
        color: white;
        position: relative;
        top: -12px;
        right: -6px;
    }
</style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <div class="col-md-4" style="padding-left: -90px">
            <form action=""     method="get" class="example"  style="margin:auto;max-width:300px;margin-left: 9px;">
                <br>

            </form>

        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                {{--new form--}}
                                <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title w-100 font-weight-bold">Add Trending Services</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{route('trendingService.store')}}" method="post">
                                                @csrf

                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-sm-offset-2">
                                                            <div class="form-group" style="padding-left: 27px;">
                                                                <label for="staticEmail2" class="sr-only"></label>
                                                                <option style="font-weight: bold;">Service</option>
                                                                <select data-placeholder="Choose Services..." id="services" class="chosen-select" tabindex="2" name="service_id" required  data-height="40px" style="width: 87%;">
                                                                    <option value="">Select Services</option>
                                                                    @foreach($services as $service)
                                                                        <option value="{{ $service->id }}" {{ $service->title == app('request')->input('service') ? 'selected' : '' }}>{{ $service->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group" style="padding-left: 24px;">
                                                                <label for="inputPassword2" class="sr-only"></label>
                                                                <div class="form-group" style="padding-left: 27px;">
                                                                    <label for="staticEmail2" class="sr-only"></label>
                                                                    <option style="font-weight: bold;">Service</option>
                                                                    <select data-placeholder="Select Your area" class="chosen-select sub-services form-control cstm-control" name="subService_id" tabindex="2" id="sub_servics" required>
                                                                        <option value="">Select Sub Service</option>
                                                                        @if(count($sub_services))
                                                                            @foreach($sub_services as $sub_service)
                                                                                <option value="{{ $sub_service->id }}" {{ $sub_service->title == app('request')->input('sub_service') ? 'selected' : '' }}>{{ $sub_service->title }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="btn-submit" style="display: block;text-align: center;">
                                                                <a href="#"><button style="background: #0f3e7c;color: white;border: none;" type="submit" class="btn btn-primary mb-2">Create</button></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="text-center" style="margin-top: -39px">
                                    {{--<a href="" style="border: none !important;padding: 7px 40px;background: #0f3e7c;color: white;float: right;margin-bottom: 28px;position: relative;right: -190px;" class="btn btn-outline-success btn-rounded mb-4  " data-toggle="modal" data-target="#modalLoginForm">Create--}}
                                    <a href="" style="border: none !important;padding: 7px 40px;background: #0f3e7c;color: white;float: right;margin-bottom: 28px;" class="btn btn-outline-success btn-rounded mb-4  " data-toggle="modal" data-target="#modalLoginForm">Create
                                    </a>
                                </div>
                                {{--//<----------Data show form---->--}}

                                <tr role="row">
                                    <th class="sorting_asc custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">#</th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Trending Service
                                    </th>

                                    <th class="sorting custom-card"  style="width: 262.135px;margin-left: 64px;">Action
                                    </th>
                                </tr>

                                <tbody>
                                @php
                                $x=1
                                @endphp
                                @foreach($trendingServices->unique('subService_id') as $trendingService)
                                    <tr role="row" >
                                        <td class=" sorting_asc custom-card">{{$x++}}</td>
                                        <td class="custom-card">{{!is_null($trendingService->SubServices) ? $trendingService->SubServices->title:'N/A'}}</td>
                                        <td class="custom-card">
                                            <form action="{{route('trendingService.destroy',$trendingService->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button  style="margin-left: 64px;" type="submit"  onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </div>

                        </table>
                        @if($trendingServices instanceof \Illuminate\Pagination\LengthAwarePaginator )
                            {{$trendingServices->links()}}
                        @endif
                    </div>
                </div>
            </div>

        </section>
    </div><!--/. container-fluid -->
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script>
    $('#services').on('change',function () {
        var service = $(this).val();
        $('.sub-services option').remove();
        $('.sub-services').append('<option value="" disabled="disabled" selected>Select Sub Service</option>');
        $.ajax({
            type: 'GET',
            url: "{{ url('getSubServices') }}/" + service,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                $.each(result.data, function (i, row) {
                    $('.sub-services').append('<option value="'+row.id+'" class="list">'+row.title+'</option>');
                });
                $('.sub-services').trigger("chosen:updated");
            },
            error: function (request, status, error) {
                let json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    toastr.error(value);
                });
            }
        });
    });
</script>
@endsection
