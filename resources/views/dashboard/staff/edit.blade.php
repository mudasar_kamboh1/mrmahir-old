@extends('layouts.master')

@section('content')
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">

    <style>
        .datepicker > div {
            display: block;
            padding-top: 10px;
        }
        .chosen-container-multi .chosen-choices{
            height: 38px !important;
        }
        .chosen-container-single .chosen-single{
            height: 38px !important;
            padding-top: 7px;
        }
        .staff-text{
            font-weight: 600 !important;
            background: #0f3e7c !important;
            color: white !important;
            font-size: 30px !important;
            padding: 13px;
        }
        .saving{
            display: block;
            margin: 0 auto;
            margin-top: 35px !important;
            background: #0f3e7c;
            color: white;
            font-size: 18px;
            border: none !important;
            padding: 9px 55px;
        }
        .saving:hover{
            background: #0f3e7c;
            color: white;
        }
        .form-group label{
            font-weight: 600 !important;
        }
        .search-choice{
            height: 32px;
            padding-top: 8px !important;
            font-size: 14px !important;
        }
        .search-choice-close{
            margin-top: 6px !important;
        }
    </style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="text-center staff-text">Staff Edit</h1>
                    </div><!-- /.col -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container">
                    <div class="row">
                                <form action="{{route('staffs.update',$user->id)}}" method="post"  enctype="multipart/form-data" style="padding: 20px 0;">
                                    @csrf
                                    @method('PUT')

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Name:</label>
                                            <input  style="height: 38px;" type="text" class="form-control"  value="{{$user->name}}" name="name" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Email:</label>
                                            <input style="height: 38px;" type="email"  class="form-control" placeholder="name@email.com"  value="{{$user->email}}" name="email" required="">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">Phone:</label>
                                            <input  style="height: 38px;" type="text" class="form-control" maxlength="11" minlength="11" name="mobile" value="{{old('mobile',$user->mobile)}}" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name"> Address:</label>
                                           <input style="height: 38px;" type="text"  class="form-control cstm-input"  name="address" value="{{old('address',$user->address)}}" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name">CNIC Number:</label>
                                            <input style="height: 38px;"  id="currIN" type="text"  class="form-control cstm-input" maxlength="15" data-inputmask="'mask': '99999-9999999-9'"  placeholder="XXXXX-XXXXXXX-X" name="card_id" value="{{!is_null($user->staff)?$user->staff->card_id:''}}" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" style="font-weight: normal;padding-top: 10px;margin-bottom: 0;">CNIC Expire Date</label>
                                                <div class="input-group date" data-provide="datepicker">
                                                    <input type="text" class="form-control date-picker" style="height: 38px;"   data-date-format="dd MM yyyy" value="{{!is_null($user->staff) && $user->staff->expire_date ? Carbon\Carbon::parse(old('expire_date',$user->staff->expire_date))->format('m/d/Y'):''}}" name="expire_date"  placeholder="Select date Slot" autocomplete="off" required>
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group" style="margin-top: -6px;">
                                                <label for="name">Area:</label>
                                                <select name="areas[]" class="form-control " multiple required>
                                                    @php
                                                        $staffAreas = !is_null($user)? $user->staff->areas->pluck('id'):'----';
                                                    @endphp
                                                    @foreach($areas as $area)
                                                        <option  style="width: 600px " value="{{$area->id}}" {{ $staffAreas->contains($area->id) ?'selected':'---'}}>{{$area->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label style="font-weight: normal;margin-bottom: 0px;" for="name">Service:</label>
                                                <input class="form-control" type="hidden" name="order" value="" required>
                                                <select class="services form-control" id="services"  name="service_id">
                                                    <option value="">Select Service</option>
                                                    @foreach($services as $service)
                                                        <option value="{{ $service->id }}" {{ !is_null($user->subService)&& !is_null($user->subService->service) && $user->subService->service->id == $service->id ? 'selected' :''}}>{{$service->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="margin-bottom: -3px;" for="name">Sub Service:</label>
                                            <select data-placeholder="Select Your SubService"  multiple  name="sub_services[]" class="chosen-select form-control cstm-control" tabindex="2" id="sub_services" required>
                                                <option value="">Select Sub Service</option>
                                                @php
                                                    $staffService = !is_null($user->staff)&& !is_null($user->staff->subServices) ? $user->staff->subServices->pluck('id'):'---';
                                                @endphp
                                                @foreach($sub_services as $sub_service)
                                                    <option value="{{$sub_service->id}}"{{$staffService->contains($sub_service->id)?'selected':'--'}}>{{$sub_service->title}}</option>
                                                    <@endforeach
                                            </select>
                                        </div>
                                    </div>

                                        <div class="col-md-4" style="margin-top: -4px;">
                                            @php
                                                $staff->images = explode(',',$staff->images);
                                            @endphp
                                            <label for="name">CNIC Images :</label>
                                            @foreach($staff->images as $image)
                                                <div id="{{$image}}" style="display:inline">
                                                    @if($image)

                                                        <img alt="" src="{{asset('assets/staff')}}{{ '/'.$image }}" class="images" data-src="{{$image}}" width="50px" height="50px">
                                                        <span style="color: red" onclick="removeImage($(this))" class="glyphicon glyphicon-remove"></span>
                                                    @else
                                                    @endif
                                                </div>
                                            @endforeach
                                            <input style="height: 38px;padding: 5px 6px;font-size: 14px;" type="file"   name="images[]"   multiple class="form-control">
                                        </div>



                                    </div>
                                    <button style="display: block;margin: 0 auto;padding: 8px 50px;margin-top: 40px;" type="submit" class="btn btn-success js-sweetalert saving" title="edit">Save<i class=""></i></button>
                                </form>
                            </div>
                    </div>
            </section>
        </div>
    </div>

    <!-- /.content -->

    <!-- /.content-wrapper -->
@endsection


@section('javascript')
    <script type="text/javascript">

        $('#cnic').keydown(function(){
            //allow  backspace, tab, ctrl+A, escape, carriage return
            if (event.keyCode == 8 || event.keyCode == 9
                || event.keyCode == 27 || event.keyCode == 13
                || (event.keyCode == 65 && event.ctrlKey === true) )
                return;
            if((event.keyCode < 48 || event.keyCode > 57))
                event.preventDefault();

            var length = $(this).val().length;

            if(length == 5 || length == 13)
                $(this).val($(this).val()+'-');

        });

        $('#services').on('change',function (e) {
            var service = $(this).val();
            $('#sub_services option').not('option:selected').remove();
            $.ajax({
                type: 'GET',
                url: "{{ url('admin/get-Sub-Services') }}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        $('#sub_services').append('<option value="'+row.id+'">'+row.title+'</option>');
                    });
                    $('#sub_services').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

    </script>
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

@endsection
