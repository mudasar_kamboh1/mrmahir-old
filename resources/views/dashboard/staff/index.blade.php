@extends('layouts.master')

@section('content')
    <style>
        .custom-card-img{
            display: inline-flex;
            padding-bottom: 33px;
        }
        .staff-create{
            background: #0f3e7c;
            color: white;
            padding:20px 0;
        }
        #modalRegisterForm .modal-header span{
            position: relative;
            top: -19px;
            right: 7px;
            color: white;
        }
        #example1 tbody th{
            background: #0f3e7c;
            color: white;
        }
        .submitting{
            background: #0f3e7c !important;
            color: white !important;
            border: none !important;
            padding: 7px 35px;
        }
        .creating{
            float: right;
            padding: 7px 35px;
            background: #0f3e7c !important;
            color: white !important;
            border: none !important;
        }
        .modal-content{
         padding: inherit !important;
        }
        .modal-content button{
            position: inherit !important;
            right: inherit !important;
        }
        #view-detail-modal .modal-header{
            background: #0f3e7c !important;
            color: white !important;
        }
        .cross{
            position: relative;
            top:12px;
            right: -7px;
            color: white;
        }

    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <form action="{{route('staffs.index')}}" method="get" class="example"  style="max-width:300px;margin-left: 9px;margin-top: -15px;">
                            {{--@csrf--}}
                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                            <div class="panel-body">
                            </div>
                        </form>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
        </div>
    </div>

        <div class="col-md-4" style="padding-left: -90px">

        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <!-- Trigger the modal with a button -->
                            <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center staff-create">
                                            <h4 class="modal-title w-100 font-weight-bold">Create Staff</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{route('staffs.store')}}" method="post">
                                            @csrf
                                            @method('post')
                                            <div class="modal-body mx-3">
                                                <div class="md-form mb-5">
                                                    <i class="fas fa-user prefix grey-text"></i>
                                                    <input type="text" id="orangeForm-name" name="name" value="{{old('name')}}" class="form-control validate" style="float: left;height: 29px;" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                                                </div>
                                                <div class="md-form mb-5">
                                                    <i class="fas fa-envelope prefix grey-text"></i>
                                                    <input type="email" id="orangeForm-email" name="email" value="{{old('email')}}" class="form-control validate" style="float: left;height: 29px;" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                                                </div>

                                                <div class="md-form mb-4">
                                                    <i class="fas fa-phone prefix grey-text"></i>
                                                    <input type="number" id="orangeForm-pass"  name="mobile"  value="{{old('mobile')}}" minlength="11" maxlength="11" onkeypress="if(this.value.length==11) return false;"  class="form-control validate" style="float: left;height: 29px;margin-bottom: 13px;" required>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-pass">Your Phone</label>
                                                </div>

                                                <div class="md-form mb-4">
                                                    <i class="fas fa-address-book prefix grey-text"></i>
                                                    <input type="text" id="orangeForm-pass" name="address" value="{{old('address')}}" class="form-control validate" style="float: left;height: 29px;margin-bottom: 13px;" required>
                                                    <label style="font-size: 14px;" data-error="wrong" data-success="right" for="orangeForm-pass">Your address</label>
                                                </div>

                                                <div class="md-form mb-4">
                                                    <i class="fas fa-area-chart prefix grey-text"></i>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-pass">Areas</label>
                                                    <select name="areas[]" multiple data-placeholder="Select Your area" style="width: 100%;">
                                                        <option value="">Select Area</option>
                                                        @foreach($areas as $area)
                                                            <option value="{{ $area->id }}">{{ $area->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="md-form mb-4">
                                                    <i class="fab fa-servicestack"></i>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-pass">Service</label>
                                                    <select data-placeholder="Select Your Service" class="chosen-select form-control cstm-control" tabindex="2"   id="services" required>

                                                        @foreach($services as $service)
                                                            <option value="{{ $service->id }}">{{ $service->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="md-form mb-4">
                                                    <i class="fab fa-servicestack"></i>
                                                    <label style="font-size: 14px;"  data-error="wrong" data-success="right" for="orangeForm-pass">Sub Service</label>
                                                    <select data-placeholder="Select Your SubService" class="chosen-select sub-services form-control cstm-control sub-services" name="subservice[]"   id="sub_service_id" tabindex="2"  multiple required>
                                                        <option value="">Select Sub Service</option>
                                                    </select>
                                                </div>
                                                <div class="md-form mb-4">
                                                    <i class="fas fa-lock prefix grey-text"></i>
                                                    <input type="password" id="orangeForm-pass" name="password" class="form-control validate" style="float: left;height: 29px;" required>
                                                    <label style="font-size: 14px;" data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                                                </div>
                                            </div>
                                            <div class="modal-footer d-flex justify-content-center ">
                                                <button  type="submit" class="btn btn-deep-orange btn-outline-success submitting">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center" style="margin-top: -1px;">
                                <a href="" class="btn btn-outline-success btn-rounded mb-4 creating" data-toggle="modal" data-target="#modalRegisterForm">Create
                                </a>
                            </div>
                    <tr role="row">
                        <th class="sorting_asc custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                            aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">#</th>
                        <th class="sorting custom-card"  style="width: 262.135px;">Name
                        </th>
                        <th class="sorting custom-card"  style="width: 262.135px;">Email
                        </th>
                        <th class="sorting custom-card"  style="width: 262.135px;">phone
                        </th>
                        <th class="sorting custom-card"  style="width: 262.135px;">Address
                        </th>
                        <th class="sorting custom-card"  style="width: 262.135px;">ALT
                        </th>
                        <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Action
                        </th>
                    </tr>
                    <tbody>
                    <?php
                    $x=1
                    ?>
                    @foreach($users as $user)
                            <tr role="row" class="odd">
                                <td class="custom-card">{{$x++}}</td>
                                <td class="custom-card">{{ $user->name}}</td>
                                <td class="custom-card">{{ $user->email }}</td>
                                <td class="custom-card">{{ $user->mobile}}</td>
                                <td class="custom-card">{{ $user->address}}</td>
                                <td class="custom-card">{{ $user->ALT}}</td>
                            <td class="custom-card">
                                <form action="{{route('staffs.edit',$user->id)}}" method="get">
                                    <button style="float: left; margin-right: 5px;" type="submit" class="btn btn-info js-sweetalert" title="edit"><i class="fa fa-edit"></i></button>
                                </form>

                                <form action="{{route('staffs.destroy',$user->id)}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button  type="submit" onclick="return confirm('Are you sure')"  class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                </form>
                                    <button style="position: absolute;width: 35px;margin-left: 90px;margin-top: -37px;" class="btn btn-danger js-sweetalert staff-detail"

                                            data-staff-id="{{$user->id}}" title="Staff Detail"><i class="fa fa-info"></i> </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                        </table>
                </div>
                    <div class="modal fade" id="view-detail-modal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title" style="font-weight: 600;font-size:30px;margin: 0 auto;position: relative;right: -93px;">Staff Detail</h4>
                                    <button type="button" class="close cross" data-dismiss="modal">&times;</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body" id="order-detail-content">
                                </div>
                                <!-- Modal footer -->
                            </div>
                        </div>
                        <!-- /.content -->
                    </div>
            </div>

                @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{$users->links ()}}
                @endif
            </div>

    </section>
    </div>


@endsection

        @section('javascript')
            <script type="text/javascript">


                $('#services').on('change',function () {
                    var service = $(this).val();
//                    $('.sub-services option').remove();
                    $('#sub-services option').not('option:selected').remove();

//                    $('.sub-services').trigger("chosen:updated");
                    $('.sub-services').append('<option value="" disabled="disabled" selected>Select Sub Service</option>');
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('admin/get-Sub-Services') }}/" + service,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (result) {
                            $.each(result.data, function (i, row) {
                                $('.sub-services').append('<option value="'+row.id+'" class="list">'+row.title+'</option>');
                            });
                            $('.sub-services').trigger("chosen:updated");
                        },
                        error: function (request, status, error) {
                            let json = $.parseJSON(request.responseText);
                            $.each(json.errors, function(key, value){
                                toastr.error(value);
                            });
                        }
                    });
                });

                $('.staff-detail').click(function () {
                    var staffID = $(this).data('staff-id');

                    $.ajax({
                        url: "{{url('admin/staff-detail')}}/"+ staffID,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr++},
                        success: function(result) {
                            if (result != '') {
                                $('#order-detail-content').html(result);
                                $('#view-detail-modal').modal('show');
//
                            } else {
                                alert('error');
                            }
                        }
                    });
                });
            </script>

@endsection
