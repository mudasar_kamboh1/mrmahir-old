<style>
    .modal-content button{
        position: absolute;
        right: 15px;
    }
    .modal-content{
        padding: 25px 0;
    }

    #projectinput8{
        padding: 11px 0;
    }
    .reject{
        background: #0f3e7c !important;
        color: white !important;
        border: none !important;
    }
</style>
<form>
    <div class="new-measurements">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Name:</label><br>
                    <input class="form-control" type="text" name="name" value="{{ !is_null($staff->user) && $staff->user->name? $staff->user->name : '-- -- --'}}"readonly>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Email:</label><br>
                    <input class="form-control" type="text" name="email" value="{{ !is_null($staff->user) && $staff->user->email ? $staff->user->email : '-- -- --'}}"readonly>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Verified Mobile:</label><br>
                    <input class="form-control" type="text" name="name" value=" {{ !is_null($staff->user)? $staff->user->mobile : ''}}" readonly>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Address:</label><br>
                    <input class="form-control" type="text" name="address" value=" {{ !is_null($staff->user) && $staff->user->address ? $staff->user->address : '' }}" readonly >
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">CNIC:</label><br>
                    <input class="form-control" type="text" name="card_id" value="{{ !is_null($staff) && ($staff->card_id) ? $staff->card_id : '' }}" readonly>
                   </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">CNIC Expire:</label><br>
                    <input class="form-control" type="text" name="mobile" value="{{ !is_null($staff) && ($staff->expire_date) ? $staff->expire_date : '' }}" readonly>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Area:</label><br>
                    <input class="form-control" type="text" name="address" value="{{str_replace(array('[',']','"'),'',$user->staff->areas->pluck('name'))}}" readonly >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group">
                        <label for="name">Sub Service:</label>
                        <input class="form-control" type="text" name="address" value="{{str_replace(array('[',']','"'),'',$user->staff->subServices->pluck('title'))}}
                                " readonly >
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Created at:</label><br>
                    <input class="form-control" type="text"  value="{{ $staff->created_at ? Carbon\Carbon::parse($staff->created_at)->format('h:s A Y-m-d') : '-- -- --'}}" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">CNIC Image:</label><br>
                    <label id="projectinput8" class="file center-block">
                        @php
                            $staff->images = explode(',',$staff->images);
                        @endphp
                        @foreach($staff->images as $image)
                            <div id="{{$image}}" style="display:inline">
                                @if($image)
                                    <img alt="" src="{{asset('assets/staff')}}{{ '/'.$image }}" class="images" data-src="{{$image}}" width="50px" height="50px">
                                @else
                                @endif
                            </div>
                        @endforeach
                    </label>
                </div>
            </div>
    </div>
    </div>
    <div class="modal-footer text-center" style="display: block;">
        <button type="button" class="btn btn-danger btn-close reject" data-dismiss="modal">Cancel</button>
    </div>
</form>

