@extends('layouts.master')
@section('content')
    <style>
        #example1 thead th{
            background: #0f3e7c !important;
            color: white !important;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <form action="{{route('quotes.index')}}" method="get" class="example"  style="max-width:300px;margin-left: 9px;margin-bottom: 1rem;margin-top: -15px;">
                            {{--@csrf--}}

                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>

                            <div class="panel-body">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                            </div>

                        </form>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.content-header -->


        <div class="col-md-4" style="padding-left: -90px">


        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row"><div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc  custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">Id</th>
                                <th class="sorting  custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Name</th>
                                <th class="sorting  custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 183.247px;">mobile</th>
                                <th class="sorting  custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 183.247px;">Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $x=1
                            ?>
                            @foreach( $quotes as  $quote)
                                <tr role="row" class="odd">
                                    <td class="sorting_1  custom-card">{{$x++}}</td>
                                    <td class=" custom-card">{{! is_null($quote->name) ? $quote->name:'N/A'}}</td>
                                    <td class=" custom-card">{{! is_null($quote->mobile) ? $quote->mobile:'N/A'}}</td>
                                    <td class=" custom-card">
                                        <form  action="{{route('quotes.destroy',$quote->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button  type="submit" onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>

                                        </form>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>
                        @if($quotes instanceof \Illuminate\Pagination\LengthAwarePaginator )
                            {{$quotes->links()}}
                        @endif
                    </div></div>

            </div>
        </section>
    </div><!--/. container-fluid -->
    </section>

        <!-- /.content -->



@endsection

@section('javascript')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8">

        $("#deleteRecord").click(function(){
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");

           $.ajax(
                {
                    url: "quotes/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                        "_token": token,
                    },

                    success: function (){
                        console.log("it Works");
                    }
                });
        });
    </script>

@endsection