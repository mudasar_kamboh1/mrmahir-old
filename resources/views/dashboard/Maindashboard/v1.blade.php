@extends('layouts.master')
@section('content')
<style>
    .btn-info:hover {
        background: #ce171f !important;
        color: #fff;
        border-color: #ce171f !important;
    }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6">
                        {{--<a href="" type="button" class="btn btn-info btn-lg">View Statistics</a>--}}
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-3">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>New Orders</h3>
                                <div class="pehra">
                                    <span style="font-size: 17px;">{{$order->count()}}</span>
                                    <span style="text-align: center;padding: 1px 24px;">Last week<br><i style="color: white;" class="green">{{$order_week->count()}}</i></span>
                                    <span style="text-align: center;">Today<br><i style="color: white;" class="green">{{$order_today->count()}}% </i></span>
                                </div>
                            </div>
                            <div class="icon" style="color: white;">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{url('admin/orders')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-3">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>Staff Registrations <sup style="font-size: 20px"></sup></h3>
                                <div class="pehra">
                                    <span style="font-size: 17px;">{{$staff->count()}}</span>
                                    <span style="text-align: center;padding: 1px 24px;">Last week<br><i style="color: white;" class="green">{{$staff_week->count()}}</i></span>
                                    <span style="text-align: center;">Today<br><i style="color: white;" class="green">{{$staff_today->count()}}% </i></span>
                                </div>
                            </div>
                            <div class="icon" style="color: white;">
                                <i class="ion ion-stats-bars"></i>
                            </div>

                            <a href="{{url('admin/staffs')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-3">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3 style="color: white;">User Registrations</h3>
                                <div class="pehra">
                                    <span style="font-size: 17px;">{{$user->count()}}</span>
                                    <span style="text-align: center;padding: 1px 24px;">Last week<br><i style="color: white;" class="green">{{$user1->count()}}</i></span>
                                    <span style="text-align: center;">Today<br><i style="color: white;" class="green">{{$user_today->count()}}% </i></span>
                                </div>
                            </div>
                            <div class="icon" style="color: white;">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{url('admin/users')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    {{--<div class="col-4">--}}
                    {{--<div class="col-3">--}}
                        {{--<!-- small box -->--}}
                        {{--<div class="small-box bg-traffic">--}}
                            {{--<div class="inner">--}}
                                {{--<h3 style="color: white;">Traffic</h3>--}}
                                {{--<div class="pehra">--}}
                                    {{--<span style="font-size: 17px;">{{$order->sum('order_price')}}</span>--}}
                                    {{--<span style="text-align: center;padding: 1px 24px;">Last week<br><i style="color: white;" class="green">{{$order_week->sum('order_price')}}</i></span>--}}
                                    {{--<span style="text-align: center;">Today<br><i style="color: white;" class="green">{{$order_today->sum('order_price')}}% </i></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="icon" style="color: white;">--}}
                                {{--<i class="fas fa-traffic-light"></i>--}}
                            {{--</div>--}}
                            {{--<a href="{{url('admin/orders')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-3">
                        <div class="small-box bg-income">
                            <div class="inner">
                                <h3 style="color: white;">Total Income</h3>
                                <div class="pehra">
                                    <span style="font-size: 17px;">{{$order->sum('order_price')}}</span>
                                    <span style="text-align: center;padding: 1px 24px;">Last week<br><i style="color: white;" class="green">{{$order_week->sum('order_price')}}</i></span>
                                    <span style="text-align: center;">Today<br><i style="color: white;" class="green">{{$order_today->sum('order_price')}}% </i></span>
                                </div>
                            </div>
                            <div class="icon" style="color: white;">
                                <i class="fas fa-coins"></i>
                            </div>
                            <a href="{{url('admin/orders')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="graph-btn">
                    <a href="{{url('admin/userChart')}}" type="button" class="btn btn-info btn-lg">View Statistics</a>
                        </div>
                    </div>
                </div>


            </div>
        </section>
    </div>

@endsection

@section('javascript')

    <script src="{{asset('assets/css/plugins/jquery/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('assets/css/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- ChartJS -->
    <script src="{{asset('assets/css/plugins/chart.js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('assets/css/plugins/sparklines/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('assets/css/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('assets/css/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('assets/css/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('assets/css/plugins/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/css/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('assets/css/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('assets/css/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('assets/css/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
@stop