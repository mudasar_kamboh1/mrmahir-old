@extends('layouts.app')
{{--@include('layouts.sidebar')--}}
@section('content')
    @include('layouts.sidebar')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">

                </div>
                <div class="col-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var record ={!! json_encode($social_users) !!};

//            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'month');
            data.addColumn('number', 'Total User');
            for(var i = 0; i < record.length; i++){
                if( record[i]['month'] != null){
                    var v = record[i]['value'];
                    var n = record[i]['month'];
                    data.addRow([n,v]);
                }
            }
            var options = {
                title: 'Monthly Activities User',
                is3D: true,
                colors: ['#1d2939'],
                bar: { groupWidth: "40%"}
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('piechart_3d'));
            chart.draw(data, options);

        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            // Define the chart to be drawn.
//            var data = new google.visualization.DataTable();

            var record ={!! json_encode($staff) !!};

            {{--var record2 ={!! json_encode($staff) !!};--}}
            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'month');
            data.addColumn('number', 'Total Staff');

            for(var i=0; i<record.length;i++){
                if(record[i]['month']!=null){
                    var v = record[i]['value'];
                    var n = record[i]['month'];
                    data.addRow([n,v]);
//                console.log(v);
                }
            }
            var options = {
                title: 'Monthly Activities Staff',
                is3D: true,
                colors: ['#d9534f'],

                bar: { groupWidth: "40%"}
            };

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('myPieChart'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            // Define the chart to be drawn.
//            var data = new google.visualization.DataTable();

            var record ={!! json_encode($order) !!};
            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'month');
            data.addColumn('number', 'Total order');

            for(var i=0; i<record.length;i++){
                if(record[i]['month']!=null){
                    var v = record[i]['value'];
                    var n = record[i]['month'];
                    data.addRow([n,v]);
//                console.log(v);
                }
            }
            var options = {
                title: 'Monthly Activities Order',
                is3D: true,
                colors: ['#ab8f3c'],
                bar: { groupWidth: "40%"}
            };

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('myPieChart1'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            // Define the chart to be drawn.
//            var data = new google.visualization.DataTable();

            var record = {!! json_encode($facebook) !!};
            {{--var record2 ={!! json_encode($staff) !!};--}}
            //            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'created_at');
            data.addColumn('number', 'Total facebook Reviews');


            for(var i=0; i<record.length;i++){
                if(record[i]['month']!=null){
                    var v = record[i]['value'];
                    var n = record[i]['month'];
                    data.addRow([n,v]);
//                console.log(v);
                }
            }
            var options = {
                title: 'Monthly Activities Facebook Review',
                is3D: true,
                bar: { groupWidth: "10%"}
            };

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('myPieChart2'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            // Define the chart to be drawn.
//            var data = new google.visualization.DataTable();

            var record ={!! json_encode($order_price) !!};
            {{--var record2 ={!! json_encode($staff) !!};--}}
            //            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'name');
            data.addColumn('number', 'Total  Income');


            for(var i=0; i<record.length;i++){
                if(record[i]['month']!=null){
                    var v = record[i]['value'];
                    var n = record[i]['month'];
                    data.addRow([n,v]);
//                console.log(v);
                }
            }
            var options = {
                title: 'Monthly Activities Income',
                is3D: true,
                colors: ['#2e6ba0'],

                bar: { groupWidth: "40%"}
            };

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('myPieChart3'));
            chart.draw(data, options);
        }
    </script>

    <div id="piechart_3d" style="margin-left: 347px;width: 900px; height: 500px;"></div>
    <div id="myPieChart" style="margin-left: 347px;width: 900px; height: 500px;"></div>
    <div id="myPieChart1" style="margin-left: 347px;width: 900px; height: 500px;"></div>
    <div id="myPieChart2" style="margin-left: 347px;width: 900px; height: 500px;"></div>
    <div id="myPieChart3" style="margin-left: 347px;width: 900px; height: 500px;"></div>

@endsection
