@extends('layouts.master')
@section('content')
<style>
    #modalLoginForm .modal-header{
       background: #0f3e7c;color: white;
    }
    #modalLoginForm span{
        color: white;
        position: relative;
        top: -13px;
        right: -6px;
    }

    #newTable tr th{
        background: #0f3e7c;
        color: white;
    }

</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <form action="{{route('services.index')}}" method="get" class="example"  style="max-width:300px;margin-top: -15px;margin-left: 8px;">
                            {{--@csrf--}}
                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button >
                            <div class="panel-body">
                            </div>
                        </form>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    {{--<!-- /.col -->--}}
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <div class="col-md-4" style="padding-left: -90px">


        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row"><div class="col-sm-12">
                        <table id="newTable" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                       {{--new form--}}
                                <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title w-100 font-weight-bold">Create Services</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                                <div class="modal-body mx-3">
                                                    <form action="{{route('services.store')}}" method="post" enctype="multipart/form-data">
                                                        @csrf

                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <i class="fab fa-servicestack"></i>
                                                            <label data-error="wrong" data-success="right" for="defaultForm-email" > Service</label><br>
                                                            <input type="text" name="title" id="defaultForm-email"  value="{{old('title')}}" class="form-control validate" style="display: block;margin: 0 auto;" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="usr">Icon ALT:</label>
                                                            <input type="text" class="form-control" id="alt" name="icon_alt" required/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <i class="fas fa-image"></i>
                                                            <label data-error="wrong" data-success="right" for="defaultForm-email"> Icon</label><br>
                                                            <input type="file" class="defaultForm-email" name="icon"  accept=".png, .jpg, .jpeg" style="font-size: 14px;" required>
                                                        </div>
                                                    </div>


                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <i class="fas fa-images"></i>
                                                            <label data-error="wrong" data-success="right" for="defaultForm-email"> Image</label><br>
                                                            <input type="file"  name="image"  class="" style="position: relative;;font-size: 14px;" accept=".png, .jpg, .jpeg" required>
                                                        </div>
                                                    </div>

                                                </div>
                                                        <div class="row">
                                                            <div class="col-12">

                                                                <div class="form-group">
                                                                    <label for="usr">Image ALT:</label>
                                                                    <input type="text" class="form-control" id="alt" name="image_alt" required/>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label for="usr">Meta Title:</label>
                                                                    <input type="text" class="form-control" id="alt" name="meta_title" required/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <i class="fab fa-servicestack"></i>
                                                                    <label data-error="wrong" data-success="right" for="defaultForm-email" > Meta Description:</label><br>
                                                                    <textarea class="form-control" rows = "5" cols = "50" name ="meta_description" placeholder="Enter your Description" style="resize: none;height:100px;display: block;margin: 0 auto;" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                 <div class="row">
                                                     <div class="col-12">
                                                         <div class="form-group">
                                                             <i class="fab fa-servicestack"></i>
                                                             <label data-error="wrong" data-success="right" for="defaultForm-email" > Description</label><br>

                                                             <textarea class="form-control" rows = "5" cols = "50" name = "description" placeholder="Enter your Description" style="resize: none;height:100px;display: block;margin: 0 auto;" required></textarea>
                                                         </div>
                                                     </div>
                                                 </div>

                                                 <div class="modal-footer d-flex justify-content-center">
                                                      <button style="padding: 5px 33px;background: #0f3e7c;color: white;" class="btn btn-default">Submit</button>
                                                 </div>
                                            </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="text-center" style="margin-top: -39px">
                                    <a href="" style="float: right;margin-bottom: 28px;margin-top: 9px;border: none !important;padding: 7px 40px;background: #0f3e7c;color: white;" class="btn btn-outline-success btn-rounded mb-4  " data-toggle="modal" data-target="#modalLoginForm">Create
                                       </a>
                                </div>
                            {{--//<----------Data show form---->--}}

                            <tr role="row">
                                <th class="sorting  custom-card"  style="width: 262.135px;">Name
                                </th>
                                <th class="sorting  custom-card"  style="width: 262.135px;">Description
                                </th>
                                <th class="sorting  custom-card"  style="width: 262.135px;">Image
                                </th>
                                <th class="sorting  custom-card"  style="width: 262.135px;">Icon
                                </th>
                                <th class="sorting  custom-card"  style="width: 262.135px;">Image ALT
                                </th>
                                <th class="sorting  custom-card"  style="width: 262.135px;">Icon ALT
                                </th>
                                <th class="sorting  custom-card"  style="width: 262.135px;">Action
                                </th>
                            </tr>

                            <tbody>
                            @foreach($services as $service)
                                <tr role="row" style="counter-increment:auto" class="odd">
                                    {{--<td class=" custom-card" style=""></td>--}}
                                    <td class=" custom-card">{{ $service->title}}</td>
                                    <td class="custom-card">
                                               {{$service->description}}
                                    </td>
                                    <td class=" custom-card">
                                        @if($service->image)
                                            <img src="{{asset('serviceImg/'.$service->image)}}"  alt="{{$service->title}}" class="step1:after" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;">
                                        @else
                                            <img alt="" src="{{asset('serviceImg/user.jpg')}}">
                                        @endif
                                    </td>
                                    <td class=" custom-card">
                                        @if($service->icon)
                                        <img alt="" src="{{ asset('serviceIcon/'.$service->icon)}}" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;background-color: #065;padding: 7px;border-radius: 50%;">
                                        @else
                                        <img alt="" src="{{ asset('serviceIcon/user.jpg')}}" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;" >
                                        @endif
                                    </td>
                                    <td class="custom-card">
                                        {{$service->image_alt}}
                                    </td>
                                    <td class="custom-card">
                                        {{$service->icon_alt}}
                                    </td>

                                    <td class=" custom-card">
                                        <form action="{{route('services.edit',$service->id)}}" method="get">
                                            <button style="    float: left; margin-right: 5px;" type="submit" class="btn btn-info js-sweetalert" title="edit"><i class="fa fa-edit"></i></button>
                                        </form>

                                        <form action="{{route('services.destroy',$service->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button  type="submit" onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </form>

                                    </td>

                                </tr>

                                @endforeach
                            </tbody>


                            </div>
                        </table>
                    </div></div>
                {{$services->links()}}


            </div>

        </section>
    </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('javascript')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@8">
        $(".deleteRecord").click(function(){
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");

            $.ajax(
                {
                    url: "users/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                        "_token": token,
                    },
                    success: function (){
                        console.log("it Works");
                    }
                });
        });
        function getit(){
            $('#newTable').find('tr').each(function(index){
                var x= this.setAttribute("id","Row"+[index]);
                console.log(x);
            })
        }
    </script>

@stop
