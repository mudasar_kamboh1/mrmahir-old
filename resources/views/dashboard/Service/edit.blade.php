@extends('layouts.master')
@section('content')
<style>
    .service-edit{
        font-weight:600;
        background: #0f3e7c !important;
        color: white !important;
        padding: 13px 0;
    }
    #projectinput1{
        width: 85% !important;
    }
</style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="text-center service-edit">Service Edit</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">

        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    </div>
                </div>
                <div class="row">
                    <form action="{{route('services.update',$service->id)}}" method="post" enctype="multipart/form-data" style="    padding: 20px 131px;margin-right: -107px;">
                        @csrf
                        @method('PUT')
                        <div class="form-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group" style="margin: 0 2px;">
                                        <label for="projectinput1"> Name:</label>
                                        <input type="text" id="projectinput1" class="form-control" value="{{old('title',$service->title)}}" placeholder="First Name" name="title">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="usr">Image ALT:</label>
                                        <input style="height: 40px;width: 85%;" type="text" class="form-control" id="alt" name="image_alt"  value="{{$service->image_alt}}" required/>

                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="usr">Icon ALT:</label>
                                        <input style="height: 40px;width: 85%;" type="text" class="form-control" id="alt" name="icon_alt" value="{{$service->icon_alt}}" required/>

                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label style="padding-top: 2px;" for="projectinput9">Description:</label>
                                            <textarea id="projectinput9" rows="1" cols="5" class="form-control" name="description" placeholder=""  style="width: 85%; resize: none;">{{$service->description}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="usr">Meta Title:</label>
                                            <input style="height: 40px;width: 85%;" type="text" class="form-control"  name="meta_title"  value="{{$service->meta_title}}" required/>
                                        </div>
                                    </div>


                                <div class="col-6">
                                    <div class="form-group">
                                        <label style="padding-top: 2px;" for="projectinput9">Meta Description:</label>
                                        <textarea id="projectinput9" rows="1" cols="5" class="form-control" name="meta_description" placeholder=""  style="width: 85%; resize: none;">{{$service->meta_description}}</textarea>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group" style="padding-top: 58px;">
                                        <label>Select Image:</label>
                                        <label id="projectinput8" class="file center-block">
                                            @if($service->image)
                                                <img alt="" src="{{asset('serviceImg/'.$service->image)}}" class="step1:after" style="height: 55px;width: 55px;display: block;margin: 0 auto;margin-top: -50px;">
                                            @else
                                                <img alt="" src="{{asset('serviceImg/user.jpg')}}" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;">
                                            @endif
                                            <input type="file"  name="image">
                                            <span class="file-custom"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-6">
                                     <div class="form-group" style="padding-top: 58px;">
                                         <label>Select Icon:</label>
                                            <label id="projectinput8" class="file center-block">
                                                @if($service->icon)
                                                    <img alt="" src="{{asset('serviceIcon/'.$service->icon)}}" class="step1:after" style="height: 55px;width: 55px;display: block;margin: 0 auto;margin-top: -50px;">
                                                @else
                                                    <img alt="" src="{{asset('serviceIcon/user.jpg')}}" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;">
                                                @endif
                                                <input type="file"  name="icon">
                                                <span class="file-custom"></span>
                                         </label>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary mb-2" style="display: block;border: none;cursor: pointer;padding: 5px 33px;margin-top: 65px;background: #0f3e7c !important;position: relative;right: -272px;color: white !important;" href="#">Update</button>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary mb-2" style="border: none;color: #fff;cursor: pointer;padding: 5px 33px;margin-top: 65px;background: #ce171f;" href="#">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('javascript')

@stop
