@extends('layouts.master')

@section('content')
<style>
    #example1 tbody th{
        background: #0f3e7c !important;
        color: white !important;
    }
    #modalLoginForm .modal-header{
        background: #0f3e7c !important;
        color: white !important;
    }
    #modalLoginForm .modal-header span{
        position: relative;
        top: -13px;
        right: -6px;
        color: white;
    }
    .saving{
        padding: 8px 45px;
        margin-top: 13px;
        background: #0f3e7c !important;
        color: white !important;
        border: none;
    }
    .creating{
        padding: 8px 45px;
        background: #0f3e7c !important;
        color: white !important;
        border: none !important;
    }
    .chosen-single{
        height: 40px !important;
        padding: 6px 5px !important;
    }
</style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <form action="{{route('subServices.index')}}" method="get" class="example"  style="max-width:300px;margin-left: 9px;margin-top: -15px;">
                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    {{--<!-- /.col -->--}}
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="col-md-4" style="padding-left: -90px">

        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                        </div></div></div><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                {{--new form--}}
                                <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title w-100 font-weight-bold">Add Sub Services</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form style="padding: 8px 0;" action="{{route('subServices.store')}}" method="post"  enctype="multipart/form-data">
                                                @csrf

                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="staticEmail2" class="sr-only"></label>
                                                                <option style="font-weight: bold;">Service</option>
                                                                <select data-placeholder="Choose Services..." id="services" class="chosen-select" tabindex="2" name="service_id" required  data-height="40px" style="width: 87%;">
                                                                    <option value="">Select Services</option>
                                                                    @foreach($services as $service)
                                                                        <option value="{{ $service->id }}">{{ $service->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="inputPassword2" class="sr-only"></label>
                                                                <option style="font-weight: bold;">Sub Service</option>
                                                                <input type="text" class="form-control" name="title" placeholder="sub-service" required style="    height: 40px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="inputPassword2" class="sr-only"></label>
                                                                <option style="font-weight: bold;">Price</option>
                                                                <input type="number" class="form-control" name="price" placeholder="price" required style="    height: 40px;">
                                                            </div>
                                                        </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label for="inputPassword2" class="sr-only"></label>
                                                                    <option style="font-weight: bold;">Price Comment</option>
                                                                    <select  class="form-control" name="price_comment" id="price_comment">
                                                                        <option value="">Select you price comment</option>
                                                                        <option value="Fixed Price">Fixed Price</option>
                                                                        <option value="Starting From">Starting From</option>
                                                                        <option value="Vary After Inspection">Vary After Inspection</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="usr">ALT:</label>
                                                                <input style="height: 40px;" type="text" class="form-control" id="alt" name="alt" required/>
                                                            </div>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-group" style="cursor: pointer;font-size: 15px;padding: 15px 0;">
                                                                <i class="fas fa-images"></i>
                                                                <label data-error="wrong" data-success="right" for="defaultForm-email"> Image</label><br>
                                                                <input type="file"  name="image"  class="" style="position: relative;left: 0px;width: 172px;font-size: 14px;" accept=".png, .jpg, .jpeg" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="btn-submit" style="text-align: center;">
                                                                <a href="#"><button type="submit" class="btn btn-primary mb-2 saving" style="background-color: #156df2;">Save</button></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="text-center" style="margin-top: -25px">
                                    <a href="" style="float: right;margin-bottom: 28px;padding: 7px 40px;" class="btn btn-outline-success btn-rounded mb-4 creating" data-toggle="modal" data-target="#modalLoginForm">Create</a>
                                </div>
                                {{--//<----------Data show form---->--}}

                                <tr role="row">
                                    <th class="sorting_asc custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending" style="width: 213.247px;">#</th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Service
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;"> Sub Services
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;"> Price
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;"> Price Comment
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;"> ALT
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;"> Image
                                    </th>
                                    <th class="sorting custom-card" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 262.135px;">Action
                                    </th>
                                </tr>

                                <tbody>
                                <?php
                                $x=1
                                ?>
                                @foreach($subServices as $subService)
                                    <tr role="row" class="odd">
                                        <td class="custom-card">{{$x++}}</td>
                                        <td class="custom-card">{{!is_null($subService->service) ? $subService->service->title:'N/A'}}</td>
                                        <td class="custom-card">{{$subService->title}}</td>
                                        <td class="custom-card">Rs{{$subService->price}}</td>
                                        <td class="custom-card">{{$subService->price_comment}}</td>
                                        <td class="custom-card">{{$subService->alt}}</td>

                                        <td class="custom-card">

                                            @if($subService->image)
                                                <img alt="" src="{{asset('thumbnailImg/'.$subService->image)}}"  class="step1:after" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;">
                                            @else
                                            @endif

                                        </td>

                                        <td class="custom-card">
                                            <form action="{{route('subServices.edit',$subService->id)}}" method="get">
                                                <button style="    float: left; margin-right: 5px;" type="submit" class="btn btn-info js-sweetalert" title="edit"><i class="fa fa-edit"></i></button>
                                            </form>
                                            <form action="{{route('subServices.destroy',$subService->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button  type="submit"  onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </div>

                        </table>
                        @if($subServices instanceof \Illuminate\Pagination\LengthAwarePaginator )
                            {{$subServices->links()}}
                        @endif
                    </div>
                </div>
            </div>

        </section>
    </div><!--/. container-fluid -->

    <!-- /.content-wrapper -->
@endsection

@section('javascript')

@stop
