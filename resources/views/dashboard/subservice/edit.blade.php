@extends('layouts.master')
@section('content')
    <style>
        .chosen-default{
            height: 26px !important;
            padding-top: 0px !important;
        }
        .sub-text{
            font-weight: 600;
            background: #0f3e7c !important;
            color: white !important;
            padding: 10px 0;
            margin-bottom: 15px !important;
        }
        .form-group{
            font-weight: 600;
        }
        .chosen-container .chosen-single{
            height: 36px !important;
            padding-top: 5px !important;
        }
    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="text-center sub-text">Sub Service</h1>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->
            <section class="content">
                <div class="container-fluid">
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"></div>
                    </div>
                    <form action="{{route('subServices.update',$subService->id)}}" method="post" enctype="multipart/form-data">

                        {{--<form action="{{route('users.update',$user->id)}}" method="post">--}}
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                Sub-services:<textarea  style="height: 38px;width: 80%;" type="text" class="form-control" name="title" required>{{$subService->title}}</textarea>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                Price:<input style="height: 38px;width: 80%;" type="text"  class="form-control"  value="{{$subService->price}}" name="price" required="">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group" style="padding-left: 24px">
                                    <label for="inputPassword2" class="sr-only"></label>
                                    <option style="font-weight: bold;">Price Comment:</option>
                                    <select  class="form-control" name="price_comment" id="price_comment">
                                        <option value="">Select you price comment</option>
                                        <option value="Fixed Price">Fixed Price</option>
                                        <option value="Starting From">Starting From</option>
                                        <option value="Vary After Inspection">Vary After Inspection</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="usr">ALT:</label>
                                    <input style="height: 40px;width: 80%;" type="text" class="form-control" id="alt" name="alt" value="{{$subService->alt}}" required/>
                                </div>
                            </div>

                            <div class="col-4">
                            <div class="form-group" style="padding: 40px 0 !important;">
                                <label style="position: absolute !important;top: 9px !important;">Select File:</label>

                                <label id="projectinput8" class="file center-block">
                                    @if($subService->image)
                                        <img alt="" src="{{asset('thumbnailImg/'.$subService->image)}}" class="step1:after" style="height: 55px;width: 55px;display: block;margin: 0 auto;margin-top: -50px;">
                                    @else
<<<<<<< HEAD
                                        <img alt="" src="{{asset('serviceImg/user.jpg')}}"  style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: -51px;">
=======
                                        <img alt="" src="{{asset('serviceImg/user.jpg')}}"  style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;">
>>>>>>> 638016f26e269657e6257bcd0d9e3ca5597c5574
                                    @endif
                                    <input type="file"  name="image">
                                    <span class="file-custom"></span>
                                </label>
                            </div>
                        </div>

                        </div>
                        <button style="display: block;margin: 0 auto;padding: 8px 55px;background: #0f3e7c !important;color: white !important;border: none;" type="submit" class="btn btn-success js-sweetalert" title="edit">Save<i class=""></i></button>
                    </form>

                </div>

            </section>

            <!-- Main content -->
        </div>
        </section>
    </div>
@endsection

@section('javascript')

@stop

