@extends('layouts.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">

        </div>
        <div class="col-md-4" >
            <form action="" method="" class="example"  style="margin:20px 0;max-width:300px;margin-left: 9px;">
                <br>
                <div class="panel-body">
                </div>
            </form>
        </div>
        <!-- Main content -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div id="mahir-icon-top">
                    <img src="{{ asset('assets/images/mahir-logo.png') }}" width="100px">
                </div>
                {{--<h5>Mr.Mahir</h5>--}}
            </div>

            <div class="def">
                <div class="text-right">
                    <span class="invice-text">INVOICE</span>
                </div>
            </div>
        </section>

        <section class="invoice-to">
            <div class="row invoice-desc-sec">
                <div class="col-md-8">
                    <h4>Mudassar Hussain</h4>
                    <p>
                        26 A J3, Joahr Town, Lahore, Pakistan
                    </p>
                </div>

                <div class="col-md-4">
                    <div class="invoice-sub-total">
                        <dl>
                            <dt>Invoice No:</dt>
                            <dd>52484</dd>
                            <dt>Date:</dt>
                            <dd>10:00 AM 01/02/2020</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </section>

        <section class="table-sec">
            <div class="table-responsive">
                <table class="table table-hover table-bordered cstm-table">
                    <thead class="thead-dark2">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Service</th>
                        <th scope="col">Invoice</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>

        <section class="sub-total">
            <div class="row">
                <div class="col-md-6">
                    <div class="buss-text">
                        <p>Thank you for your business</p>
                    </div>
                    <div class="terms">
                        <p>Terms & Conditions</p>
                        <strong>Lorem ipsum, or lipsum as it is known as well as lorem ipsum,<br> is dummy text, graphic or web designs.</strong>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="right-sub-total">
                        <table class="table inner-cstm-table">
                            <tbody>
                            <tr>
                                <td>Sub Total:</td>
                                <td>Rs. 220.00</td>
                            </tr>
                            <tr>
                                <td>Tax:</td>
                                <td>0.00%</td>
                            </tr>
                            <tr class="bg">
                                <td>Total:</td>
                                <td>Rs. 220.00</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
            </div>

            <div class="Authorized">
                <div class="text-right bg">
                    <span class="author-text"></span>
                </div>
            </div>

            <div class="row">
                <ul class="cstm-nav">
                    <li>
                        Phone: <a href="#">03049351502</a>
                    </li>
                    <li><span class="separator"> | </span></li>
                    <li>
                        Address: <a href="#">Johar town lahore</a>
                    </li>
                    <li><span class="separator"> | </span></li>
                    <li>
                        Website <a href="#">wwww.mrmahir.com</a>
                    </li>
                </ul>
            </div>
        </section>


@endsection
