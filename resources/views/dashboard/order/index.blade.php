@extends('layouts.master')
<style>
    #view-detail-modal .close{
        position: absolute;
        right: 9px;
        top: 2px;
        color: white;
    }
    #example1 .chosen-container .chosen-single{
        position: relative;
        display: block;
        overflow: hidden;
        border: 1px solid #aaa;
        border-radius: 5px;
        padding: 5px 0 0 7px;
        height: 33px;
        background-color: #fff;
        background-clip: padding-box;
        -webkit-box-shadow: 0 0 3px #fff inset, 0 1px 1px rgba(0,0,0,.1);
        box-shadow: 0 0 3px #fff inset, 0 1px 1px rgba(0,0,0,.1);
        color: black;
        /*background: inherit;*/
        text-decoration: none;
        white-space: nowrap;
        line-height: 24px;
    }

    .chosen-container-single .chosen-drop{
        /*background-color: #c9302c !important;*/
    }

    #view-assign-modal .close {
        position: absolute;
        right: 15px;
        color: #fff;
    }

    #view-assign-modal .modal-header{
        background: #0f3e7c;
        color: white;
    }

    #view-invoice-modal .modal-header{
        padding-top: 40px;
    }

    #order-assign-content{
        padding: 21px 15px;
    }

    .datetimepicker-dropdown-bottom-right{
        padding: 10px !important;
        z-index: 100000 !important;;
    }

    .pending-row td{
        background-color: #c9302c;
    }

    .table-bordered {
        border: 1px solid #dee2e6;
        border-collapse: separate;
        border-spacing: 0 10px;
    }



</style>
@section('content')
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <form action="{{route('orders.index')}}" method="get" class="example"  style="max-width:300px;margin-top: -15px;margin-left: 9px;">
                            <br>
                            <input type="text" placeholder="Search.." name="search"  value="{{ request()->get('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row" style="background: #0f3e7c;color: white;">
                                <th style="width: 15%;">Name</th>
                                <th style="width: 20%">Service</th>
                                <th style="width: 15%;">Time Schedule</th>
                                <th style="width: 10%;">Status</th>
                                <th style="width: 15%;">Change Status</th>
                                <th style="width:10%;">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach( $orders as  $order)
                                <tr role="row" class="odd {{!is_null($order->status)? $order->status->title =="Pending" ?'pending-row':'':'--'}}" style="font-weight: 500;">
                                    <td style="border-bottom-left-radius: 15px;border-top-left-radius: 15px;" class="sorting_1 custom-card">{{! is_null($order->user) ? $order->user->name:'------'}}</td>
                                    <td class="sorting_1 custom-card">{{!is_null($order->subService) ?$order->subService->title:'N/A'}}
                                        | {{! is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}</td>
                                    <td class="custom-card">{{!is_null( $order->created_at) ? Carbon\Carbon::parse($order->created_at)->format('d-m-Y h:iA') : ''}}</td>
                                    <td class="sorting_1 custom-card"><button class="btn  btn-{{ !is_null($order->status) ? $order->status->id : 1 }}">{{ !is_null($order->status) ? $order->status->title : '-- -- --' }}</button></td>
                                    <td class="sorting_1 custom-card">
                                        <form action="{{url('admin/change-status')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="order" value="{{ $order->id }}">
                                            <select class="change-status form-control" name="status">
                                                <option value="">Select Status</option>
                                                @if(count($statuses))
                                                    @foreach($statuses as $status)
                                                        <option value="{{ $status->id }}" {{ !is_null($order->status) && ($order->status->id  ==  $status->id) ? ' selected': '' }}>{{ $status->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </form>
                                    </td>

                                    <td class="text-center sorting_1 custom-card" style="width: 17%;border-bottom-right-radius: 15px;    border-top-right-radius: 15px;">
                                        <div class="action-btns" style="position: absolute;">
                                            <form style="width: 39px;" class="pull-left" action="{{route('orders.destroy',$order->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <div name="">
                                                    <button style="float: left;"  type="submit" onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                </div>
                                            </form>

                                            <div class="view-user pull-left">
                                                <button style="width: 38px;position: relative;right: -3px;" class="btn btn-primary js-sweetalert order-detail"
                                                        data-order-id="{{$order->id}}" title="Order Detail"><i class="fa fa-info"></i>
                                                </button>
                                                <form action="{{route('pdfView',$order->id)}}" method="GET">
                                                    <button style="position: absolute;margin-top: -29px;margin-left: 25px;" class="btn btn-info invoice-detail" title="Order Invoice"><i class="fas fa-file-invoice"></i></button>
                                                </form>
                                                {{--<button style="width: 39px;position: relative;top: -43px;right: -44px;" class="btn btn-success assign" data-order-id="{{$order->id}}"  title=" Order Detail"><i class="fas fa-suitcase"></i></button>--}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>
                        @if($orders instanceof \Illuminate\Pagination\LengthAwarePaginator )
                            {{$orders->links()}}
                        @endif

                    </div>
                </div>
            </div>
        </section>
    </div><!--/. container-fluid -->
    <div class="modal fade" id="view-detail-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" style="font-weight: 600;font-size:30px;margin: 0 auto;">Order Detail</h4>
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="order-detail-content">
                </div>
                <!-- Modal footer -->
            </div>
        </div>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="view-invoice-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{--<!-- Modal Header -->--}}
                <div class="modal-header">
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="order-invoice-content">
                </div>
                <!-- Modal footer -->
            </div>
        </div>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="view-assign-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{--<!-- Modal Header -->--}}
                <div class="modal-header">
                    <h4 class="modal-title" style="font-weight: 600;font-size:28px;margin: 0 auto;">Staff Assign Task</h4>
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="order-assign-content">
                </div>
                <!-- Modal footer -->
            </div>
        </div>
        <!-- /.content -->
    </div>

@endsection

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).on('change','.change-status',function () {
            $(this).parent('form').submit();
        });
        $('.order-detail').click(function () {
            var orderID = $(this).data('order-id');
            $.ajax({
                url: "{{url('admin/order-detail')}}/"+ orderID,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr++},
                success: function(result){
                    if(result != ''){
                        $('#order-detail-content').html(result);
                        $('#view-detail-modal').modal('show');
                        $('.form_datetime').datetimepicker({
                            weekStart: 1,
                            startDate: '-3m -23d',
                            todayBtn:  1,
                            format: 'dd-mm-yyyy HH:iiP',
                            autoclose: 1,
                            todayHighlight: 1,
                            startView: 2,
                            forceParse: 0,
                            showMeridian: 1
                        });
                    }else{
                        alert('error');
                    }

                }

            });
        });

        $(document).on('change','.services',function () {
            var service = $(this).val();
            $('.sub-services option').remove();
            $('.sub-services').append('<option value="" disabled="disabled" selected>Select Sub Service</option>');
            $.ajax({
                type: 'GET',
                url: "{{url('getSubServices')}}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        $('.sub-services').append('<option value="'+row.id+'" class="list">'+row.title+'</option>');
                    });
//                    $('.sub-services').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

        $('.invoice-detail').click(function () {
            var orderID = $(this).data('order-id');
            $.ajax({
                url: "{{url('admin/invoice-detail')}}/"+ orderID,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result){
                    $('#order-invoice-content').html(result);
                    $('#view-invoice-modal').modal('show');
                }

            });
        });
        $('.assign').click(function () {
            var orderID = $(this).data('order-id');
            $.ajax({
                url: "{{url('admin/assign')}}/"+ orderID,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result) {
                    if (result != '') {
                        $('#order-assign-content').html(result);
                        $('#view-assign-modal').modal('show');
//
                    } else {
                        alert('error');
                    }

                }
            });
        });

    </script>

@endsection

