
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">View User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

                <div class="modal-body mx-3">
                    <div class="md-form mb-5">

                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" id="orangeForm-name" name="name" value="" class="form-control validate" style="float: left;" required>
                        <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="email" id="orangeForm-email" name="email"  value="" class="form-control validate" style="float: left;" required>
                        <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                    </div>

                    <div class="md-form mb-4">
                        <i class="fas fa-phone prefix grey-text"></i>
                        <input type="number" id="orangeForm-pass"  name="mobile" value="" class="form-control validate" style="float: left" required>
                        <label data-error="wrong" data-success="right" for="orangeForm-pass">Your Phone</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="password" id="orangeForm-pass" name="password" class="form-control validate" style="float: left" required>
                        <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-address-book prefix grey-text"></i>
                        <input type="address" id="orangeForm-pass" name="address" class="form-control validate" style="float: left" required>
                        <label data-error="wrong" data-success="right" for="orangeForm-pass">Your address</label>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button  type="submit" class="btn btn-deep-orange btn-outline-success">Submit</button>
                </div>
        </div>
    </div>
</div>

{{--<div class="text-center" style="margin-top: -39px">--}}
    {{--<a href="" style="float: right" class="btn btn-outline-success btn-rounded mb-4" data-toggle="modal" data-target="#modalRegisterForm">Create--}}
    {{--</a>--}}
{{--</div>--}}
