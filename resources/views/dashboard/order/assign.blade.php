<style>
    /* Links inside the dropdown */
    .dropdown-content a {
        color: white;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        background-color: #3498DB;
        width: 268px;
    }

    /* Change color of dropdown links on hover */
    .dropdown-content a:hover {
        background-color: #0f3e7c;
        color: white;
    }
</style>

<html>
<body>

<form  action="{{url('admin/assignOrder',$order->id)}}" method="post">
    {{--@csrf--}}
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                <div class="form-group">
                    <label>Vendor List:</label>
                    <input type="hidden" name="order" value="{{ $order->id }}">
                    {{--<input type="hidden" name="order" value="{{!is_null($order->area)? $order->area->name:'' }}">--}}
                    <select class=" form-control" name="staff_id"  id="staff_id"  style="width: 99%;" required>
                        <option value="">Select Vendor</option>
                        {{--{{dd($order->area->name)}}--}}
                        @foreach( $staffs as $staff)

                        <option value="{{$staff->id}}">{{!is_null($staff->user) ? $staff->user->name:'N/A'}}</option>
                        @endforeach
                    </select>
                </div>
                </div>


                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Vendor Price:</label><br>
                            <input class="form-control" type="text" name="staff_price" value="" >
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="name">Statuses:</label>
                        <select class="change-status form-control" name="staff_status" >
                            <option value="">Select Status</option>
                            <option value="Pending">Pending</option>
                            <option value="Done">Done</option>
                        </select>
                    </div>
                    </div>
                </div>

                <div class="col-md-12">
                <div class="form-group">
                    <label for="name" class="form-label">Order Assign Date:</label>
                    <div class="form-group" style="margin: 0;">
                        <div class="controls input-append date form_datetime " data-date-format="dd MM yyyy - hh:ii " data-link-field="dtp_input1">
                            <input size="16" class="form-control cstm-input-field"  name="assign_date" type="text"  placeholder="Set Time Slot" autocomplete="off" required>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                    <label for="name" class="form-label">Order Completion Date:</label>
                    <div class="form-group" style="margin: 0;">
                        <div class="controls input-append date form_datetime " data-date-format="dd MM yyyy - hh:ii " data-link-field="dtp_input1">
                            <input size="16" class="form-control cstm-input-field"  name="done_date" type="text" value="{{!is_null( $order->schedule_datetime) ? Carbon\Carbon::parse($order->schedule_datetime)->format('d-m-Y h:iA') : ''}}" placeholder="Set Time Slot" autocomplete="off" required>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div class="row">
                 <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Order Price:</label><br>
                                <input class="form-control" type="text" name="order_price" value="{{ !is_null($order) ? $order->order_price:'---'}}" >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="name"> Assignment Statuses:</label>
                            <select class="change-status form-control" name="status" >
                                <option value="pending">Select Status</option>
                                <option value="Pending">Pending </option>
                                <option value="Done">Done</option>
                            </select>
                        </div>
                        </div>

                        <div class="row">
                             <div class="col-md-6">
                                  <div class="form-group">
                                        <label for="name">Mahir Price:</label><br>
                                        <input class="form-control" type="text" name="mahir_price" value="" >
                                   </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="name">Statuses:</label>
                                    <select class="change-status form-control" name="mahir_status" >
                                        <option value="">Select Status</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Done">Done</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                 <label for="name" class="form-label">Duration:</label>
                                       <input class="form-control" type="text" name="duration" value="" placeholder="Duration" required>
                            </div>
                 </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="modal-btn text-center" style="display: block;padding: 30px 0px;height: 0;">
                    <button type="submit" class="btn btn-success" id="save-btn" >Submit</button>
                    <button type="button" class="btn btn-danger btn-close"  style="border-radius: 5px;width: 122px;" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>

    <script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            startDate: '-0d',
            todayBtn:  1,
            format: 'dd-mm-yyyy HH:iiP',
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    });

    /* When the user clicks on the button,
     toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown menu if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }


</script>
</body>
</html>