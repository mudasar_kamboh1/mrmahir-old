<form  action="{{url('admin/order-update',$order->id)}}" method="post">

<div class="new-measurements">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Token:</label><br>
                <input class="form-control" type="text" name="token" value="{{ !is_null($order)? $order->token : '-- -- --'}}" readonly="">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Name:</label><br>
                <input class="form-control" type="text" name="name" value="{{ !is_null($order->user) && $order->user->name? $order->user->name : '-- -- --'}}"readonly>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Email:</label><br>
                <input class="form-control" type="text" name="email" value="{{ !is_null($order->user) && $order->user->email ? $order->user->email : '-- -- --'}}"readonly>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Password:</label><br>
                <input class="form-control" type="text" name="password" value="{{ !is_null($order->user) && $order->user->password_hint? decrypt($order->user->password_hint) : ''}}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="name">Service:</label>
                <input type="hidden" name="order" value="">
                <select class="services form-control" name="service_id" style="width: 99%;">
                    <option value="">Select Service</option>
                    @foreach($services as $service)
                        <option value="{{ $service->id }}" {{ !is_null($order->subService)&& !is_null($order->subService->service) && $order->subService->service->id == $service->id ? 'selected' :''}}>{{$service->title}}</option>
                    @endforeach
                </select></div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="name">Sub Service:</label>
                <select class="sub-services form-control" name="subservice_id" style="width: 99%;">
                    <option value="">Select SubService</option>
                    @if(count($sub_services))
                        @foreach($sub_services as $sub_service)
                            <option value="{{$sub_service->id}}" {{ !is_null($order->subService) && ($order->subService->id == $sub_service->id) ? 'selected' :''}}>{{$sub_service->title}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="name">Area:</label><br>
                <select class="change-status form-control" name="area_id" style="width: 99%;">
                 <option>select Area</option>
                    @if(count($areas))
                        @foreach( $areas as $area)
                    <option value="{{$area->id}}" {{ !is_null($order->area) && $order->area->id == $area->id ? 'selected' :''}}>{{$area->name}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="name">Verified Mobile:</label><br>
                <input class="form-control" type="text" name="name" value=" {{ !is_null($order->user)? $order->user->mobile : ''}}"readonly>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="name">Additional  Mobile:</label><br>
                <input class="form-control" type="text" name="mobile" value="{{ !is_null($order) && ($order->mobile) ? $order->mobile : '' }}">
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Address:</label><br>
            <input class="form-control" type="text" name="address" value=" {{ !is_null($order) && $order->user->address ? $order->user->address : '' }}" readonly >
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Additional  Address:</label><br>
            <input class="form-control" type="text" name="address" value="{{ !is_null($order) && $order->address ? $order->address : '' }}">
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-6">
                <label for="name">Statuses:</label>
                <input type="hidden" name="order" value="{{ $order->id }}">
                <select class="change-status form-control" name="status">
                    <option value="">Select Status</option>
                @if(count($statuses))
                    @foreach($statuses as $status)
                            <option value="{{ $status->id }}" {{ !is_null($order->status) && ($order->status->id  ==  $status->id) ? ' selected': '' }}>{{ $status->title }}</option>
                        @endforeach
                    @endif
                </select>
            {{--</form>--}}
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="name">Problem Message:</label><br>
                <input class="form-control" type="text" name="problem_message" value="{{ !is_null($order) && $order->problem_message ? $order->problem_message : '' }}">
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-md-6">
           <div class="form-group">
               <label for="name" class="form-label">Set Time Schedule:*</label>
               <div class="form-group">
                   <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - hh:ii " data-link-field="dtp_input1">
                       <input size="16" class="form-control cstm-input-field"  name="schedule_datetime" type="text" value="{{!is_null( $order->schedule_datetime) ? Carbon\Carbon::parse($order->schedule_datetime)->format('d-m-Y h:iA') : ''}}" placeholder="Set Time Slot" autocomplete="off">
                       <span class="add-on"><i class="icon-remove"></i></span>
                       <span class="add-on"><i class="icon-th"></i></span>
                   </div>
               </div>
           </div>
       </div>
       <div class="col-md-6">
           <div class="form-group">
               <label for="name">Price:</label><br>
               @if ($order->order_price === null)
                   <input class="form-control" type="number" name="" value="{{ !is_null($order->subService) ? $order->subService->price:'---'}}" >
                   @else
                   <input class="form-control" type="number" name="order_price" value="{{ !is_null($order) ? $order->order_price:'---'}}" >
               @endif
           </div>
       </div>
   </div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Created at:</label><br>
            <div class='input-group date' id='datetimepicker2'>
                <input type='text' class="form-control" name="created_at" value="{{!is_null( $order->created_at) ? Carbon\Carbon::parse($order->created_at)->format('d-m-Y h:iA') : ''}}"/>
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
   </div>

    <div class="modal-footer text-center" style="display: block;">
        <button type="submit" class="btn btn-success" id="save-btn" >Save</button>
        <button type="button" class="btn btn-danger btn-close"  style="border-radius: 5px;width: 122px;" data-dismiss="modal">Close</button>
    </div>
</div>
</form>


<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            weekStart: 1,
            startDate: '-3m -23d',
            todayBtn:  1,
            format: 'dd-mm-yyyy HH:iiP',
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    });
</script>

