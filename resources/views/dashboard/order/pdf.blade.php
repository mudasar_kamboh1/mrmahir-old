<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/mahir-logo-fav.png') }}">

<!DOCTYPE html>
<html>
<head>
    <style>
        .cstm-table thead{
            background-color: #0b2240 !important;
            color:#fff !important;
        }
        .buss-text{
            font-weight: 600;
        }
        .terms p{
            font-weight: 600;
            margin-bottom: -4px;
        }
        .terms strong{
            font-weight: normal;
            font-size: 11px;
        }

        .inner-cstm-table td{
            border-top:none !important;
            padding: 0 37px;
        }

        .bg{
            background-color: #0b2240 !important;
            color: #fff;
            height: 30px;
        }

        .Authorized {
            background-color: #0b2240;
            height: 10px;
            margin-top: 20px;
        }
        .cstm-nav{
            list-style: none;
            display: inline-flex;
        }
        .cstm-nav li{
            padding: 0 30px 0 0px;
        }
        .cstm-nav li a{
            color: black;
            font-weight: 600;
        }
        .invoice-to strong{
            color: #545151 !important;
        }

        .btn-primary:focus :hover{
            outline: none !important;
            border: none;
        }
        .btn-primary:hover{
            outline: none;
        }

        @media pdf{
            .terms-condition{
                padding:45px 20px !important;
            }
            .btn{
                display: none !important;
            }
            .inner-cstm-table td{
                padding: 0px 10px;
                height: 25px;
            }
            .thead-dark2 tr th{
                height:35px;
            }
            .right-sub-total .inner-cstm-table{
                margin: -2px 2px;
            }
            .invice-text{
                font-size: 40px !important;font-weight: bold !important;color: white !important;display: block !important;text-align: center !important;
            }
            .def{background-color: #0b2240 !important;color-adjust: exact;height: 50px !important;margin-top: 40px !important;}
            .cstm-table thead {
                background-color: #0b2240 !important;
                color: white !important;
            }
            .bg {
                background-color: #0b2240 !important;
                color-adjust: exact;
                color: white!important;
                height: 30px;
               }
            .Authorized {
                background-color: #0b2240;
                color-adjust: exact;
                height: 10px;
                margin-top: 20px;
                }
        }
        @media print{
            #mahir-icon-top img,  #mahir-icon-top{
                display: none !important;
                box-shadow: none !important;
            }
            .texts h3{
                display: none !important;
            }
            .btn{
                display: none !important;
            }
            .inner-cstm-table td{
                padding: 0px 10px;
                height: 25px;
            }
            .thead-dark2 tr th{
                height:35px;
            }
            .right-sub-total .inner-cstm-table{
                margin: -2px 2px;
            }
            .invice-text{
                font-size: 40px !important;font-weight: bold !important;color: #fff !important;display: block !important;text-align: center !important;
            }
            .def{background-color: #0b2240 !important;color-adjust: exact !important;-webkit-print-color-adjust: exact;height: 50px !important;margin-top: 40px !important;}
            .cstm-table thead {
                background-color: #0b2240 !important;
                color-adjust: exact !important;
                -webkit-print-color-adjust: exact;
                color: #fff !important;
            }
            .bg {
                background-color: #0b2240 !important;
                color: #fff !important;
                height: 30px;
                color-adjust: exact !important;
                -webkit-print-color-adjust: exact;
            }
            .Authorized {
                background-color: #0b2240;
                height: 10px;
                margin-top: 20px;
                color-adjust: exact !important;
                -webkit-print-color-adjust: exact;
            }
        }
    </style>
</head>
<body>

<div class="invoice-to">
    <section class="content">

        <div class="row">
            <a style="text-decoration: none;cursor: pointer!important;" href="{{url('admin/orders')}}"><button class="btn btn-primary" style="background-color: gray;border: none;padding: 7px 21px;border-radius: 50%;height: 47px;display: block;margin: 0 auto;float: right;margin-top: 54px;cursor: pointer;color: #fff;font-size: 20px;margin-right: 42px;">x</button></a>
            <a onclick="window.print();" style="text-decoration: none;cursor: pointer!important;" href="{{url('admin/orders')}}"><button class="btn btn-primary" style="background-color: gray; border: none;padding: 7px 21px;border-radius: 3px;height: 47px;display: block;margin: 0 auto;float: left;margin-top: 54px;cursor: pointer;color: #fff;font-size: 20px;margin-left: 42px;">Print</button></a>

            <div id="mahir-icon-top" style=" display: block !important;
                                             margin: 0 auto !important;
                                             text-align: center !important;
                                             /*padding: 30px 0;*/
                                             background-color: #fff;
                                             border-radius: 60%;
                                             box-shadow: 0px 0px 8px #cbcbcb;
                                             height: 115px;
                                             width: 160px;
                                             padding-top: 33px;">
                <img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAABwCAYAAAGP7mnWAAAAGXRFWHRTb2Z0d2Fy
                ZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4RpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/
                IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb
                2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRw
                Oi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6
                Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4b
                Wxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpjNTc4NTA1Yi1lZDZmLWRhNGE
                tOGNmZi04Njk3NDAwOTMyNGYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NjJDNzVFQUY4RTk0MTFFOThCMzdBRkQzQzRGQkNENjIiIHhtcE1NOkluc3Rhb
                mNlSUQ9InhtcC5paWQ6NjJDNzVFQUU4RTk0MTFFOThCMzdBRkQzQzRGQkNENjIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFd
                pbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6YzRmNmJkMGItYTRhZi0xNjRlLWEyZWQtNzU0ODZlY2I5ZDhlIiBzdFJlZjpkb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6YWNjODU1ODctODAyNi01ODRkLWIyMzQtYjVlOWI3OTJhNTQ4Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+AQp8sgAAEcBJREFUeNpiYCACvJVW+Q/EH6Dss7jUMRIyBJu48NM7WPUxEeEwVkKGkASg3vxPSB0TOd7EBVgoMQiohg1I/cTpMmJdA1X3E6c3STQIBmzwedOQ2DCCxS7QcMx0hs1lIA3EupiJ1GSCL61h8+ZZPLF8kUfZ4b88kPHwx5OLGIYBvaAMtO0uks0mMC+juYJFnkPmz5e7BxiBBjoA2fux5k2kMNkINCAAS+xxAcW/gVyFLwgAAoiRiGTwCkiJgsIXaOB/qpUYJJcW6GEFZRsPbClBtRKCUMEHVPMX5hgWbBoJeQWbPBMlBR4ugxSJDPy/2JICC7EFHzZxZDFyYs2Y2Fg7CMQC2EoBEAHMtHbAUmAukGmCy6D7UH87YIt6IN8ARPMoy/wHGgYLz3/AUuERKEOjRDe2dINsKL4SACCAKKppoe0IfkpKWZJyCTUra6plXVJ8j+5ISts6TOQ6CNraGDwhhRQyjNR0DNBcUM31igWHpDYwNK5C2TpA9hVqpSWgGZOAVC45IXUFKRQuUyNEgI65AaTUKYk+RaRQMaSCg2ywOIgPGAOfscUAEwN9wGE0/lOYg8hN6OdhDV0qlmPS+NSw0DqIgPURiJoMii5gJQeqm+wIpimgi/WAQXkJTdwBic0DlP+KFALVQH4b8dXKHYQ4kR6BVaL/gAYxYQn6O0BxVaRosQJSR/GU6CAPXkaqcB2h4qD2eyEQvwDi5UAciUY7QtXAaOoDoIMeE+oH4AMAAUSN8ucAkDJAai18BIaWAFTuIJBtTzdHAS0EOeIDEUonAh1WQPMKGeig10Q6iAFHs5O6jgI6iBNIiZCg5QPNHQUE32C5D1e7CU2uky7tKSAQQmI34iuvgPg5PdpTzECL/tGyFiBnqOUfrdvp5Oa+X7RyENDsZhYyNH1kQBoQpbKDNgEpXxYSNT0C1fZUckA7kKqgRkKXpYJjSB/7AWp6i40PpAUodAwbJQOdQgT45IKflJRTylAf/YdW2CpUiLJnOKSmADGowfgMr6OA5dA9aJucEcq/S4VQkiTQ7adKNUMRIDTWMCCOorjuI6c6IWecdNCHFBOt0w45Y1U0Dylgr6YA2CFdSIrjiKlmDpLRzkae9IlAGku4SBVHgYaKkRMnET7OB6qZBON8uXvAAsksA6IcBbSMFaj4N1LOQO4UYBt+R1GPxROToB3Rh0AHKQDZclA2I1QcNLR0HokG9YoTgDgeOU1JAi2ShFo4AUi9RrKACUuB9wuobiERhSIHaMoNSFtB0xZsfMIEjQaFngSUXYs8lgDqLPaTUuoihVwDkKpHVk9Jl51arUVNLDnuPxDzkGsmQAAxDnRBCfTUPqTRGXzgBTA2JJH0gcaYlKHcBqBc44BWfTQImJ1Ayo0MrRJ4qky5AW8jUDGAQONzDjQynuZ5nYlOgXQESyCBBk4fk2CMLVKTA30E7gmt/cBIp4D6j6+WgNUIlLTph0WKgoLSgfbsoC+j0AMImIJygWKTRwOKuGw4GlA4Agg07s451LIc0N0cQOo7Cx0sAo3X8g3ywNADUiFAHMaAYyEACw0tnw6kMgZRYHQDqRKaZj2gJdygbjeu1VlAihco9wVJTGCgAwnoBlCN/pfeZdQXAvKfB0O/EVu7jd4BBRpYYYU6wBSYes4A2aAli2eQ5AdDAIFS8vsBa3ACAwa0uC0Hyj0NDTBYIBVB5QcDIDaQjGHdIWJHYklpmW/FIb5/kKSmdUQomw4NmHODuQtDaxBIRM7IGgp9vYEGBwe6ZU7yssEBqsnsqbVlYKgCUNm0jNaWDOmAAhXM8hwyQUB8Gq0G4xsNKOygHzrLAsbQJcgHqWkBaGaPF9/aZiSAa6pHH31BL3QCQRzKvQ2UD6VG6iFRvQO1W+bZQNxBhNp9OBx0CYsw8izLKQrd+B826wlMLaCuFKjfCZ5eBfet7h5A3wUEn/pCEjMEqruAh+8I5B8A8h1x+RO0N7ADeU8XltoGtr5JFIu0CQ5t5kjsVOiGva9AvBzaiycWKCNPDUMDCTOpKzsoEBIjxIeCeCR2IhD3wTjIq4JagFQ1kR74BPQAP7WSNZbJBVWg+XeweH7ApqBZkLJQDZCqgTp8PgNkRQAymAxUk0dDt8BXqxMBFjEQmLWhJgBmywdDYlYEmJJAqzRgy0emAh2eQ283AARg72pCo4ihcFwXCnVLqcUe/KHWChYv7cGTYlWqFZRSUPGwW4pYsPZYEcFbT+JNEAQRBaUoVBS0iAdFWQXFX4onLXalB3exKnqwWoU9+F43w6ZpJpvZnckk2wkMmWQyM9lvX5L3Xt57Y4LtAa6m6N+F1IRaCFavjgbSuCK/Amrr5u7bABkKt0m4dr+qgYIfi/rpD4rN1wEgn+l9KDN9LJd1sAYolZ3hIHmssIRiLwAhE/rFRtY/phGkDltB0gYUgIRDY8JmYVIXRWVtl7p1AcX7CuCyf8/D/VOMCqWOlN4+sw8oGHb7uSp0B0avth76w88pPOY6s8LhRiwL1u9qoag9rN6I95mG8okyWYI6SmWJagGqnebT5YKyVOaoBkoBLcTipAOoXQKLu/4IqMVziWib+3wEVOlV8Fk09EqDhDF2ttoGEvR7h05Zb7eNQ46mtC5ZDwPZPLQRIej7fOQqHcauuCPzxEKAcGW+4mAUD/hluJ31wnBAUP18gBQsgnvcMAnSKng1ZK8NBKaLFDZ8t3i5L0iKyhoEznrIXsLRFDh7QO0J+Loxl7YPDAGoAY48KUT4bKrkWTHFF34iYov+TfSaTGMQFkiopfgBx3ItDCf6pRN5XNoW2sakeQgNc0/r5syPKbQxyZUDAdoXhgij4hFVa9BSfyYsWe8yzZuhIxNMp/DcUcpdMoSgvoYmFDdmpzCI3h9a7GCCkzvBOuZcVCm6qQlZHS+mSLiVf4QoRn9S4qMAiBWSr1DUGkJN1xTbTUKf2ziQzVKzBJySCm3e8yD5znBWQwKQNgfOmRsu2KrwcRU5N1ULRW1XaHMqAoqQNoVh9yYCqkKBVyUt8zAP+Gm6vMACOOiPTejizKMUAbV0gPpGOfJc0C+KWwzSbZjnDuma52ymqFSidedxxz+Gc2y8EVFUkS/6l2hdi5Z36AKCqueUoxVozKRTQGHJCKhictzS2M0M3Gw9ayJFObvA9R74sl8e3zFD5DbqwzRn7UHfGQUU75LK25SrRuMtkQ7Cc2TmQixAT318b6BDD4Prtvs8HwlBms2kRyAb0THGVeNHuQWpGRfUNfvcx046Yc+HZ6YALRjicA15qX72GtShrn/AqYPyKJz2MeWLcDrI3ePKYsTYDQNJcgsIOiSou8CBPFYBSI+Bmpwo37IJWmQTOsCV+7jyoFc+6iT8mL0SarpJxI7X6D2eEwwTdDbMM1WH6YbEWziuetgszcGzupiyTt38JCk4XxeHHnTmEXR+Fo5ROB9iAEKNOxpR1bg8rF4yp2BQLgSM/TAennfLoqEz6Tu0W+N2kQlErnSNrytVhvQXjmmhmsWjCLANfshzv/4+7t0ZePZGwY+9A1lv6EIxZf1VviW0yk+QuHRXBJIJcmWcGzI19BuQt0gxZIgzZo8GCBCmXnj+uOQ6+2UhnV4QM7Aqzhnhzo+7vABSXjLv4GcgnZDeP6HjK3X30fi4BwBSJyMmLeKhdKX/ArR3tSFWVGH43PUSbS5SuRbZGtrCppSVG6YS1NUS+rAliSgqiRaR/qSUaGRGu1FIhWtF9LVRYdkqYRL0Qwhh648WZS7in1gpyxIqKE3aarHteZj37s69OzN3ZufsvTNz3xcOs3fPzDlnznnmnPe85/1Irdtsi2imohmjbd5lqcj78XXsCKiPXwcVgN2rOjmB9mKQ4TRTrg4BRBFWJ9Iq4xMm3kY1SK8axyMn2THGmKGGNs0V/ewwI8coTSLl6wBAOQHQFp+N2GTQdJGhPFlvH2w+w0CajwtdALWkpMnnKqCSCaSi/c1Mo1Q7wUFGwLTaOHZKaQTTH1kYg3yGwEQ+qTchzaGxG3X7KIpsQ1piHLc7QXRKAZUcMDWHABOlgYyX+r3rOfq6fd5ENAMNoG6U3xXQTkbo2WfGfGS66UcFVHKoM2D5fpx+kD23Yj8NUud8oQz26EHppG39HMfTM8Ri/w0kbhz4v7Wuc0sFVALIS57EHV4HBipsNKbj1Wos2sQl8WqTQcoKoNwHajzDYTjVA0ZJd3kTJIZSpJIA9XnO8gMTlppGHXKdocIsIYtDMO5UMfq8yDMpKaDi7ALnCPN7Rodcl7y4YHoFF7rWadLh1hkqDpB4uv+hUSPEavU3+diBLJ7lLTOOE3llwKvX57RM4clAPktHLxQW0vpsmg5x1fqc1js8oViemSVPgTSpfcud8eXGOYtcJNe2TPJQeNl5IgZo1qGP1G+0JV4oALlGAEM/oVZ8buZT2CFk/mj92a7wCJxZVhhH7bijmrN3fpJeaEqYMzTcxwC2/0Uot8SyQ2m0X+gqi6Fa77E10yQCUHixK3DhqflJpNkhHjkozrKv9wkBXf7V7VP4jPYHDZnfNglTJmyw+IJduBw20XWjaYQ8IM8H0VRTJ5L9gD5muPHdYkO81yRQM7XB0ovSjWPcaLBPoZyCzj2e/ZtjvHn8+Y/wRYklWzPUjZbKUUCNBxMjVQ0Lf5R4sgWog2W/Z6IjHhNP4l6dlGe+Ge939RuFUEk/bZQd7ZS0tNkKoMR7g9vLHhloqt0Oo1POIPVQ7xtpqwRjGZZ8t3OXPpTzscJoFEybcHkube22xuQCDPRGSEZxuwdoHzFjvua86AE8v11hNAomypCetVwsGXkqIPagrwd86h1JDKAEVO+hURQbfBfhsQV47pDCqEQ88r7FInmaQN36k6maoZSs0QYTLYKNH1Gw3F5JvqeAyj6tsVAGl65WgOlYWnd5SnaWu0uNHYeu62sBJgVU8ug6C2XwbPS1VIsNlOztayyU8TVmp79TLzaIQR2Y6sNY0Z6TAkDMxbvcEeP5JTYmiZhtSD2gujM0w9wtqZZEpbk9uuQpZYIUUEoKqIxQD1JjWdQx8omfpvmlVLBZI/EAwy00tRaam1pbdhvHZ/kL04/27wBDzViAXtJtepg5WwGlNF42MBa7g0GNbpW/Z0geXSPO9npOvMf8YBJs6aOAqi251aVXYcba5nPfZ6eP9hcAtiGA6gh+36CAUqpElMVtNY5VD8NUMoxSb9peIgmAypL6yqN4l20xNkjrJZXTkGu5LCQaUJhCL0Ajf7FY5kUR76/on1tiifk5Nf2Ayn0J6Esexr4c8Zm9Id6fumWfpGmG4oGkTclqZ4R7T7jdPAfQgwF5SZjdvkWa7zZuBT/EY5QXka4tAg58UAmzjd903XjA9Qz1oN40jrVvcUe3FPeVGM3iPsZ3exeJtnnF0LQLcN8hyT8Pl3eQbjZjatbufPrKYvhXWtA0uurpL6tnHS4PI7VGAdQcmi+hM/rj9irKeQiX1REeCaszPRiQtwH1vo72/1kjMG1B3ZvKBoJBg56OUgieoSECo0BMrXAfNTqPGx93RcjPSzl++Q2SP61CPRybjZHXbXQGBWybMSibYwCJBoj0gBJFbeIj1P1SyG02I0l94ZPN7fYp1P8V0u0RwvXamJXOLweT0ETCps2qBCahi02w76uWCvmXmHC+Di6cKCPIAbuJSx8G4y9RkA8LpFak/cYxQFweod63UOedEWU3i0Vu4+czgYNI65t/JR400zExj7dJvyFdifZchvR7ne1EKb7ISbTQlV435MoAQmPCPvlJcycquNPm7rRxTKP4FdEVzLwJNoiK8pQSH7GwvJJHuQXpKtkI8OP4WWayXahjMGJ5XSbY+vkEeRaUezjE8lXTUNKJERugs3aiY3cJM7kWaZmkuMQ1e4VN8QDK4sy4vwp99KW0/VejVImGGjwGagRpnRxWUtg2Ufc59Ll4mxx8tqRQ1tQtPOYiBVNFIhvCEPGz8hVmARoEFlzLQk6WvTZh7LhlHZIZaBD3pz3mG8/Y7gspyvBb7pb6LHf3gvfoyzqycvpx2SEAic71GbBorkf2GoCptx76QQEVD0TsPzr9eMZ4O7SguOMJgGmkXvrkf6SaLMnCmfPhAAAAAElFTkSuQmCC" width="100px">
            </div>
        </div>

        <div class="def" style=" background-color: #0b2240;height: 50px;margin-top: 40px;">
            <div class="text-right">
                <span class="invice-text" style="font-size: 40px !important;font-weight: bold;color: #fff;display: block;text-align: center !important;">INVOICE</span>
            </div>
        </div>
    </section>

    <section class="invoice-to">
        <div class="row invoice-desc-sec" style="padding: 15px 15px 0;">
            <div class="col-md-8">
                <h4>{{ !is_null($order->user) && $order->user->name? $order->user->name : '-- -- --'}}</h4>
                <p style="margin-bottom: 1px;">
                    {{ !is_null($order->user) && $order->user->address? $order->user->address : '-- -- --'}}
                </p>
            </div>

            <div class="col-md-4">

                <div class="invoice-sub-total" style="float: right;margin: -95px 0px;">
                    <dl>
                        <dt>Token No:</dt>
                        <dd>{{!is_null($order)?$order->token:'--'}}</dd>
                        <dt>Date:</dt>
                        <dd>{{ $order->created_at ? Carbon\Carbon::parse($order->schedule_datetime)->format('Y-m-d H:i:s') : '-- -- --'}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </section>

    <section class="table-sec">
        <div class="table-responsive">
            <table class="table table-hover table-bordered cstm-table" style="width:100%">
                <thead class="thead-dark2">
                <tr style="height: 40px;">
                    <th scope="col">#</th>
                    <th scope="col">Service</th>
                    <th scope="col">Address</th>
                    <th scope="col">Area</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>{{! is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}
                     |  {{!is_null($order->subService) ?$order->subService->title:'N/A'}}
                    </td>
                    <td>{{ !is_null($order)?$order->address:'---'}}</td>
                    <td>{{ !is_null($order->area)?$order->area->name:'---'}}</td>
                    <td>{{ !is_null($order->subService) ? $order->subService->price:'---'}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>

    <section class="sub-total" style="padding: 15px;">
        <div class="row">
            <div class="col-md-6">
                <div class="buss-text" style="color: white !important;">
                    <p>Thank you for your business</p>
                </div>
                <div class="terms" style="color: white !important;">
                    <p>Terms & Conditions</p>
                    <strong style="color: white !important;">Lorem ipsum, or lipsum as it is known as well as lorem ipsum,<br> is dummy text, graphic or web designs.</strong>
                </div>
            </div>

            <div class="col-md-6">
                <div class="right-sub-total" style="margin: -75px 2px;font-weight: 600;float: right;">
                    <table class="table inner-cstm-table">
                        <tbody>
                        <tr>
                            <td>Sub Total:</td>
                            <td>Rs.{{ !is_null($order->subService) ? $order->subService->price:'---'}}  </td>
                        </tr>
                        <tr>
                            <td>Tax:</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="bg">
                            <td>Total:</td>
                            <td>Rs. {{ !is_null($order->subService) ? $order->subService->price:'---'}} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="Authorized">
            <div class="background">
                <span class="author-text"></span>
            </div>
        </div>

        <div class="row">
            <div class="texts" style="display: inline-flex;margin: 0 auto;">
                <h3 style=" font-size: 15px;   margin-bottom: -13px;">Phone: 03096661919</h3>
                <h3 style=" font-size: 15px; text-align: center; margin-bottom: -13px;padding: 0 100px;">Address: Johar town Lahore</h3>
                <h3 style="font-size: 15px; float: right;   margin-bottom: -13px;;">Website: www.mrmahir.com</h3>
            </div>
        </div>
    </section>
    <section class="row">
        <div class="center">
            <a style="text-decoration: none;cursor: pointer!important;" href="{{route('pdfDownload',$order->id)}}"><button class="btn btn-primary" style="background: #ce171f;padding: 10px 55px;border-radius: 5px;display: block;margin: 0 auto;margin-top: 57px;color: #fff;">Download</button></a>
        </div>
   </section>

    <section class="terms-condition" style="padding: 15px 20px;">
        <div class="terms">
            <strong style="font-weight: bold;font-size: 20px;">Mr. Mahir-Terms and Conditions</strong>
            <ul style="padding: 0;">
                <div style="padding: 0 38px;" class="">
                    <li>Standard service charges of minimum Rs. 500 will be applied each time you call a service provider through Mr. Mahir. </li>

                    <li>These standard charges will be applied if the total charges of your service are less than Rs. 500, however, if your total charges for the service are above Rs. 500 then these service charges will be exempted. </li>

                    <li>In case of any parts required for the repair or fixing, customers can provide those parts. However, Mr. Mahir can also provide these parts but the charges of it will have to be paid by the customer, and these charges will be other than the service charges applied.</li>

                    <li>In case of any pricing conflict, customers can reach out to the Customer Support department for any queries or concerns.</li>

                    <li>In case of poor weather conditions or traffic disruptions, or any other such unfavorable conditions, the technician might get late in reaching your location. </li>

                </div>
            </ul>
        </div>
    </section>
</div>

</body>
</html>
