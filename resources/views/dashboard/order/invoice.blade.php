<!DOCTYPE html>
<html>
<head>
    <style>
        .cstm-table thead{
            background-color: #0b2240 !important;
            color:#fff !important;
        }
        .right-sub-total strong{
            padding: 0 0 0 135px;
        }
        .buss-text{
            font-weight: 600;
        }
        .terms p{
            font-weight: 600;
            margin-bottom: -4px;
        }
        .terms strong{
            font-weight: normal;
            font-size: 11px;
        }

        .inner-cstm-table td{
            border-top:none !important;
        }

        .bg{
            background-color: #0b2240 !important;
            color: #fff;
        }

        .Authorized {
            background-color: #0b2240;
            height: 10px;
            margin-top: 20px;
        }
        .cstm-nav{
            list-style: none;
            display: inline-flex;
        }
        .cstm-nav li{
            padding: 0 30px 0 0px;
        }
        .cstm-nav li a{
            color: black;
            font-weight: 600;
        }
        .invoice-to strong{
            color: #545151 !important;
        }

    </style>
</head>
<body>
<div class="invoice-to">
    <section class="content">
        <div class="row">
            <div id="mahir-icon-top" style=" display: block !important;
                                                margin: 0 auto !important;
                                                text-align: center !important;
                                                padding: 30px 0;
                                                background-color: #fff;
                                                border-radius: 60%;
                                                box-shadow: 0px 0px 8px #cbcbcb;
                                                height: 140px;
                                                width: 140px;">
                <img alt="" src="{{ public_path('assets/images/mahir-logo.png') }}" width="100px">
            </div>
        </div>

        <div class="def" style=" background-color: #0b2240;height: 50px;margin-top: 40px;">
            <div class="text-right">
                <span class="invice-text" style="font-size: 35px !important;font-weight: bold;color: #fff;display: block;text-align: center !important;">INVOICE</span>
            </div>
        </div>
    </section>

    <section class="invoice-to">
        <div class="row invoice-desc-sec" style="padding: 15px 15px 0;">
            <div class="col-md-8">
                <h4>{{ !is_null($order->user) && $order->user->name? $order->user->name : '-- -- --'}}</h4>
                <p style="margin-bottom: 1px;">
                    {{ !is_null($order->user) && $order->user->address? $order->user->address : '-- -- --'}}
                </p>
            </div>

            <div class="col-md-4">
                <div class="invoice-sub-total" style="float: right;margin: -8px 0px;">
                    <dl>
                        <dt>Token No:</dt>
                        <dd>{{!is_null($order)?$order->token:'--'}}</dd>
                        <dt>Date:</dt>
                        <dd>{{ $order->created_at ? Carbon\Carbon::parse($order->created_at)->format('h:s A Y-m-d') : '-- -- --'}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </section>

    <section class="table-sec">
        <div class="table-responsive">
            <table class="table table-hover table-bordered cstm-table" style="width:100%">
                <thead class="thead-dark2">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Service</th>
                    <th scope="col">Address</th>
                    <th scope="col">Area</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>{{! is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}</td>
                    <td>{{ !is_null($order)?$order->address:'---'}}</td>
                    <td>{{ !is_null($order->area)?$order->area->name:'---'}}</td>
                    {{--<td> {{ !is_null($order->area) && $order->area->id == $area->id ? 'selected' :''}}</td>--}}
                    <td>{{ !is_null($order->subService) ? $order->subService->price:'---'}}</td>

                </tr>
                </tbody>
            </table>
        </div>
    </section>

    <section class="sub-total" style="padding: 15px;">
        <div class="row">
            <div class="col-md-6">
                <div class="buss-text">
                    <p>Thank you for your business</p>
                </div>
                <div class="terms">
                    <p>Terms & Conditions</p>
                    <strong>Lorem ipsum, or lipsum as it is known as well as lorem ipsum,<br> is dummy text, graphic or web designs.</strong>
                </div>
            </div>

            <div class="col-md-6">
                <div class="right-sub-total" style="padding: 17px 0 0;font-weight: 600;float: right;margin: -32px -2px;">
                    <table class="table inner-cstm-table">
                        <tbody>
                        <tr>
                            <td>Sub Total:</td>
                            <td>Rs.{{ !is_null($order->subService) ? $order->subService->price:'---'}}  </td>
                        </tr>
                        <tr>
                            <td>Tax:</td>
                            <td>0.00%</td>
                        </tr>
                        <tr class="bg">
                            <td>Total:</td>
                            <td>Rs. {{ !is_null($order->subService) ? $order->subService->price:'---'}} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
        </div>

        <div class="Authorized">
            <div class="background">
                <span class="author-text"></span>
            </div>
        </div>

        <div class="row" style="text-align: center;">
            <ul class="cstm-nav">
                <li>
                    Phone: <a href="#">03049351502</a>
                </li>
                <li><span class="separator"> | </span></li>
                <li>
                    Address: <a href="#">Johar town Lahore</a>
                </li>
                <li><span class="separator"> | </span></li>
                <li>
                    Website <a href="http://www.mrmahir.com/">www.mrmahir.com</a>
                </li>
            </ul>
        </div>
        <button style="display: block;margin: 0 auto;padding: 6px 30px;" class="btn btn-primary"><a style="color: black;"href="{{route('printView',$order->id)}}">Print</a></button>
    </section>
</div>

</body>
</html>