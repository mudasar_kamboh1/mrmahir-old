@extends('layouts.master')
@section('content')
<style>
    #modalLoginForm .modal-header{
        background: #0f3e7c;
        color: white;
    }
    #modalLoginForm .modal-header span{
        color: white;
        position: relative;
        top: -10px;
        right: -5px;
    }
    #example1 tr th{
        background: #0f3e7c !important;
        color: white !important;
    }
</style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                            </div></div></div><div class="row">
                        <div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">

                            <!-- Trigger the modal with a button -->
                                <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title w-100 font-weight-bold">Area Add</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="row">
                                                <div class="modal-body mx-3" style="margin-bottom: -76px;">
                                                    <form action="{{route('Facebook.store')}}" method="post"  enctype="multipart/form-data">
                                                        {{--<form action="#" method="post">--}}
                                                        @csrf
                                            <div class="row">
                                                 <div class="col-6">
                                                      <div class="form-group">
                                                           <i class="fab fa-servicestack"></i>
                                                                <label data-error="wrong" data-success="right" for="defaultForm-email" > Name:</label><br>
                                                                <input type="text" name="name" id="defaultForm-email" class="form-control validate" style="display: block;margin: 0 auto;" required>
                                                                </div>
                                                      </div>

                                                      <div class="col-6">
                                                          <div class="form-group">
                                                                <i class="fas fa-image"></i>
                                                                <label data-error="wrong" data-success="right"   for="defaultForm-email"> Rating:</label><br>
                                                                <input type="number" class="defaultForm-email form-control" name="rating"  style="height: 36px;font-size: 14px;" required>
                                                          </div>
                                                      </div>
                                            </div>

                                            <div class="row">
                                                  <div class="col-6">
                                                          <div class="form-group" style="padding-top: 1%;font-size: 15px;cursor: pointer;">
                                                              <i class="fas fa-images"></i>
                                                              <label data-error="wrong" data-success="right" for="defaultForm-email"> Image:</label>
                                                              <input type="file" name="image" class="form-control" style="font-size: 13px;position: relative;padding: 5px 0px;" accept=".png, .jpg, .jpeg" required>
                                                          </div>
                                                  </div>

                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label for="usr">ALT:</label>
                                                        <input type="text" class="form-control" id="alt" name="alt" required/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                    <label data-error="wrong" data-success="right" for="defaultForm-email" > <i class="fab fa-servicestack"></i> Description:</label>
                                                    <textarea class="form-control" rows = "5" cols = "40" name = "description" placeholder="Enter your Description" style="resize: none;height:100px;margin: 0 auto;" required></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer d-flex justify-content-center" >
                                                  <button style="margin-bottom: 53px;padding: 6px 30px;background: #0f3e7c;color: white;border: none;" class="btn btn-default">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="" style="border: none !important;padding: 7px 40px;background: #0f3e7c;color: white;float: right;" class="btn btn-outline-success btn-rounded mb-4  " data-toggle="modal" data-target="#modalLoginForm">Create</a>
                                    {{--<a href="" style="border: none !important;padding: 7px 22px;background: #0f3e7c;color: white;float: right;" class="btn btn-outline-success btn-rounded mb-4  " data-toggle="modal" data-target="#modalLoginForm">Create</a>--}}
                                </div>
                                <tr role="row">
                                    <th class="sorting_asc  custom-card" aria-sort="ascending"
                                         style="width: 213.247px;">#</th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Name
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Description
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Rating
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Alt
                                    </th>
                                    <th class="sorting custom-card"  style="width: 262.135px;">Image
                                    </th>
                                    <th class="sorting  custom-card"  style="width: 262.135px;">Action
                                    </th>
                                </tr>
                                <tbody>
                                <?php
                                $x=1
                                ?>
                                @foreach( $Facebook as  $facebook_review)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1  custom-card">{{$x++}}</td>
                                        <td class=" custom-card">{{$facebook_review->name}}</td>
                                        <td class="custom-card">
                                               {{$facebook_review->description}}
                                                        </td>
                                        <td class="custom-card">{{$facebook_review->rating}}</td>
                                        <td class="custom-card">{{$facebook_review->alt}}</td>
                                        <td class=" custom-card">
                                            @if($facebook_review->image)
                                                <img src="{{asset('fbImages/'.$facebook_review->image)}}" class="step1:after" alt="" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;background-color: #065;padding: 7px;border-radius: 50%;">
                                            @else
                                                <img alt="" src="{{asset('fbImages/user.jpg')}} ">
                                            @endif
                                        </td>
                                        {{--<td class="custom-card">{{$facebook_review->alt}}</td>--}}

                                        <td class=" custom-card">
                                            <form action="{{route('Facebook.edit',$facebook_review->id)}}" method="get">
                                                <button style="    float: left; margin-right: 5px;" type="submit" class="btn btn-info js-sweetalert" title="edit"><i class="fa fa-edit"></i></button>
                                            </form>

                                            <form action="{{route('Facebook.destroy',$facebook_review->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button  type="submit"  onclick="return confirm('Are you sure')" class="btn btn-warning js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table></div></div>
{{--                    {{$area->links()}}--}}

                </div>

            </section>
        </div>

    </div>

@endsection

@section('javascript')
@stop
