@extends('layouts.master')
@section('content')
    <style>
        .facebook-text{
            font-weight: 600;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="background: #0f3e7c;color: white;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-center facebook-text">Facebook Edit</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <form style="padding: 60px 200px;" action="{{route('Facebook.update',$facebook->id)}}" method="post"  enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="projectinput1">First Name:</label>
                                    <input type="text" id="projectinput1" class="form-control" value="{{$facebook->name}}" placeholder="First Name" name="name">
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <i class="fas fa-image"></i>
                                    <label data-error="wrong" data-success="right"  for="defaultForm-email"> Rating:</label><br>
                                    <input type="number" class="defaultForm-email form-control" value="{{$facebook->rating}}" name="rating" style="margin-bottom: -19px;height: 36px; width: 100%;font-size: 14px;" required="">
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group" style="position: relative;top: 18px;">
                                    <i class="fas fa-image"></i>
                                    <label data-error="wrong" data-success="right"  for="defaultForm-email"> Alt:</label><br>
                                    <input type="text" class="defaultForm-email form-control" value="{{$facebook->alt}}" name="alt" style="margin-bottom: -19px;height: 36px; width: 100%;font-size: 14px;" required>
                                </div>
                            </div>


                            <div class="col-6">
                                <div class="form-group" style="padding: 22px 11px;">
                                    <label>Select File:</label>
                                    <label id="projectinput8" class="file center-block">

                                        @if($facebook->image)
                                            <img src="{{asset('fbImages/'.$facebook->image)}}" class="step1:after" alt="" style="height: 55px;width: 55px;display: block;margin: 0 auto;margin-top: -50px;">
                                        @else
                                            <img src="{{asset('serviceImg/user.jpg')}}" alt="" style="height: 50px;width: 50px;display: block;margin: 0 auto;margin-top: 5px;">
                                        @endif
                                        {{--<img src="http://127.0.0.1:8000/serviceIcon/1560511706.png" style="height: 55px;width: 55px;display: block;margin: 0 auto;margin-top: -50px;">--}}
                                        <input type="file"  name="image">
                                        <span class="file-custom"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="projectinput9">Description:</label>
                                    <textarea id="projectinput9" rows="3" cols="5" class="form-control" name="description" placeholder=""  style="width: 100%; resize: none;">{{$facebook->description}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mb-2" style="display: block;border: none;color: #fff;cursor: pointer;margin: 0 auto;padding: 5px 35px;margin-top: 26px;margin-right: 1px;background: #0f3e7c !important;" href="#">Update</button>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mb-2" style="border: none;color: #fff;cursor: pointer;padding: 5px 35px;margin-top: 25px;background-color: #dc3545;" href="#">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('javascript')

@stop

