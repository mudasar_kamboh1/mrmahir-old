{{-- Extends Layout --}}
<style>
    .chosen-single{
        height: 37px !important;
        padding-top: 7px !important;
        font-size: 15px !important;
    }
    .form-control{
        font-size: 15px !important;
        margin-bottom: 0px !important;
    }
    .btn-primary{
        margin: 0 auto !important;
        padding: 6px 30px !important;
        margin-top: 26px !important;
        margin-bottom: -60px !important;
        background: #0f3e7c !important;
        color: white !important;
    }
    .modal-header{
        text-align: center !important;
        margin:0 auto !important;
        font-weight: bold !important;
    }

    .btn-default{
        margin-top: 81px !important;
        display: block !important;
    }
    .modal-footer{
        margin-bottom: 41px !important;
        margin: 0 auto !important;
        display: block !important;
    }
    .btn-default{
        padding: 7px 29px !important;
    }
    .bg-categ{
        background: #0f3e7c !important;
        color: white !important;
    }
    .bg-categ span{
        position: relative;
        top: -12px;
        right: -5px;
        color: white;
    }
    .table thead{
        background: #0f3e7c !important;
        color: white !important;
    }
    .btn-info{
        background: #0f3e7c !important;
        color: white !important;
        border-color: #0f3e7c !important;
    }

</style>



@extends('layouts.master')

{{-- Page Title --}}
@section('page-title', 'Category')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

@section('head-extras')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

            </div>
    <div style="background-color: #fff; padding: 15px 15px 0" xmlns="http://www.w3.org/1999/html">

        <div class="row">
            <div class="col-md-4">
                <div class="box-tools">
                </div>
            </div>
            <div class="col-md-8 text-right">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add New SubCategory</button>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal1">Add New Category</button>
            </div>
        </div>

        <br>
        <div class="table-responsive list-records">
            <table class="table table-hover table-bordered">
                <thead>
                <tr><th>#</th>
                    <th>Name</th>
                    <th>Sub Category</th>
                    <th>meta title</th>
                    <th>meta description</th>
                    <th>meta keywords</th>
                    <th style="width: 120px;">Actions</th>
                </tr></thead>
                <tbody>
                @foreach($data as $name)
                <tr>
                    <td>1</td>
                    <td>{{ $name->name }}</td>
                    <td>
                        @if(count($name->subcategories->pluck('name')) > 0)
                            @foreach($name->subcategories->pluck('name') as $value)
                                {{$value}},
                            @endforeach
                        @endif
                    </td>
                    <td>{{ $name->meta_title }}</td>
                    <td>{{ $name->meta_description }}</td>
                    <td>{{ $name->meta_keywords }}</td>


                    <td>
                        <div class="btn-group">
                            <form action="{{ route('categories.show',$name->id) }}" method="get">
                                @csrf
                                <button  style="position: relative !important;top: -14px !important;" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                            </form>
                            <form id="formDeleteModel_1" action="{{ route('categories.destroy',$name->id) }}" method="POST" class="form-inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button style="position: relative !important;top: 12px !important;padding: 6px 31px;" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                        <!-- Delete Record Form -->

                    </td>
                </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="bg-categ">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Add New Sub-Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <div class="box-tools col-md-12 text-center">
                            <form class="form input-group input-group-sm margin-r-5 " role="form" method="POST" action="{{ route('sub_categories.store') }}">
                                @csrf
                                <div class="input-group input-group-sm margin-r-5 pull-left">
{{--                                    {{ dd($data) }}--}}
                                    <div class="col-md-6">
                                        <label style="float: left">Sub-Category:</label>
                                    <input type="text" name="name" class="form-control" value="" placeholder="Sub Category Name">
                                    </div>
                                    <div class="col-md-6">
                                        <label style="float: left">Main Category:</label>
                                    <select name="category_id" class="form-control option-input" style="width: 100%;" >

                                        <option>Select</option>
                                        @foreach($data as $name1)
                                            <option value="{{ $name1->id }}"> {{ $name1->name }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <input type="submit" class="btn btn-sm btn-primary" style="float: right;margin-top: 10px;">
                                </div>
                               </form>
                        </div>
                        </div>
                    </div>
                    <br>
                    <div class="modal-footer" style="border-top: 0;">
                    </div>
                </div>
            </div>
        </div>
        <!-- The Modal -->

        <!-- Modal -->
        <div id="myModal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="bg-categ">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Add New Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                        <div class="modal-body">
                            <form class="form input-group input-group-sm margin-r-5 " role="form" method="POST" action="{{ route('categories.store') }}">
                            @csrf
                            <div class="col-md-6">
                            {{--<form  role="form" method="POST" action="{{ route('categories.store') }}">--}}
                            {{--@csrf--}}
                                <div class="form-group" >
                                    <label for="usr">Category:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Add New Category" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="usr">Meta Title:</label>
                                    <input type="text" class="form-control" id="meta_title" name="meta_title" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" style="padding: 12px 0;">
                                    <label for="usr">Meta Description:</label>
                                    <input type="text" class="form-control" id="meta_description" name="meta_description" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" style="padding: 12px 0;">
                                    <label for="usr">Meta keywords:</label>
                                    <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                                </div>
                            </div>

                            <div class="modal-footer" style="display: block;margin: 0 auto;">
                            </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="modal-footer" style="position: relative;top: -20px;right: 89px;">
                                    <button style="background: #0f3e7c !important;border:none;color: white !important;padding: 6px 30px;" type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="modal-footer" style="position: relative;top: -77px;height: 38px;left: -122px;">
                                    <button style="background: #ce171f !important;color: white;padding: 6px 30px;" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>

            </div>
        </div>
    </div>
        </div>
    </div>

            @endsection

            @section('footer-extras')

                <script type="application/javascript">
                    $(document).ready(function(){
                        $("#myInput").on("keyup", function() {
                            var value = $(this).val().toLowerCase();
                            $("#myTable tr").filter(function() {
                                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                            });
                        });
                    });

                    $('.order-detail').on('click',function () {
                        getData($(this).data('order-id'));
                    });

                    function getData(id) {
                        $('.submit-order-detail').html('');
                        $.ajax({
                            url: "{{url('admin/orders/details')}}/"+id,
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            success: function(result){

                                $('.submit-order-detail').html(result);
                            }
                        });
                    }


                    $(document).on('submit','#update-order-details',function (e) {
                        e.preventDefault();
                        let $this = $(this);
                        $.ajax({
                            url: $this.attr('action'),
                            type: "POST",
                            data :  $this.serialize(),
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            success: function(result){
                                if(result.status){
                                    $('#view-detail').modal('hide');

                                    swal({
                                        title: 'Success',
                                        text: result.msg ,
                                        icon: "success",
                                        button: "OK",
                                    });
                                    location.reload();
//                       getData($('input[name=order_id]').val());
                                }else{
                                    swal({
                                        title: 'Error',
                                        text: result.msg ,
                                        icon: "error",
                                        button: "OK",
                                    });
                                }
                            },
                            error: function (request, status, error) {
                                let json = $.parseJSON(request.responseText);
                                $.each(json.errors, function(key, value){
                                    toastr.warning(value);
                                });
                            }
                        });
                    });
                </script>
@endsection
