{{-- Extends Layout --}}
@extends('layouts.master')

{{-- Page Title --}}
@section('page-title', 'SubCategory')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

@section('content')
    <style>
        .subcat{
            background: #0f3e7c !important;
            color: white !important;
            padding: 10px 0;
        }
        .m-0{
            font-weight: 600;
            color: white;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row subcat">
                    <div class="text-dark" style="display: block;margin: 0 auto;">
                        <h1 class="m-0">Edit Sub-Category</h1>
                    </div>
                </div><!-- /.container-fluid -->
            </div>
    <div style="background-color: #fff;" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="col-md-4">
                <div class="box-tools">
                </div>
            </div>
        </div>

        <div class="table-responsive list-records">
            <form class="form" role="form" method="POST" action="{{ route('sub_categories.update',$data->id) }}">
            @csrf
            {{ method_field('PUT') }}
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group" >
                                    <label for="usr">SubCategory:</label>
                                    <input value="{{ $data->name }}" type="text" name="name" class="form-control" placeholder="Add New Category" required>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <label for="usr">Meta Title:</label>
                                    <input  value="{{ $data->meta_title }}" type="text" class="form-control" id="meta_title" name="meta_title" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="usr">Meta Description:</label>
                                    <input  value="{{ $data->meta_description }}" type="text" class="form-control" id="meta_description" name="meta_description" />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="usr">Meta keywords:</label>
                                    <input value="{{ $data->meta_keywords }}" type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="display: block;margin: 0 auto;">
                        <button style="background: #0f3e7c !important;border:none;color: white !important;padding: 6px 30px;" type="submit" class="btn btn-primary">Save</button>
                        <button style="background: #ce171f !important;color: white;padding: 6px 30px;" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
        </div>
    </div>
@endsection

@section('footer-extras')

@endsection
