{{-- Extends Layout --}}
<style>
    .modal-header .sub{
        font-weight: bold !important;
        text-align: center !important;
        margin:0 auto !important;
        padding: 0 101px;
    }
    .btn-primary{
        margin:0 auto !important;
        padding: 8px 35px !important;
    }

    .chosen-single{
        height: 36px !important;
        padding-top: 5px !important;
        font-size: 16px !important;
    }
    #separt-bg{
        background: #0f3e7c !important;
        color: white !important;
    }
    #separt-bg span{
        position: relative;
        top: -12px;
        right: -8px;
        color: white;
    }
    .table thead{
        background: #0f3e7c !important;
        color: white !important;
    }

</style>

@extends('layouts.master')

{{-- Page Title --}}
@section('page-title', 'SubCategory')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

@section('head-extras')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div><br><!-- /.col -->


                </div>
            </div>
    <div style="background-color: #fff; padding: 15px 15px 0" xmlns="http://www.w3.org/1999/html">

        <div class="row">
            <div class="col-md-4">
                <div class="box-tools">
                </div>
            </div>
            <div class="col-md-8 text-right">
                <button style="background: #0f3e7c !important; color: white !important;border: none !important;" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add New SubCategory</button>
            </div>
        </div>

        <br>
        <div class="table-responsive list-records">
            <table class="table table-hover table-bordered">
                <thead>
                <tr><th>#</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Meta title</th>
                    <th>Meta description</th>
                    <th>Meta keywords</th>
                    <th style="width: 120px;">Actions</th>
                </tr></thead>
                <tbody>

                @foreach($subCats as $name)
                    <tr>
                        <td>1</td>
                        <td>{{ $name->name }}</td>
                        <td>{{!is_null($name->category)? $name->category->name:'---' }}</td>
                        <td>{{ $name->meta_title }}</td>
                        <td>{{ $name->meta_description }}</td>
                        <td>{{ $name->meta_keywords }}</td>

                        <td>
                            <div class="btn-group">
                                <form action="{{ route('sub_categories.show',$name->id) }}" method="get">
                                    @csrf
                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                                </form>
                                <form id="formDeleteModel_1" action="{{ route('sub_categories.destroy',$name->id) }}" method="POST" class="form-inline">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button style="padding: 8px 35px;" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>

                                </form>
                            </div>
                            <!-- Delete Record Form -->

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form class="form" role="form" method="POST" action="sub_categories">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add New Category  </h4>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-group" >
                            <label for="usr">Category :</label>
                            <input type="text" name="name" class="form-control" placeholder="Add New Category" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta Title:</label>
                            <input type="text" class="form-control" id="meta_title" name="meta_title" />
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta Description:</label>
                            <input type="text" class="form-control" id="meta_description" name="meta_description" />
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta Title:</label>
                            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" id="separt-bg">
                    <h4 class="sub">Add New SubCategory</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="box-tools col-md-12 text-center">
                            <form class="form input-group input-group-sm margin-r-5 " role="form" method="POST" action="{{ route('sub_categories.store') }}">
                                @csrf
                                <div class="input-group input-group-sm margin-r-5 pull-left">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label style="float: left" for="usr">Sub Category:</label>
                                        <input type="text" name="name" class="form-control" value="" placeholder="Sub Category Name" style="width: 100%;margin-bottom: 8px;">
                                        </div>
                                        </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label style="float: left" for="usr">Main Category:</label>
                                        <select name="category_id" class="form-control option-input" style="width: 98%;" >

                                        <option>Select</option>
                                        @foreach($data as $name1)
                                            <option value="{{ $name1->id }}"> {{ $name1->name }}</option>
                                        @endforeach

                                    </select>
                                    </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="float: left" for="usr">Meta Title:</label>
                                            <input type="text" class="form-control" id="meta_title" name="meta_title" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="float: left" for="usr">Meta Description:</label>
                                            <input type="text" class="form-control" id="meta_description" name="meta_description" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="float: left"  for="usr">Meta keywords:</label>
                                        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                                    </div>
                                </div>
                                </div>
                                <div class="sub-category-btn" style="display: block;margin: 0 auto;margin-top: 22px;margin-bottom: -53px;">
                                    <input style="background: #0f3e7c;border:none;color: white;" type="submit" class="btn btn-sm btn-primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br>
                <div class="modal-footer" style="border-top: 0;">
                </div>
            </div>

        </div>
    </div>
        </div>
    </div>

        @endsection

        @section('footer-extras')

@endsection
