<!-- footer section -->
<section class="footer">
    <div class="container-fluid">
        <div class="footer-nav">
            <ul>
                <li> <a href="{{ url('/') }}">Home</a></li>
                @php $services = \App\Service::get(); @endphp
                @if(count($services) > 0)
                    @foreach( $services as $k=>$service)
                        @if($k==1)
                            @break
                        @endif
                        <div class="dropup">
                            <li class=""><a class="drop-btn" href="{{ url('services/')}}">Services</a></li>

                            <div class="dropup-content">
                                @foreach( $services as $k=>$service)
                                    @if($k==4)
                                        @break
                                    @endif
                                    <a href="{{ url('services/'.$service->slug)}}">{{$service->title}}</a>

                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @endif

                <li> <a href="{{ url('rate-list') }}">Rate list</a></li>
                <li> <a href="{{ url('about-us') }}">About Us</a></li>
                <li> <a href="{{ url('portfolio') }}">Portfolio</a></li>
            </ul>
        </div>
    </div>
</section>
<!--// footer section-->

<!-- Whatsapp live chat -->
<a class="whatsapp-link" href="https://api.whatsapp.com/send?phone=+923096661919&text=Hi Mr.Mahir!" target="_blank" style="position: fixed;right: 27px;bottom: 43px;z-index: 999;"><img src="{{ asset('assets/images/whatsapp-512.png') }}" class="img-circle" alt="whatsapp-icon"  width="60px"></a>
<!--// Whatsapp live chat -->

{{--Social fixed side bar--}}
<div id="fixed-social">
    <div>
        <a href="https://www.facebook.com/teammahir/" class="fixed-facebook" target="_blank"><i class="fab fa-facebook-f"></i> <span>Facebook</span></a>
    </div>
    <div>
        <a href="https://twitter.com/MahirTeam" class="fixed-twitter" target="_blank"><i class="fab fa-twitter"></i> <span>Twitter</span></a>
    </div>
    <div>
        <a href="https://www.instagram.com/teammahir/" class="fixed-instagram" target="_blank"><i class="fab fa-instagram"></i> <span>Instagram</span></a>
    </div>
    <div>
        <a href="https://www.youtube.com/channel/UCICIsVRC6pNmqLRYilG5bNQ" class="fixed-youtube" target="_blank"><i class="fab fa-youtube"></i> <span>Youtube</span></a>
    </div>
</div>

