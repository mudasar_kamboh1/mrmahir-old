@yield('meta')
<style>
    body::-webkit-scrollbar { width: 0 !important; }

    .chosen-container-single .chosen-single {
        padding-left: 10px !important;
        font-size: 14px !important;
        height: 40px !important;
        border-radius: 50px !important;
        background: #fff !important;
        border-color: #ced4da !important;
        color: #495057 !important;
    }

    .chosen-container-single .chosen-single span {
        line-height: 40px !important;
    }
    .modal-dialog .modal-content .modal-header{
        background-color: #fff;
    }
    button.close{
        background-color: 0;
    }
    .custom-navbar .cstm-nav li .cstm-dropdown{
        box-shadow: none;
    }
    .custom-navbar .cstm-nav li{
        padding-left: 0px !important;
    }

    @media screen and (max-width:767px){
        .header-btn{
            margin-top:0;
        }
    }

</style>

<main class="home-body">
<!--    header section-->
<section class="top-header-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="text-left header-btn">
                    @guest
                    <button class="signup-signin">
                        <a href="{{ url('register') }}">SignUp/ </a>
                        <a href="{{url('login')}}"> SignIn</a>
                    </button>
                    @endguest
                    @auth
                    <div class="dropdown" id="drop" style="display: none;">
                        <img src="{{ asset('assets/images/avatar.png') }}" alt="" style="width:50px;height:50px;">
                        <div class="dropdown-content">
                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </div>
                    @endauth

                    <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav cstm-nav">
                                @auth
                                <li class="nav-item">
                                    <div class="dropdown show">
                                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/images/avatar.png') }}" alt="" style="width:50px;height:50px;">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                        </div>
                                    </div>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-center mahir-icon">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="text-right header-btn">
                    <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--trending section-->
<section class="trending-serv-section home-tab-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <section class="service-section2">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Trending Services:</legend>
                        <div class="row scheduler-row">
                            <div class=" p-0 cstm-width">
                                <div class="sub-service-list">
                                    <div class="cons">
                                        <div id="mytreadmill" class="treadmill">
                                            @foreach($trendingServices as $trendingService)
                                                @php
                                                    $imagePath = !is_null($trendingService->SubServices) && $trendingService->SubServices->service ? $trendingService->SubServices->service->icon : '';
                                                @endphp
                                                <div class="treadmill-unit active" data-service="{{!is_null($trendingService->SubServices)? $trendingService->SubServices->id:''}}" data-price="{{!is_null($trendingService->SubServices)? $trendingService->SubServices->price:''}}/-" data-price_comment="{{!is_null($trendingService->SubServices)? $trendingService->SubServices->price_comment:''}}">
                                                    <div class="row" style="margin: 10px !important;">
                                                    <div class="col-sm-2 col-2 p-0">
                                                    <img alt="{{!is_null($trendingService->SubServices) ?$trendingService->SubServices->alt:'--'}}" src="{{ asset('serviceIcon/'.$imagePath) }}">
                                                    </div>
                                                    <div class="col-sm-10 col-10">
                                                    <h2 style="font-size:17px;cursor: pointer;">{{!is_null($trendingService->SubServices)?$trendingService->SubServices->title:'--'}}</h2>
                                                    </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=" p-0 register-form" style="display: none;">
                                <div class="">
                                    <form class="Order-form" id="instanceOrder">
                                        <input type="hidden" id="sub-service-field" name="subService">
                                        <div class="form-group">
                                            <input class="form-input" type="text" name="name" placeholder="Name" aria-describedby="name-format" required="">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-input" placeholder="Phone Number" type="text" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" required="">
                                        </div>
                                        <div class="form-group">
                                            <select data-placeholder="Choose Services..." id="area_id" class="chosen- form-input select-area" tabindex="2" name="area" required  data-height="40px">
                                                <option value="">Select Areas</option>

                                                @foreach($areas as $area)
                                                    <option value="{{ $area->id }}">{{ $area->name }}</option>
                                                @endforeach
                                            </select>
                                            <input class="form-input service-name" type="hidden" placeholder="Service" aria-describedby="name-format" required="">
                                        </div>
                                        <div class="Price-box">
                                            <strong><span id="price_comment"></span></strong>
                                            <h3><p><span id="price" ></span></p></h3>
                                            <button class="button btn-Book-now">Book Now</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </section>
            </div>
            <div class="col-md-6">
                <div id="demo" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @if(count($posts) > 0)
                        @foreach($posts as $k => $post)
                            <div class="carousel-item {{$k == 0 ? 'active':''}}">
                        @if($post->image)
                            <a href="{{ url('portfolio')}}"><img src="{{ asset('assets/uploads/'.$post->image) }}" alt="{{$post->alt}}" class="img-fluid"></a>
                        @else
                            <img alt="" src="{{ asset('assets/images/youtube.jpg') }}" class="img-fluid">
                        @endif
                            </div>
                        @endforeach
                    @endif
                    @if(count($videos)>0)
                        @foreach( $videos as $k=>$video)
                                <div class="carousel-item ">
                                <a href="javascript:void(0)" onclick="$('#ytplayer1').attr('src','{{$video->url}}')" data-toggle="modal" data-target="#myModal" ><img alt="" src="{{ asset('assets/uploads/'.$video->image) }}" class="img-fluid"></a>
                                </div>
                        @endforeach
                    @endif
                    <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                </div>

                <div class="row">
                    <div class="form">
                        <h4>Call Us <strong class="phone-number">0309 666 1919</strong> Or Request for a Call</h4>
                        <form class="request-form" id="formsubmitbutton">
                                <div class="form-group">
                                    <input class="cstm-input-control" type="text" value="{{ old('name') }}" placeholder="Full Name" name="name" aria-describedby="name-format" required="" pattern="[a-zA-Z ]+" title="Name consist on Alphabets only">
                                </div>
                                <div class="form-group">
                                    <input class="cstm-input-control" placeholder="Phone Number" type="text" value="{{ old('mobile') }}" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" required="">
                                </div>
                                <div class="form-group">
                                    <button class="button btn-Request-a-Call-back">Request a Call back</button>
                                </div>
                        </form>
                    </div>
                </div>

                <div class="sub-service-block">
                    <div class="row">
                        <div class="services-widget service-desktop-hide">
                            @foreach( $services as $k=> $service)
                             @if($k==4)
                                 @break
                                @endif
                            <div class="col-md-3">
                                <a href="{{ url('services/'.$service->slug)}}">
                                <div class="service-content">
                                    <h6>{{$service->title}}</h6>
                                </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="sub-service-block desktop-view-hide" style="display: none;">
                    <div class="row">
                        <ul class="services-widget-mobile service-desktop-hide">
                            @foreach( $services as $k=> $service)
                                @if($k==4)
                                    @break
                                @endif
                                <li class="service-content">
                                    <a href="{{ url('services/'.$service->slug)}}">
                                        <h6>{{$service->title}}</h6></a>
                                </li>

                                @endforeach
                        </ul>
                        </div>
                    </div>
                </div>
                </div>
           </div>
        </div>
</section>
<!-- // trending section-->



    {{--trending tab view div--}}
    <section class="trending-serv-section home-tab-show">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="service-section2">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Trending Services:</legend>
                            <div class="row scheduler-row">
                                <div class=" p-0 cstm-width">
                                    <div class="sub-service-list">
                                        <div class="cons">
                                            <div id="mytreadmill" class="treadmill">
                                                @foreach($trendingServices as $trendingService)
                                                    @php
                                                        $imagePath = !is_null($trendingService->SubServices) && $trendingService->SubServices->service ? $trendingService->SubServices->service->icon : '';
                                                    @endphp
                                                    <div class="treadmill-unit active" data-service="{{!is_null($trendingService->SubServices)? $trendingService->SubServices->id:''}}" data-price="{{!is_null($trendingService->SubServices)? $trendingService->SubServices->price:''}}/-" data-price_comment="{{!is_null($trendingService->SubServices)? $trendingService->SubServices->price_comment:''}}">
                                                        <div class="row" style="margin: 10px !important;">
                                                            <div class="col-sm-2 col-2 p-0">
                                                                <img alt="{{!is_null($trendingService->SubServices) ?$trendingService->SubServices->alt:'--'}}" src="{{ asset('serviceIcon/'.$imagePath) }}">
                                                            </div>
                                                            <div class="col-sm-10 col-10">
                                                                <h2 style="font-size:17px;">{{!is_null($trendingService->SubServices)?$trendingService->SubServices->title:'--'}}</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="p-0 register-form" style="display: none;">
                                    <div class="">
                                        <form class="Order-form" id="instanceOrder">
                                            <input type="hidden" id="sub-service-field" name="subService">
                                            <div class="form-group">
                                                <input class="form-input" type="text" name="name" placeholder="Name" aria-describedby="name-format" required="">
                                            </div>

                                            <div class="form-group">
                                                <input class="form-input" placeholder="Phone Number" type="text" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" minlength="11" required="">
                                            </div>
                                            <div class="form-group">
                                                <select data-placeholder="Choose Services..." id="area_id" class="chosen- form-input select-area" tabindex="2" name="area" required  data-height="40px">
                                                    <option value="">Select Areas</option>

                                                    @foreach($areas as $area)
                                                        <option value="{{ $area->id }}">{{ $area->name }}</option>
                                                    @endforeach
                                                </select>
                                                <input class="form-input service-name" type="hidden" placeholder="Service" aria-describedby="name-format" required="">
                                            </div>
                                            <div class="Price-box">
                                                <strong><span id="price"></span></strong>
                                                <p id="price_comment">Price may vary after inspection</p>
                                                <h3><p><span id="price_comment" ></span></p></h3>
                                                <button class="button btn-Book-now">Book Now</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </section>
                </div>

                <div class="col-md-12">
                    <div id="demo2" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            @if(count($posts) > 0)
                                @foreach($posts as $k => $post)

                                    <div class="carousel-item {{$k==0?'':''}}">
                                        @if($post->image)
                                            <a href="{{ url('portfolio')}}"><img src="{{ asset('assets/uploads/'.$post->image) }}" alt="{{$post->alt}}" class="img-fluid"></a>
                                        @else
                                            <img src="{{ asset('assets/images/youtube.jpg') }}" alt="" class="img-fluid">
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                            @if(count($videos)>0)
                                @foreach( $videos as $video)
                                    <div class="carousel-item active">
                                        <a href="javascript:void(0)" onclick="$('#ytplayer1').attr('src','{{$video->url}}')" data-toggle="modal" data-target="#myModal" ><img src="{{ asset('assets/uploads/'.$video->image) }}" alt="" class="img-fluid"></a>
                                    </div>
                            @endforeach
                        @endif
                        <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo2" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo2" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>

                        <div class="row">
                            <div class="form">
                                <h4>Call Us <strong class="phone-number">+923096661919</strong> Or Request for a Call</h4>
                                <form  id="formsubmitbutton" class=" ">
                                <form class="request-form" id="formsubmitbutton">

                                    @csrf
                                    <div class="form-group">
                                        <input class="cstm-input-control" placeholder="Phone Number" type="text" value="{{ old('mobile') }}" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" minlength="11" required="">
                                        <input class="cstm-input-control" type="text" value="{{ old('name') }}" placeholder="Full Name" name="name" aria-describedby="name-format" required="" pattern="[a-zA-Z ]+" title="Name consist on Alphabets only">
                                    </div>
                                    <div class="form-group">
                                        <button class="button btn-Request-a-Call-back">Request a Call back</button>
                                    </div>
                                </form>
                                </form>
                            </div>
                        </div>

                        <div class="sub-service-block">
                            <div class="row">
                                <div class="services-widget service-desktop-hide">
                                    @foreach( $services as $k=> $service)
                                        @if($k==4)
                                            @break
                                        @endif
                                        <div class="col-md-3">
                                            <a href="{{ url('services/'.$service->slug)}}">
                                                <div class="service-content">
                                                    <h6>{{$service->title}}</h6>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="sub-service-block" style="display: none;">
                            <div class="row">
                                <div class="services-widget service-desktop-hide">
                                    <div class="row">
                                        @foreach( $services as $k=> $service)
                                            @if($k==4)
                                                @break
                                            @endif
                                                <div class="col-sm-3">
                                                    <div class="service-content">
                                                        <a href="{{ url('services/'.$service->slug)}}">
                                                            <h6>{{$service->title}}</h6></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--end services--}}

<!-- The Modal screen of video on click-->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <iframe  data-target="#myModal" id="ytplayer1" src="" width="100%" height="470" frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!--// Modal footer -->
<!-- footer section -->

    @include('web.partials.footer')
</main>

