<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

<!-- Bootstrap core CSS -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="{{ asset('assets/css/mdb.min.css') }}" rel="stylesheet">
<!-- style css -->
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/lightgallery.min.css') }}" rel="stylesheet">

<!-- owl carousel css -->

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/prism.css') }}">