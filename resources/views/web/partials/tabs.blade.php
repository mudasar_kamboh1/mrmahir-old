<!-- tabs section -->

<section class="tab-section">
    <div class="container">
        <ul class="nav nav-tabs cstm-tabs" id="myTab" role="tablist">

            @foreach($services as $k => $service)
                @if ($k == 4)
                    @break
                @endif
                <li class="nav-item">
                    <a class="nav-link text-center {{ $k == 0 ? 'active' : '' }} services-link" data-service="{{ $service->id }}" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        <img src="{{ asset('serviceIcon/'.$service->icon)}}" style="height: 40px;width: 40px">
                        <span class="clearfix">{{ $service->title }}</span></a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="cstm-tab-content" id="myTabContent">
        <div class="" >
            <div class="container">
                <form class="form-inline service-form" id="instanceOrder">
                    <div class="row margin-0">
                        <div class="col-md-3 padding-0">
                            <div class="form-group left-inner-addon">
                                <i class="fas fa-map-marker-alt"></i>
                                <select data-placeholder="Select Sub Service" class="chosen-select sub-services form-control cstm-control" name="subService" tabindex="2" required>
                                    <option value="" disabled="disabled" selected>Select Sub Service</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 padding-0">
                            <div class="form-group left-inner-addon">
                                <i class="fas fa-directions"></i>
                                <select data-placeholder="Select Your area" class="chosen-select form-control cstm-control" name="area" tabindex="2" required>
                                    <option value="" disabled="disabled"  selected>Select Your Area</option>
                                    @foreach($areas as $area)
                                        <option value="{{ $area->id }}">{{ $area->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 padding-0">
                            <div class="form-group left-inner-addon">
                                <i class="far fa-user"></i>
                                <input type="text" class="form-control cstm-control" name="name" id="name"  placeholder="Name" pattern="[a-zA-Z][a-zA-Z ]+[a-zA-Z]$" title="Name consist on letters only"  maxlength="30" required>
                            </div>
                        </div>
                        <div class="col-md-2 padding-0">
                            <div class="form-group left-inner-addon">
                                <i class="fas fa-mobile-alt"></i>
                                <input type="text" class="form-control cstm-control" name="mobile" id="mobile" pattern="\d+" placeholder="Phone Number" title="Please add a valid Number"  maxlength="11" required>
                            </div>
                        </div>
                        <div class="col-md-2 pr-0">
                            <button type="submit"  class="btn cstm-btn">Get Your Services</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>