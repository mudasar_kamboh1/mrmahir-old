@guest
<button class="signup-signin">
    <a style="color: #fff;font-weight: 300;" href="{{ url('register') }}">SignUp/ </a>
    <a style="color: #fff;font-weight: 300;" href="{{url('login')}}"> SignIn</a>
</button>
@endguest
@auth
<div class="dropdown" id="drop" style="display: none;">
    <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
    <div class="dropdown-content">
        <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
        {{--@if($user->hasRole('Staff'))--}}
            {{--<a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>--}}
        {{--@endif--}}
        <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
        <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
    </div>
</div>
@endauth