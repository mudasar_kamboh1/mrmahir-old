@include('web.partials.header')

<!-- banner section -->

<section class="banner-section">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ asset('assets/images/inner-banner.png') }}" alt="">
                <div class="carousel-caption cstm-caption text-left d-none d-md-block">
                    <h1>Mr. Mahir at Your Home Service </h1>
                    <p>Mahir Keeps Your Home Tip Top</p>
                </div>
            </div>
        </div>
    </div>
</section>