<div class="col-md-4">
    <div class="call-section">
        <div class="freeQuote">
            <p>Call for a free quote<br>
                <strong>0309 666 1919</strong>
        </div>

        <div class="callbackForm">
            <p>Let us call you...</p>
            <form class="call-us-form" id="formsubmitbutton">
            @csrf
                <div class="row margin-0">
                    <div class="col-sm-6 padd-l-0">
                        <div class="form-group">
                            <input type="text" class="form-control cstm-control" name="name" value="{{ old('name') }}" placeholder="Your Name" required>
                        </div>
                    </div>
                    <div class="col-sm-6 padd-l-0">
                        <div class="form-group">
                            <input class="form-input" placeholder="Phone Number" type="text" name="mobile" value="{{ old('mobile') }}" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" minlength="11" required="" style="border-radius: 3px;">
                        </div>
                    </div>
                    <button  class="btn btn-submit my-4" type="submit" style="width:100%;">Request A Call Back</button>
                </div>
            </form>
        </div>
    </div>
</div>
@section('script')

    @endsection