<!-- My order Modal -->
<div class="modal fade" id="otp-modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class=" modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="order-track-section">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1>Verify Your Mobile Number</h1>
                    <form action="#" class="order-form" id="check-otp-valid">
                        @csrf
                        <div id="contact-block">
                            <div class="form-group">
                                <label for="name" class="form-label">Enter OTP(One Time Password):</label>
                                <input type="text" class="form-control" id="otp-code" name="code" placeholder="Enter OTP Code" required="required">
                                <input type="hidden" class="modal-mobile" name="mobile" value="" required="required">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-btn">Verify</button>
                                <hr>
                                <span id="time" style="display: block;"></span>
                                <button type="button" class="btn btn-default otp-resend-btn">Resend</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>