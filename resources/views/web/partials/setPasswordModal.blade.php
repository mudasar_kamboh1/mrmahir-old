<!-- My order Modal -->
<div class="modal fade" id="setUserInfo-modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class=" modal-dialog modal-sm">
        <div class="modal-content cstm-modal-content">
            <div class="modal-body">
                <div class="order-track-section">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1>Please Update Your Info</h1>
                    <form action="#" class="order-form" id="saveProfileInfo">
                        @csrf
                        <div id="contact-block">
                            <div class="form-group">
                                <label for="name" class="form-label">Name:</label>
                                <input type="text" class="form-control cstm-control" name="name" placeholder="Name" required="required">
                            </div>
                            <div class="form-group">
                                <label for="name" class="form-label">Password:</label>
                                <input type="text" class="form-control cstm-control"  name="password" placeholder="Password" required="required">
                            </div>
                            <div class="form-group">
                                <label for="name" class="form-label">Confirm Password:</label>
                                <input type="text" class="form-control cstm-control" name="cpassword" placeholder="Confirm Password" required="required">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-btn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>