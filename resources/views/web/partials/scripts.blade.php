<!-- JQuery -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ asset('assets/js/mdb.min.js') }}"></script>


<script src="{{ asset('assets/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/js/prism.js') }}"></script>
<script src="{{ asset('assets/js/init.js') }}"></script>
<!-- owl carousel JavaScript -->
<script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
{{--<script src="{{ asset('assets/js/jquery.totemticker.js') }}"></script>--}}
<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/super-treadmill.js') }}"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/datatables.js') }}"></script>

@toastr_js
@toastr_css
@toastr_render
<script>
    $(function(){
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif
    });
</script>
@include('sweetalert::alert')
<script>
    $(document).ready(function() {
        @if(Auth::check() && !Auth::user()->password)
        $('#setUserPassword-modal').modal('show');
                @endif

        var a = 0;
        $(window).scroll(function () {
            if ($('#counter').length) {
                var oTop = $('#counter').offset().top - window.innerHeight;
                if (a == 0 && $(window).scrollTop() > oTop) {
                    $('.counter-value').each(function () {
                        var $this = $(this),
                            countTo = $this.attr('data-count');
                        $({
                            countNum: $this.text()
                        }).animate({
                                countNum: countTo
                            },
                            {
                                duration: 2000,
                                easing: 'swing',
                                step: function () {
                                    $this.text(Math.floor(this.countNum));
                                },
                                complete: function () {
                                    $this.text(this.countNum);
//alert('finished');
                                }
                            });
                    });
                    a = 1;
                }
            }
        });


        $(document).ready(function () {
            $("#clients-slider").owlCarousel({
                items: 3,
                margin: 10,
                itemsDesktop: [1000, 1],
                itemsDesktopSmall: [990, 2],
                itemsTablet: [767, 1],
                pagination: false,
                navigation: true,
                autoPlay: true,
                loop: true,
            });

        });


        $(document).ready(function () {
            $("#clients-slider").owlCarousel({
                items: 1,
                margin: 10,
                itemsDesktop: [1000, 1],
                itemsDesktopSmall: [990, 1],
                itemsTablet: [767, 1],
                pagination: false,
                navigation: true,
                autoPlay: true,
                loop: true,
            });
        });

        $('.dtBasicExample').DataTable();

        $("#service-slider").owlCarousel({
            items: 3,
            margin: 10,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [990, 2],
            itemsTablet: [767, 1],
            pagination: false,
            navigation: true,
            autoPlay: true,
            loop: true,
        });
        $("#business-slider").owlCarousel({
            items: 3,
            margin: 10,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [990, 2],
            itemsTablet: [767, 1],
            pagination: true,
            navigation: false,
            autoPlay: true,
            loop: true,
        });


        $('.services-link').on('click', function () {
            var service = $(this).data('service');
            $('.sub-services option').remove();
            $('.sub-services').append('<option value="" disabled="disabled" selected>Select Sub Service</option>');
            $.ajax({
                type: 'GET',
                url: "{{ url('getSubServices') }}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        $('.sub-services').append('<option value="' + row.id + '" class="list">' + row.title + '</option>');
                    });
                    $('.sub-services').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function (key, value) {
                        toastr.error(value);
                    });
                }
            });
        });


        $('#mytreadmill').startTreadmill({
            runAfterPageLoad: true,
            direction: "up",
            speed: "slow",
            viewable: 10,
            pause: false,
            click: true
        });

        $(document).on('click', '.treadmill-unit', function () {
            $(this).addClass('activeBold');
            $(this).siblings().removeClass('activeBold');
            $(".register-form").show();
            $('#price_comment').text($(this).data('price_comment'));
            $('#price').text($(this).data('price'));
            $('#sub-service-field').val($(this).data('service'));
        });


        $('.show-dropdown').on('click',function () {
            $(".dropdown-content").show();
        });

        $('.mahir-service').on('click',function () {
            var service = $(this).data('service');
            var url = "{{ url('order-now?service=')}}"+$(this).data('title')+"&sub_service=";
            $('.sub-service-box').empty();
            $.ajax({
                type: 'GET',
                url: "{{ url('getSubServices') }}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        var html = '';
                        html += '<div class="col-md-3 col-6"><a href="'+url+row.title+'" class="mahir-sub-service"><div class="sub-service-sect">';
                        html += '<span class="title">'+row.title+'</span></div></a></div>';
                        $('.sub-service-box').append(html);
                    });
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

        $('.services-link').first().click();
        $('.service-list > div > div').first().find('a').click();

        $('.otp-resend-btn').on('click',function () {
            $this = $(this);
            var mobile = $('#otp-modal .modal-mobile').val();
            $.ajax({
                type: 'GET',
                url: "{{ url('resend-otp') }}",
                data: { mobile: mobile},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if(result.status){
                        disabledButton($this);
                        showTimer();
                        toastr.success(result.message);
                    }else{
                        toastr.error(result.message);
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

        $('.forgot-password').on('click',function (e) {
            e.preventDefault();
            $this = $(this);
            var mobile = $('#check-password-modal .modal-mobile').val();
            $.ajax({
                type: 'GET',
                url: "{{ route('forgotPassword') }}",
                data: { mobile: mobile},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if(result.status){
                        disabledButton($this);
                        showTimer();
                        toastr.success(result.message);
                    }else{
                        toastr.error(result.message);
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });
        $('.service-list a[href*="#"]').click(function(e) {
            e.preventDefault();
            console.log('here');
            $('html, body').animate(
                {
                    scrollTop: $($(this).attr('href')).offset().top - 70,
                },
                500,
                'linear'
            )
        });

        $(document).on('click','.treadmill-unit',function () {
            $('html, body').animate({
                scrollTop: $('.register-form').offset().top
            });
        });

        $('#instanceOrder').submit(function (e) {
            e.preventDefault();
            var $this = $(this);
            var $form = $this.serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('instanceOrder') }}",
                data: $form,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function () {
                    $('.btn-Book-now').html('Wait <i class="fas fa-circle-notch fa-spin"></i>').attr('disabled','disabled');
                },
                success: function (response) {
                    if(response.status){
                        Swal.fire({
                            title: 'Order Placed',
                            text: response.message,
                            type: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (response.redirect) {
                            location.href = "{{ url('order') }}";
                        }else{
                            $('#instanceOrder')[0].reset();
                            $('select[name="subService"], select[name="area"]').val('').trigger('chosen:updated');
                        }
                    });
                    }else{
                        toastr.error(response.message);
                    }


                    $('.btn-Book-now').html('Book Now').removeAttr('disabled');
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                    $('.btn-Book-now').html('Book Now').removeAttr('disabled');
                }
            });
        });
        $('#formsubmitbutton').submit(function (e) {
            e.preventDefault();
            var $this = $(this);
            var $form = $this.serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('quote.store') }}",
// data: $form,
                data: $(this).serializeArray(),
// contentType: "application/json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    if(response.status){
                        Swal.fire({
                            title: 'Request Call',
                            text: response.message,
                            type: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (response.redirect) {
                            location.href = "{{ url('/') }}";
                        }else{
                            $('#formsubmitbutton')[0].reset();
// $('select[name="subService"], select[name="area"]').val('').trigger('chosen:updated');
                        }
                    });
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function (key, value) {
                        toastr.error(value);
                    });
                }
//
            });
        });

        $('#check-otp-valid').submit(function (e) {
            e.preventDefault();
            var $form = $(this).serialize();
            $.ajax({
                type: 'GET',
                url: "{{ route('checkOtpValid') }}",
                data: $form,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    toastr.info(result.message);
                    if(result.status){
                        location.href = "{{ url('order') }}";
                    }else{
                        $('#otp-modal').modal('show');
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });


        $('#setUserPassword').submit(function (e) {
            e.preventDefault();
            var $form = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('setUserPassword') }}",
                data: $form,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if(result.status){
                        toastr.success(result.message);
                        $('#setUserPassword-modal').modal('hide');
                        location.reload();
                    }else{
                        toastr.error(result.message);
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

        $('#checkUserPasswordValid').submit(function (e) {
            e.preventDefault();
            var $form = $(this).serialize();
            $.ajax({
                type: 'GET',
                url: "{{ route('checkUserPasswordValid') }}",
                data: $form,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if(result.status){
                        toastr.success(result.message);
                        location.href = "{{ url('order') }}";
                    }else{
                        toastr.error(result.message);
                    }

                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });
    });

    function disabledButton($this) {
        $this.attr("disabled", true);
        setTimeout(function() {
            $this.removeAttr("disabled");
        }, 1*60*1000);
    }
    function showTimer(){
        var counter = 60;
        var interval = setInterval(function() {
            counter--;
// Display 'counter' wherever you want to display it.
            if (counter <= 0) {
                clearInterval(interval);
                $('#time').html("00:00");
                return false;
            }else{
                if (counter < 10) {
                    $('#time').html('00:0' + counter);
                }else{
                    $('#time').html('00:' + counter);
                }
            }
        }, 1000);
    }
    $.fn.oldChosen = $.fn.chosen
    $.fn.chosen = function(options) {
        var select = $(this)
            , is_creating_chosen = !!options

        if (is_creating_chosen && select.css('position') === 'absolute') {
            select.removeAttr('style')
        }

        var ret = select.oldChosen(options)

// only act if the select has display: none, otherwise chosen is unsupported (iPhone, etc)
        if (is_creating_chosen && select.css('display') === 'none') {
            select.attr('style','display:visible; position:absolute; clip:rect(0,0,0,0)');
            select.attr('tabindex', -1);
        }
        return ret
    }

    $('select').chosen({allow_single_deselect: true});


    // rotateIn
    /*animation of text on profile page*/
    $('.media-body').addClass ('animated zoomIn');
    $('.media-body').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething);



</script>
