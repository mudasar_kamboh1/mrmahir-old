<!-- header section -->
<section class="header-section">
    <div class="container">
        <nav class="navbar custom-navbar navbar-expand-md navbar-light">
            <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('assets/images/logo.png') }}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto cstm-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('services') }}">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('rate-list') }}">Rate List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('about-us') }}">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('portfolio') }}">Portfolio</a>
                    </li>
                    <li class="nav-item">

                        @guest
                            <a class="nav-link sign-up" href="{{ url('login') }}">SignUp/SignIn</a>
                        @endguest
                    </li>

                    <li class="nav-item">
                        <a class="nav-link order-now" href="{{ url('order-now') }}">Book Now</a>
                    </li>
                    @auth
                    <li class="nav-item">
                        <div class="dropdown">
                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <img src="{{ asset('assets/images/avatar.png') }}" style="width:25px;height:25px;">
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </div>
                    </li>
                    @endauth
                </ul>

            </div>
        </nav>
    </div>
</section>
<!--  Whatsapp live chat   -->
<a class="whatsapp-link" href="https://api.whatsapp.com/send?phone=+923096661919&text=Hi Mr.Mahir!" target="_blank" style="position: fixed;right: 27px;bottom: 43px;z-index: 999;"><img src="{{ asset('assets/images/whatsapp-512.png') }}" class="img-circle" width="60px"></a>
<div id="fixed-social">
    <div>
        <a href="https://www.facebook.com/teammahir/" class="fixed-facebook" target="_blank"><i class="fab fa-facebook-f"></i> <span>Facebook</span></a>
    </div>
    <div>
        <a href="https://twitter.com/MahirTeam" class="fixed-twitter" target="_blank"><i class="fab fa-twitter"></i> <span>Twitter</span></a>
    </div>
    <div>
        <a href="https://www.instagram.com/teammahir/" class="fixed-instagram" target="_blank"><i class="fab fa-instagram"></i> <span>Instagram</span></a>
    </div>
    <div>
        <a href="https://www.youtube.com/channel/UCICIsVRC6pNmqLRYilG5bNQ" class="fixed-youtube" target="_blank"><i class="fab fa-youtube"></i> <span>Youtube</span></a>
    </div>
</div>
