@extends('web.layouts.banner-layout')

@section('content')

    <section class="terms-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="mahir-pehra">
                        <strong style="font-weight: bold;font-size: 20px;">Mr. Mahir-Terms and Conditions</strong>
                        <ul style="padding: 0;">

                            <p>The use of services at Mr. Mahir is subject to following terms and conditions. It is mandatory for all users to read and accept these Terms of Use at Mr. Mahir. </p>

                            <p>Note: Please note that Mr. Mahir may change these Terms and Conditions with or without notifying the users. The users are therefore advised to keep themselves updated with the changes on their own. </p>

                            <div style="padding: 0 38px;" class="">
                                <li>Standard service charges of minimum Rs. 500 will be applied each time you call a service provider through Mr. Mahir. </li>

                                <li>These standard charges will be applied if the total charges of your service are less than Rs. 500, however, if your total charges for the service are above Rs. 500 then these service charges will be exempted. </li>

                                <li>In case of any parts required for the repair or fixing, customers can provide those parts. However, Mr. Mahir can also provide these parts but the charges of it will have to be paid by the customer, and these charges will be other than the service charges applied.</li>

                                <li>In case of any pricing conflict, customers can reach out to the Customer Support department for any queries or concerns.</li>

                                <li>In case of poor weather conditions or traffic disruptions, or any other such unfavorable conditions, the technician might get late in reaching your location. </li>

                                <li style="font-weight: bold;padding-top: 30px;">I hereby affirm that I have read the Terms and Conditions of Mr. Mahir and accept them.</li>
                            </div>

                        </ul>
                    </div>
                </div>


  @include('web.partials.quote')

            </div>
        </div>
    </section>
    <!-- Expand  Business section -->
    <section class="expand-business-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Expand your service business with Mr.Mahir</h3>
            </div>
            <div class="row">
                <div id="business-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Electricion</h4>
                            <p>Saleem is an experienced and knowledgeable electrician with more than 6 years of experience in the field.
                                He has been a strong asset of Mr. Mahir for 4 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Carpenters</h4>
                            <p>Meet Raheem who has an exceptional and broad experience of 3 years in carpentry and woodworking.
                                He is a strong resource for Mr. Mahir for more than a year.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Plumbers</h4>
                            <p>Jamil is a highly skilled plumber with 8 years’ experience in both industrial and commercial plumbing.
                                He has been serving our customers with quality work for 5 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Handymen</h4>
                            <p>Our handyman Ahmed is a true professional with 2 years of experience in the field.
                                He has proven to be the best resource of Mr. Mahir with high customer rating.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>

                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Painters</h4>
                            <p>Aleem offers years of experience on a wide variety of painting projects.
                                Our customers can’t help but to give him 5 star rating for his quality work.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>A/C Technicians</h4>
                            <p>Faisal is an accomplished air conditioning technician with more than 8 years of experience working on commercial and residential AC units.
                                Known to paying close attention to our customers’ needs.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="cta-section">
                        <h2>Are you a professional looking to grow your service business?</h2>
                        <a href="" target="_blank" class="btn">Join Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- counter section -->

    <!-- Clients section -->
    <section class="clients-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Customer Reviews</h3>
            </div>
            <div class="row">
                <div id="clients-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="clients-testimonial card">
                            <div class="client-info">
                                <div class="media">
                                    <img class="mr-3" src="{{ asset('assets/images/testimonial.png') }}" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Reema Shah</h5>
                                    </div>
                                </div>
                                <p>Mr. Mahir is very easy to contact. The carpenter was prompt and efficient with the job.
                                    Great work! </p>
                                <div class="row">
                                    <div class="reviews-fb">
                                        <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="stars-img">
                                        <img src="{{ asset('assets/images/rating.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="clients-testimonial card">
                            <div class="client-info">
                                <div class="media">
                                    <img class="mr-3" src="{{ asset('assets/images/testimonial.png') }}" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Furqan Ahmed</h5>
                                    </div>
                                </div>
                                <p>Very helpful and quick response by Mr. Mahir. The workers always leave everything clean and tidy after the job.
                                    It feels that help is a just a call away with your services. </p>
                                <div class="row">
                                    <div class="reviews-fb">
                                        <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="stars-img">
                                        <img src="{{ asset('assets/images/rating.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="clients-testimonial card">
                            <div class="client-info">
                                <div class="media">
                                    <img class="mr-3" src="{{ asset('assets/images/testimonial.png') }}" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Usman Nadeem</h5>
                                    </div>

                                </div>
                                <p>I got a quick response after connecting with Mr. Mahir and the worker arrived on time.
                                    The job was done perfectly, would be happy to recommend. </p>
                                <div class="row">

                                    <div class="reviews-fb">
                                        <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="stars-img">
                                        <img src="{{ asset('assets/images/rating.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="clients-testimonial card">
                            <div class="client-info">
                                <div class="media">
                                    <img class="mr-3" src="{{ asset('assets/images/testimonial.png') }}" alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0">Maria Khan</h5>
                                    </div>
                                </div>
                                <p>The electrician who came to fix wiring problem at my house was very informative and efficient.
                                    An excellent experience using your services.
                                </p>
                                <div class="row">
                                    <div class="reviews-fb">
                                        <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="stars-img">
                                        <img src="{{ asset('assets/images/rating.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection