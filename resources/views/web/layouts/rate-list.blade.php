@extends('web.layouts.banner-layout')
@section('meta')
    <title>Home Maintenance Services Rate List | Mr Mahir</title>
    <meta  name="description" content="What can our home improvement professionals do for you? Get the best competitive rates with guaranteed results and excellent customer services.">
@endsection
@section('content')
    {{--    @include('web.partials.header-with-banner')--}}
    <section class="top-header-ratelist-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-left header-btn">
                        @include('web.partials.drop-down')

                        <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav cstm-nav">
                                    @auth
                                    <li class="nav-item">
                                        <div class="dropdown show">
                                            <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <img src="{{ asset('assets/images/avatar.png') }}" alt="" style="width:50px;height:50px;">
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                                <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                                <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                            </div>
                                        </div>
                                    </li>
                                    @endauth
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="text-center mahir-icon">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="" width="100px">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-right header-btn">
                        <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ratelist-section">
        <div class="container">
            <div class="main-heading">
                <h2>How Can Mr. Mahir <span class="redd">Help You?</span></h2>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="about-description">
                        <ul class="nav nav-tabs nav-justified md-tabs price-tabs" id="myTabJust" role="tablist">
                            @foreach($services as $k => $service)
                                <li class="nav-item">
                                    <a class="nav-link {{ $k == 0 ? 'active': '' }}" data-toggle="tab" href="#home-just{{$k}}" role="tab" aria-controls="home-just{{$k}}"
                                       aria-selected="true">{{$service->title}}</a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tab-content pt-5" id="myTabContentJust" style="padding-top: 0px !important;">
                            @foreach($services as $k => $service)
                                <div class="tab-pane fade show {{ $k == 0 ? 'active': '' }}" id="home-just{{$k}}" role="tabpanel" aria-labelledby="home-tab-just">
                                    <table class="table table-hover table-striped table-bordered table-sm dtBasicExample">
                                        <thead>
                                        <tr>
                                            <th class="img-hide-on-mobile" scope="col"></th>
                                            <th scope="col">Sub Service</th>
                                            <th scope="col">Rates in PKR</th>
                                            <th scope="col">Book Now</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($service->subServices))
                                            @foreach($service->subServices as    $k=> $subService)
                                                @if ($k == 8)@break @endif
                                                <tr>
                                                    <td class="img-hide-on-mobile">
                                                        @if($subService->image)
                                                        <img  src="{{ asset('thumbnailImg/'.$subService->image) }}" alt="{{$subService->alt}}" width="60px" style="height: 40px;margin-top: 7px; border-radius: 10%;">
                                                    @else
                                                        @endif
                                                    </td>
                                                    <td class="text-table">
                                                        {{--{{$subService->image}}--}}
                                                        <strong class="text-capitalize" style="font-weight: 600;">{{$subService->title}}</strong>
                                                    </td>
                                                    <td><strong style="font-weight: 600;">{{$subService->price_comment}}</strong> - {{$subService->price}}</td>

                                                    <td>
                                                        @php
                                                            $serviceSelected = !is_null($subService->service) ? $subService->service->slug : '-';
                                                        @endphp
                                                        <a href="{{ url('order-now?service='.$serviceSelected.'&sub_service='.$subService->title) }}">
                                                        <button class="btn order-now">Book Now</button></a></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                </div>
                            @endforeach
                        </div>

                        <div class="terms" id="conditions">
                            <strong>Mr. Mahir-Terms and Conditions</strong>
                            <ul style="padding: 0;font-size: 20px;">

                                <p>The use of services at Mr. Mahir is subject to following terms and conditions. It is mandatory for all users to read and accept these Terms of Use at Mr. Mahir. </p>

                                <p>Note: Please note that Mr. Mahir may change these Terms and Conditions with or without notifying the users. The users are therefore advised to keep themselves updated with the changes on their own. </p>

                                <div style="padding: 0 38px;" class="">
                                    <li>Standard service charges of minimum Rs. 500 will be applied each time you call a service provider through Mr. Mahir. </li>

                                    <li>These standard charges will be applied if the total charges of your service are less than Rs. 500, however, if your total charges for the service are above Rs. 500 then these service charges will be exempted. </li>

                                    <li>In case of any parts required for the repair or fixing, customers can provide those parts. However, Mr. Mahir can also provide these parts but the charges of it will have to be paid by the customer, and these charges will be other than the service charges applied.</li>

                                    <li>In case of any pricing conflict, customers can reach out to the Customer Support department for any queries or concerns.</li>

                                    <li>In case of poor weather conditions or traffic disruptions, or any other such unfavorable conditions, the technician might get late in reaching your location. </li>

                                    <li style="font-weight: bold;padding-top: 30px;">I hereby affirm that I have read the Terms and Conditions of Mr. Mahir and accept them.</li>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>

                @include('web.partials.quote')

            </div>
        </div>
    </section>

    <!-- Expand  Business section -->
    <section class="expand-business-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Expand your service business with Mr. Mahir</h3>
            </div>
            <div class="row">
                <div id="business-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Electricion</h4>
                            <p>Saleem is an experienced and knowledgeable electrician with more than 6 years of experience in the field.
                                He has been a strong asset of Mr. Mahir for 4 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Carpenters</h4>
                            <p>Meet Raheem who has an exceptional and broad experience of 3 years in carpentry and woodworking.
                                He is a strong resource for Mr. Mahir for more than a year.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Plumbers</h4>
                            <p>Jamil is a highly skilled plumber with 8 years’ experience in both industrial and commercial plumbing.
                                He has been serving our customers with quality work for 5 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Handymen</h4>
                            <p>Our handyman Ahmed is a true professional with 2 years of experience in the field.
                                He has proven to be the best resource of Mr. Mahir with high customer rating.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Painters</h4>
                            <p>Aleem offers years of experience on a wide variety of painting projects.
                                Our customers can’t help but to give him 5 star rating for his quality work.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>A/C Technicians</h4>
                            <p>Faisal is an accomplished air conditioning technician with more than 8 years of experience working on commercial and residential AC units.
                                Known to paying close attention to our customers’ needs.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Clients section -->
    <section class="clients-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Customer Reviews</h3>
            </div>
            <div class="row">
                <div id="clients-slider" class="owl-carousel owl-theme">
                    @foreach($Facebook_review as $facebook)

                        <div class="item">
                            <div class="clients-testimonial card">
                                <div class="client-info">
                                    <div class="media">
                                        <img class="mr-3" src="{{ asset('fbImages/'.$facebook->image) }}" alt="{{$facebook->alt}}">
                                        <div class="media-body">
                                            <h5 class="mt-0">{{$facebook->name}}</h5>
                                        </div>
                                    </div>
                                    <div class="pehra-of-rate">
                                        <p>{{$facebook->description}} </p>
                                        <div class="row">
                                            <div class="reviews-fb">
                                                <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="stars-img">
                                            <div class="stars-img">
                                                @php $rating = $facebook->rating; @endphp
                                                <div class="placeholder" style="color: lightgray;">

                                                    {{--<span class="small">({{ $rating }})</span>--}}
                                                </div>
                                                <div class="overlay" style="position: relative;top: -22px;">
                                                    @foreach(range(1,5) as $i)
                                                        <span class="fa-stack" style="width:1em">
                                                        <i class="far fa-star fa-stack-1x"></i>
                                                            @if($rating >0)
                                                                @if($rating >0.5)
                                                                    <i class="fas fa-star fa-stack-1x checked"></i>
                                                                @else
                                                                    <i class="fas fa-star-half fa-stack-1x checked"></i>
                                                                @endif
                                                            @endif
                                                            @php $rating--; @endphp</span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('web.partials.footer')
@endsection
