@extends('web.layouts.banner-layout')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}">
@endsection

@section('content')

{{--@include('web.partials.header-with-banner')--}}
<style>
    .message{
        display: block !important;
        width: 100% !important;
        outline: none !important;
        resize: none !important;
        border: none !important;
        overflow: hidden !important;
        min-height: 30px;
    }
    .form-group .active{
        margin-bottom: -24px;
    }
    .mobile-hide table tr th{
        color: white;
    }
    .order-track-section h1 {
        background: #0f3e7c;
        font-size: 28px;
        text-align: center;
        padding: 13px;
        font-weight: 600;
        color: white;
        margin-bottom: 0;
        border-bottom: 2px solid #0b2240;
    }

</style>
<section class="top-header-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="text-left header-btn">
{{--                   @include('web.partials.drop-down')--}}
                    @guest
                    <button class="signup-signin">
                        <a style="color: #fff;font-weight: 300;" href="{{ url('register') }}">SignUp/ </a>
                        <a style="color: #fff;font-weight: 300;" href="{{url('login')}}"> SignIn</a>
                    </button>
                    @endguest
                    @auth
                    <div class="dropdown" id="drop" style="display: none;">
                        <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                        <div class="dropdown-content">
                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                            @if($user->hasRole('Staff'))
                                <a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>
                            @endif
                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </div>
                    @endauth

                    <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav cstm-nav">
                                @auth
                                <li class="nav-item">
                                    <div class="dropdown show">
                                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                        </div>
                                    </div>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-center mahir-icon">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="text-right header-btn">
                    <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                </div>
            </div>
        </div>
    </div>
</section>

    <section class="order-detail-section">
        <div class="container">
            <div class="order-track-info">
                <div class="order-track-detail">
                    <div class="col-lg-12 col-sm-12">
                        <div class="table-responsive mobile-hide">
                            <table class="table table-bordered">
                                <tbody>
                                <tr style="background: #0f3e7c;">
                                    <th scope="col">Order Token</th>
                                    <th scope="col">Sub Service | Service</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Problem Message</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                <tr>
                                    <td>{{$order->token}}</td>
                                    <td>
                                        {{!is_null($order->subService) ?$order->subService->title:'N/A'}}
                                        | {{ !is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}
                                    </td>
                                    <td>{{ !is_null($order->user) ? $order->user->mobile : '-- -- --'}}</td>
                                    <td>{{ $order->problem_message ? $order->problem_message : '-- -- --' }}</td>
                                    <td>{{ !is_null($order->status) ? $order->status->title:'-- -- --'}}</td>
                                    <td width="200" align="center">
                                        @if(!is_null($order->status) && $order->status->title == "Pending")

                                        <a class="btn track-order" href="#" data-toggle="modal" data-target="#add-mobile">Add Additional Mobile Number</a>
                                        <a class="btn track-order" href="#" data-toggle="modal" data-target="#notes">Add Problem Message</a>
                                        <a class="btn track-order" href="#" data-toggle="modal" data-target="#address"> Add Additional Address</a>
                                        <a class="btn track-order" href="#" data-toggle="modal" data-target="#addTimeSlot"> Set Time Schedule</a>
                                       @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div id="show" class="text-center">
                            <a href="#order-detail" style="display: none" class="btn view-order-detail">View More Detail</a>
                        </div>
                        <div id="order-detail" class="submit-order-detail" >
                            <div class="order-detail-section">
                                <h2>Order Information:</h2>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"><b style="font-weight:bold"> Name</b> : {{! is_null($order->user)?$order->user->name:'N/A'}}</label>&nbsp;<span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"><b style="font-weight:bold">Service  </b>  : {{!is_null($order->subService) ?$order->subService->title:'N/A'}}
                                                | {{! is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}
                                                </label>&nbsp;<span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"> <b style="font-weight:bold">Additional Mobile </b>:  {{$order->mobile}}</label>&nbsp;<span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"><b style="font-weight:bold">Schedule TimeSlot </b>: {{ $order->schedule_datetime ? Carbon\Carbon::parse($order->schedule_datetime)->format('h:s A Y-m-d') : '-- -- --'}}</label>&nbsp;<span></span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"> <b style="font-weight:bold">Order  Area</b> : {{!is_null($order->area) ? $order->area->name:'N/A'}}</label>&nbsp;<span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"> <b style="font-weight:bold">Order Address</b> :{{ $order->address ? $order->address: $order->user->address}}</label>&nbsp;<span></span>
                                            {{--<label for="contact"> <b style="font-weight:bold">Order Address</b> : </label>--}}
                                            {{--<textarea readonly class="message"> {{ $order->address ? $order->address: $order->user->address}}</textarea>--}}
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact"> <b style="font-weight:bold">Order Price</b> :
                                                @if ($order->order_price === null)
                                                    {{!is_null($order->subService) ? $order->subService->price:'N/A'}}/
                                                    {{--<input class="form-control" type="number" name="order_price" value="{{ !is_null($order->subService) ? $order->subService->price:'---'}}" >--}}
                                                @else
                                                    {{ !is_null($order) ? $order->order_price:'---'}}
                                                    {{--<input class="form-control" type="number" name="order_price" value="{{ !is_null($order) ? $order->order_price:'---'}}" >--}}
                                                @endif
                                                -</label>&nbsp;<span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="modal fade" id="add-mobile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content cstm-modal-content">
                                <div class="modal-body">
                                    <div class="order-track-section">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h1>Add Additional Number</h1>
                                        <form action="{{route('addMobile',$order->id)}}" method="post" class="order-form" id="mobile-form" novalidate="novalidate">
                                            @csrf
                                            {{--@method('GET')--}}

                                            <div class="form-group">
                                                <label for="name" class="form-label">Add Your Additional Number:<strong style="color: red;">*</strong></label>
                                                <input type="text" id="textbox" class="form-control cstm-control num" placeholder="Mobile" name="mobile" value="{{$order->mobile}}" minlength="0" min="0" maxlength="11">
                                            </div>
                                            <br>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-default order-submit">Save</button>
                                                <button type="button" class="btn btn-default btn-cancel order-submit" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="notes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content cstm-modal-content">
                                <div class="modal-body">
                                    <div class="order-track-section">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h1>Add Additional Problem Message</h1>
                                        <form action="{{route('addNote',$order->id)}}" method="post" class="order-form" id="mobile-form" novalidate="novalidate">
                                            @csrf
                                            <div class="form-group">
                                                <label for="name" class="form-label">Add Your problem  Message:<strong style="color: red;">*</strong></label>
                                                <textarea placeholder="Your Note" name="problem_message" maxlength="1500" class="form-control cstm-textarea" required="required">{{$order->problem_message}}</textarea>
                                            </div>
                                            <br>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-default order-submit">Save</button>
                                                <button type="button" class="btn btn-default btn-cancel order-submit" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal fade" id="address" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content cstm-modal-content">
                            <div class="modal-body">
                                <div class="order-track-section">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h1>Add your Address</h1>
                                    <form action="{{route('addAddress',$order->id)}}" method="post" class="order-form" id="mobile-form" novalidate="novalidate">
                                        @csrf
                                        <div class="form-group">
                                            <label for="name" class="form-label"> Set your Address:<strong style="color: red;">*</strong></label>
                                            <div class="form-group">
                                                <input size="12" type="email" class="form-control cstm-control" name="address" value="" placeholder="address">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-default order-submit">Save</button>
                                            <button type="button" class="btn btn-default btn-cancel order-submit" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="modal fade" id="addTimeSlot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content cstm-modal-content">
                                <div class="modal-body">
                                    <div class="order-track-section">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h1>Time and Schedule</h1>
                                        <form action="{{route('addTimeSlot',$order->id)}}" method="post" class="order-form" id="mobile-form" novalidate="novalidate">
                                            @csrf
                                            <div class="form-group">
                                                <label for="name" class="form-label"> Set Time Schedule:<strong style="color: red;">*</strong></label>
                                                <div class="form-group">
                                                    <div class="controls input-append date  form_datetime " data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                                        <input size="16" class="form-control cstm-input"  name="schedule_datetime" type="text" value="{{ $order->schedule_datetime ? Carbon\Carbon::parse($order->schedule_datetime)->format('d-m-Y h:s A') : '-- -- --'}}" placeholder="Select Time Slot" autocomplete="off">
                                                        <span class="add-on"><i class="icon-remove"></i></span>
                                                        <span class="add-on"><i class="icon-th"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-default order-submit">Save</button>
                                                <button type="button" class="btn btn-default btn-cancel order-submit" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

@include('web.partials.footer')

{{--@include('web.partials.footer')--}}
@endsection

@section('script')
    <script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                startDate: '-0d',
                todayBtn:  1,
                format: 'dd-mm-yyyy HH:iiP',
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            });
        });

        $(document).ready(function() {
            $("#show").click(function() {
                $("#order-detail").slideToggle("slow");
                if ($('.view-order-detail').text() == 'Hide Detail') {
                    $('.view-order-detail').text('View Detail');
                } else {
                    $('.view-order-detail').text('Hide Detail');
                }
            });
        });
    </script>

@endsection
