<!DOCTYPE html>
<html {{--lang="{{ str_replace('_', '-', app()->getLocale()) }}"--}}>
<head>
    @yield('meta')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/mahir-logo-fav.png') }}">


    <title>{{ config('app.name', 'M.Mahir') }}</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="{{ asset('assets/css/mdb.min.css') }}" rel="stylesheet">
    <!-- style css -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- owl carousel css -->

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/prism.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('assets/css/jquery.scrolling-tabs.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('assets/css/st-demo.css') }}">--}}
</head>
<body>
<div id="app">

    @yield('content')

    @include('web.partials.otpModal')

    @include('web.partials.setPasswordModal')

    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/chosen.jquery.js') }}"></script>
    <script>
        $.fn.oldChosen = $.fn.chosen
        $.fn.chosen = function(options) {
            var select = $(this)
                , is_creating_chosen = !!options

            if (is_creating_chosen && select.css('position') === 'absolute') {
                select.removeAttr('style')
            }

            var ret = select.oldChosen(options)

            // only act if the select has display: none, otherwise chosen is unsupported (iPhone, etc)
            if (is_creating_chosen && select.css('display') === 'none') {
                select.attr('style','display:visible; position:absolute; clip:rect(0,0,0,0)');
                select.attr('tabindex', -1);
            }
            return ret
        }

        $('select').chosen({allow_single_deselect: true});
    </script>
    @toastr_js
    @toastr_css
    @yield('scripts')
</div>
</body>
</html>
