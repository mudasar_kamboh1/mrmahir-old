@extends('web.layouts.banner-layout')
@section('content')
@include('web.partials.header-with-banner')
    <section class="ratelist-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="about-description">
                        <!-- inner serveices section -->
                            <section class="inner-service-section">
                                <div class="container">
                                    <div class="main-heading text-center">
                                        <h2>{{ $service->title }}</h2>
                                    </div>
                                </div>
                            </section>

                            <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0">
                                <thead>
                                <tr>
                                    <th scope="col">Sub Service</th>
                                    <th scope="col">Rates in PKR</th>
                                    <th scope="col">Book Now</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($service->subServices))
                                    @foreach($service->subServices as    $k=> $subService)
                                <tr>
                                    <td>
                                        {{$subService->title}}
                                    </td>
                                    <td>
                                        {{$subService->price}}
                                    </td>
                                    <td>
                                        <a href="{{ url('order-now?service='.$service->title.'&sub_service='.$subService->title) }}">
                                            <button class="btn order-now">Book Now</button></a></td>
                                </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                    </div>
                </div>
                @include('web.partials.quote')

            </div>
        </div>
    </section>

    <!-- Expand  Business section -->
    <section class="expand-business-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Expand your service business with Mr.Mahir</h3>
            </div>
            <div class="row">
                <div id="business-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="" >
                            <h4>Electricion</h4>
                            <p>Saleem is an experienced and knowledgeable electrician with more than 6 years of experience in the field.
                                He has been a strong asset of Mr. Mahir for 4 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Carpenters</h4>
                            <p>Meet Raheem who has an exceptional and broad experience of 3 years in carpentry and woodworking.
                                He is a strong resource for Mr. Mahir for more than a year.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Plumbers</h4>
                            <p>Jamil is a highly skilled plumber with 8 years’ experience in both industrial and commercial plumbing.
                                He has been serving our customers with quality work for 5 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Handymen</h4>
                            <p>Our handyman Ahmed is a true professional with 2 years of experience in the field.
                                He has proven to be the best resource of Mr. Mahir with high customer rating.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Painters</h4>
                            <p>Aleem offers years of experience on a wide variety of painting projects.
                                Our customers can’t help but to give him 5 star rating for his quality work.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>A/C Technicians</h4>
                            <p>Faisal is an accomplished air conditioning technician with more than 8 years of experience working on commercial and residential AC units.
                                Known to paying close attention to our customers’ needs.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="cta-section">
                        <h2>Are you a professional looking to grow your service business?</h2>
                        <a href="{{ url('register') }}" target="_blank" class="btn">Join Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Clients section -->
    <section class="clients-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Customer Reviews</h3>
            </div>
            <div class="row">
                <div id="clients-slider" class="owl-carousel owl-theme">
                    @foreach($Facebook_review as $facebook)

                        <div class="item">
                            <div class="clients-testimonial card">
                                <div class="client-info">
                                    <div class="media">
                                        <img class="mr-3" src="{{ asset('fbImages/'.$facebook->image) }}" alt="">
                                        <div class="media-body">
                                            <h5 class="mt-0">{{$facebook->name}}</h5>
                                        </div>
                                    </div>
                                    <p>{{$facebook->description}} </p>
                                    <div class="row">
                                        <div class="reviews-fb">
                                            <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="stars-img">
                                            <div class="stars-img">
                                                @php $rating = $facebook->rating; @endphp
                                                <div class="placeholder" style="color: lightgray;">
                                                    <span class="small">({{ $rating }})</span>
                                                </div>

                                                <div class="overlay" style="position: relative;top: -22px;">

                                                    @foreach(range(1,5) as $i)
                                                        <span class="fa-stack" style="width:1em">
                                                   <i class="far fa-star fa-stack-1x"></i>

                                                            @if($rating >0)
                                                                @if($rating >0.5)
                                                                    <i class="fas fa-star fa-stack-1x checked"></i>
                                                                @else
                                                                    <i class="fas fa-star-half fa-stack-1x checked"></i>
                                                                @endif
                                                            @endif
                                                            @php $rating--; @endphp
                                                        </span>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- footer section -->
    @include('web.partials.footer')
@endsection
