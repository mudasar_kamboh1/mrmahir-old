<!DOCTYPE html>
<html {{--lang="{{ str_replace('_', '-', app()->getLocale()) }}"--}}>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/mahir-logo-fav.png') }}">


    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="{{ asset('assets/css/mdb.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('assets/css/bootstrap-material-design.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">--}}

    <!-- style css -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- owl carousel css -->

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/prism.css') }}">
</head>
<body>
<div id="app">
    @include('web.partials.header')

    @yield('content')

    @include('web.partials.footer')

    @include('web.partials.scripts')
</div>
</body>
</html>
