
@extends('web.layouts.banner-layout')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
    <style>
        .cstm-input{
            border-radius: 0 !important;
        }
        #services_chosen .chosen-single{
            height: 37px !important;
        }
        #services_chosen .chosen-single:hover{
             outline: none !important;
        }
        .chosen-single span{
            margin: -7px -7px !important;
        }
        .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
            height: 32px !important;
        }
        .chosen-container-multi .chosen-choices .search-choice{
            height: 29px !important;
            padding-top: 7px !important;
            font-size: 14px !important;
        }
        .chosen-container-multi .chosen-choices li.search-choice .search-choice-close{
            margin-top: 4px !important;
        }
    </style>
@endsection

@section('content')

{{--@include('web.partials.header-with-banner')--}}

<section class="top-header-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="text-left header-btn">
                    @include('web.partials.drop-down')

                    <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav cstm-nav">
                                @auth
                                <li class="nav-item">
                                    <div class="dropdown show">
                                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                            {{--@if($user->hasRole('Staff'))--}}
                                            {{--<a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>--}}
                                            {{--@endif--}}
                                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                        </div>
                                    </div>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-center mahir-icon">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="text-right header-btn">
                    <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                </div>
            </div>
        </div>
    </div>
</section>

    <section class="order-listing-section">
        <div class="container">
            <div class="order-track-info">
                <div class="row margin-auto">
                    <div class="col-12">
                        <div class="profile">
                            <div class="media">
{{--                                <div class="media-left">--}}
{{--                                    <img src="{{asset('img/user.jpg')}}" class="media-object" width="75%">--}}
{{--                                </div>--}}
                                <div class="media-body">
                                    <h4 class="media-heading">{{auth()->user()->name!=null ? auth()->user()->name : "Administrator"}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contact-form-section">
                    <form action="{{route('updateProfile')}}" method="post" class="order-form" id="profile-form" enctype="multipart/form-data">
                        @csrf
                        {{--@method('PUT')--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label"> Your Name:</label>
                                    <input type="text" class="form-control cstm-input" name="name" maxlength="50" value="{{$user->name}}" placeholder="YOUR NAME"  required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="name" class="form-label">Your Phone:</label>
                                    <input type="text" id="phone"  class="form-control cstm-input" value="{{$user->mobile}}" placeholder="YOUR PHONE NUMBER"   minlength="11" maxlength="11" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label"> Your Email:</label>
                                    <input class="form-control cstm-input" name="email" value="{{$user->email}}" placeholder="YOUR EMAIL" maxlength="50" pattern="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label">Address:</label>
                                    <input type="text" class="form-control cstm-input" name="address"  placeholder="New address" value="{{$user->address}}">
                                </div>
                            </div>
                            @if($user->hasRole('Staff'))
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label">CNIC Number:</label>
                                    <input type="text" class="form-control cstm-input"  id="cnic" maxlength="15" name="card_id"  placeholder="CNIC number" value="{{!is_null($user->staff)?$user->staff->card_id:'---'}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="name" style="font-weight: 600;">CNIC Expire Date:</label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" data-date-format="dd-mm-yyyy" name="expire_date"  value="{{!is_null($user->staff) && $user->staff->expire_date ? Carbon\Carbon::parse($user->staff->expire_date)->format('m/d/Y'):''}}"  placeholder="MM/DD/YY" autocomplete="off" required>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label">New password:</label>
                                    <input type="password" class="form-control cstm-input" name="password"  placeholder="New password" maxlength="250">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label">Confirm password:</label>
                                    <input type="password" class="form-control cstm-input" name="password_confirmation"  placeholder="Confirm password" maxlength="250">
                                </div>
                            </div>
                            @if($user->hasRole('Staff'))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="form-label">Services:</label>
                                        <select name="services[]" data-placeholder="Service List" class="chosen-select form-control cstm-control" tabindex="2" id="services" style="width: 100%;" >
                                            <option value="" selected disabled="disabled"></option>
                                            @foreach($services as $service)
                                                <option class="form-control cstm-input" style="width: 200px" value="{{$service->id}}">{{$service->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group left-inner-addon">
                                        <label for="name" class="form-label">Sub Services:</label>
                                        <select data-placeholder="Select Your SubService"    name="sub_services[]" class="chosen-select sub-services form-control cstm-control" tabindex="2" id="sub_services" multiple required>
                                            <option value="">Select Sub Service</option>
                                            @php
                                                $staffService = !is_null($user->staff)&& !is_null($user->staff->subServices) ? $user->staff->subServices->pluck('id'):'---';
                                            @endphp
                                            @foreach($sub_services as $sub_service)
                                            <option value="{{$sub_service->id}}"{{$staffService->contains($sub_service->id)?'selected':'--'}}>{{$sub_service->title}}</option>
                                                <@endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                            @if($user->hasRole('Staff'))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="form-label">Your Area:</label>
                                        <select name="areas[]" multiple data-placeholder="Select Your Areas" class="chosen-select form-control cstm-control" style="width: 100%;">
                                            @php
                                                $staffAreas = !is_null($user->staff)&& !is_null($user->staff->areas) ? $user->staff->areas->pluck('id'):'----';
                                            @endphp
                                            @foreach($areas as $area)
                                                <option class="form-control cstm-input" style="width: 200px" value="{{$area->id}}" {{ $staffAreas->contains($area->id) ?'selected':'---'}}>{{$area->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="text" style="margin-left: 17px;padding-top: 4px;">
                                    @php
                                        $staff->images = explode(',',$staff->images);
                                    @endphp
                                    <label style="font-weight: 600;" for="name" class="form-label">CNIC Images:</label>

                                    @foreach($staff->images as $image)
                                        <div id="{{$image}}">
                                            @if($image)
                                                <img src="{{asset('assets/staff')}}{{ '/'.$image }}" class="images" data-src="{{$image}}" width="50px" height="50px">
                                                <span style="color: red" onclick="removeImage($(this))" class="glyphicon glyphicon-remove"></span>
                                        @else
                                            @endif
                                        </div>
                                    @endforeach
                                    <input type="file" id="images" name="images[]" placeholder="address" multiple="">
                                    <input type="hidden" id="new_image" name="edit_images" />
                                </div>
                            @endif
                        </div>
                        <br>
                        <div class="text-center">
                            <button style="margin-top: -18px;" type="submit" class="btn btn-default order-submit">Update</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>


@include('web.partials.footer')

@endsection

@section('script')
    {{--<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>--}}
    <script type="text/javascript">
  $('#services').on('change',function (e) {
      var service = $(this).val();
        $('#sub_services option').not('option:selected').remove();
            $.ajax({
                type: 'GET',
                url: "{{ url('getSubServices') }}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        $('#sub_services').append('<option value="'+row.id+'">'+row.title+'</option>');
                    });
                    $('#sub_services').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

  $('#cnic').keydown(function(){
      //allow  backspace, tab, ctrl+A, escape, carriage return
      if (event.keyCode == 8 || event.keyCode == 9
          || event.keyCode == 27 || event.keyCode == 13
          || (event.keyCode == 65 && event.ctrlKey === true) )
          return;
      if((event.keyCode < 48 || event.keyCode > 57))
          event.preventDefault();

      var length = $(this).val().length;

      if(length == 5 || length == 13)
          $(this).val($(this).val()+'-');

  });

    </script>
@endsection
