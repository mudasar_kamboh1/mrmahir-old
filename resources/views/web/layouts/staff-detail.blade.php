        <div class="modal-content cstm-modal-content">
                <div class="order-track-detail-section" style="background: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h1 style="font-weight: 600;font-size: 28px;">Order Detail</h1>
                    <form  method="post" class="order-form" id="mobile-form" style="padding: 20px 10px;" novalidate="novalidate">
                    <div class="container">
                        {{--@if(count($orders) > 0)--}}
                        {{--@foreach($orders as $order)--}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Name :</label><br>
                                        <input class="form-control" type="text" name="" value="{{!is_null($order)?$order->user->name:'-'}}" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Service :</label><br>
                                        <input class="form-control" type="text" name="price" value="{{ !is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Sub-Service:</label><br>
                                        <input class="form-control" type="text" name="price"  value="{{!is_null($order)?$order->Subservice->title:'-'}}" required readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Phone:</label><br>
                                        <input class="form-control" type="text" name="phone" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" value="{{!is_null($order)?$order->user->mobile:'-'}}" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Address:</label><br>
                                        <input class="form-control" type="text" name="price" value="{{!is_null($order)?$order->address:'-'}}" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Area:</label><br>
                                        <input class="form-control" type="text" name="price"  value="{{!is_null($order->area) ? $order->area->name:'N/A'}}" required readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Order Date:</label><br>
                                        <input class="form-control" type="text" name="price" value="{{ $order->schedule_datetime ? Carbon\Carbon::parse($order->schedule_datetime)->format('h:s A Y-m-d') : '-- -- --'}}" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Problem Message:</label><br>
                                        <input class="form-control" type="text" name="assign_date" value="{{!is_null($order)?$order->problem_message:''}}" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Create Date:</label><br>
                                        <input class="form-control" type="text" name="price" value="{{ $order->created_at ? Carbon\Carbon::parse($order->created_at)->format('h:s A Y-m-d') : '-- -- --'}}" required readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--@endforeach--}}
                        {{--@endif--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="modal-btn text-center">
                                    <button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
