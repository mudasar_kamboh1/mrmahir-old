@extends('web.layouts.banner-layout')

@section('content')
    <style>
        .table-hover tr th{
            color: white;
        }
        .control{
            padding: 5px; !important;
        }
        hr{
            margin: 5px 0 !important;
        }

    </style>
{{--    @include('web.partials.header-with-banner')--}}
<section class="top-header-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="text-left header-btn">
                    {{--@include('web.partials.drop-down')--}}
                    @guest
                    <button class="signup-signin">
                        <a style="color: #fff;font-weight: 300;" href="{{ url('register') }}">SignUp/ </a>
                        <a style="color: #fff;font-weight: 300;" href="{{url('login')}}"> SignIn</a>
                    </button>
                    @endguest
                    @auth
                    <div class="dropdown" id="drop" style="display: none;">
                        <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                        <div class="dropdown-content">
                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                            {{--@if($user->hasRole('Staff'))--}}
                                {{--<a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>--}}
                            {{--@endif--}}
                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </div>
                    @endauth
                    <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav cstm-nav">
                                @auth
                                <li class="nav-item">
                                    <div class="dropdown show">
                                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                            {{--@if($user->hasRole('Staff'))--}}
                                                {{--<a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>--}}
                                            {{--@endif--}}
                                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                        </div>
                                    </div>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-center mahir-icon">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="text-right header-btn">
                    <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
    <section class="order-listing-section">
        <div class="container">
            <div class="order-track-info">
                <div class="row margin-auto">
                    <div class="col-12">
                        <div class="profile">
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="media-heading">{{ Auth::user()->name }}</h4>
                                    <a href="{{url('profile')}}">Update Profile </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="order-track-detail">
                    <div class="col-lg-12 col-sm-12">
                        <div class="table-responsive table-bordered">
                            <table class="table table-hover">
                                <thead>
                                <tr style="background: #0f3e7c;">
                                    <th scope="col">Order Token</th>
                                    <th scope="col">Sub Service | Service</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $orders as $order)
                                <tr>
                                    <td>{{$order->token}}</td>
                                    <td>
                                        {{!is_null($order->subService) ?$order->subService->title:'N/A'}}
                                        | {{ !is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}
                                    </td>
                                    <td>{{ $order->mobile ? $order->mobile: $order->user->mobile}}</td>
                                    <td>{{ !is_null($order->status) ? $order->status->title == 'Cancel' ? 'Cancelled' : $order->status->title:'-- -- --'}}</td>
                                    <td>{{ $order->created_at ? Carbon\Carbon::parse($order->created_at)->format('h:s A Y-m-d') : '-- -- --'}}</td>

                                    <td>
                                        <a href="{{url('order-detail/'.$order->id)}}"  class="view-detail btn-info">View Detail</a>
                                        @if(!is_null($order->status) && $order->status->title == "Pending")
                                            <hr>
                                            {{--<a class="user-rating" href="{{ url('change-status/'.$order->id) }}" data-toggle="modal" data-target="#add-mobile">Rating us</a>--}}

                                            <hr>
                                            <a href="{{ url('change-status/'.$order->id) }}" class="view-detail btn-danger" onclick="return confirm('Are you sure to cancel your order?')" >Cancel</a>
                                        @endif
                                        {{--@if(!is_null($order->status) && $order->status->title == "Done")--}}
                                        {{--<button class="view-detail btn-info user-rating assign"--}}
                                                {{--data-order-id="{{$order->id}}"  title=" Order Detail">Rating us--}}
                                        {{--</button>--}}
                                            {{--@endif--}}

                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div class="pull-left">
                                {{ $orders->render() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="rating-modal">
        <div class="modal-dialog modal-gl">
            <div class="modal-content">

                <!-- Modal body -->
                <div class="modal-body" id="rating-content">
                </div>
            </div>
        </div>
    </div>

    @include('web.partials.footer')

@endsection
@section('script')
    <script type="text/javascript">
        $('.assign').click(function () {
            var orderID = $(this).data('order-id');
            $.ajax({
                url: "{{url('rating')}}/"+ orderID,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result) {
                    if (result != '') {
                        $('#rating-content').html(result);
                        $('#rating-modal').modal('show');
//
                    } else {
                        alert('error');
                    }

                }
            });
        });
    </script>
    @endsection
