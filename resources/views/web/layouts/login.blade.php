@extends('web.layouts.header-layout')
@section('meta')
    <title>Login | Home Maintenance and Repair Services | Mr Mahir</title>
    <meta charset="UTF-8">
    <meta name="description" content="We’re home repair and maintenance experts, and we are known for the quality of our workmanship and professional reliability. What can Mr Mahir do for you? Login now!">
@endsection
@section('content')
    <style>
        @media screen and (max-width: 767px) {
            .user-signup-section {
                position: relative;
                top: -110px;
            }
            .header-regist-btn .btn-register-order-now {
                width: 28% !important;
                font-size: 12px;
                position: relative;
                top: -28px;
            }
            .user-signup-form-section {
                padding: 40% 15%;
            }
            .user-signup-section {
                position: relative;
                top: -250px;
            }
            .mahir-regist-icon img{
                margin-top: -91px !important;
                width: 85px !important;
            }
            .header-btn .signup-signin{
                position: relative;
                top: 86px;
                z-index: 99999;
                cursor: pointer !important;
                width: 28% !important;
                font-size: 13px;
            }
        }

        @media screen and (max-width: 360px){
            .user-signup-section {
                position: relative;
                top: -130px;
            }
            }
    </style>
    <!-- Sign Up Section -->
    <section class="top-header-register-section" style="position: absolute;z-index: 999;left: 0;right: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-left header-btn">
                        @guest
                        <button class="signup-signin">
                            <a href="{{ url('register') }}">SignUp </a>
                        </button>
                        @endguest
                        @auth

                        @endauth

                        <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav cstm-nav">
                                    @auth
                                    <li class="nav-item">
                                        <div class="dropdown show">
                                            <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                                <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                                <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                            </div>
                                        </div>
                                    </li>
                                    @endauth
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="text-center mahir-regist-icon">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-right header-regist-btn">
                        <a href="{{ url('/order-now') }}"><button class="button btn-register-order-now">Book Now</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--for desktop view--}}
    <section class="signup-section login-mobile-hide">
        <div class="main-section">
            <div class="row margin-0">
                <div class="col-md-6 padd-0">
                    <div class="user-signup-section">
                        <div class="user-img-signup">
                            <img src="{{ asset('assets/images/mr-mahir.png') }}">
                        </div>
                        <h2>We Care About Your Home!</h2>
                        <a href="{{ url('register') }}" class="btn join-mr-mahir">Join Mr Mahir</a>
                    </div>
                </div>
                <div class="col-md-6 padd-0">
                    <div class="user-signup-form-section">
                        <h1>Welcome Back</h1>
                        <span class="fill-info">Fill your information to Login</span>
                        <form id="login-form" class="text-center signup-form">
                            <div class="form-group">
                                <input class="form-control cstm-control" placeholder="Phone Number" type="text" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control cstm-control" name="password" placeholder="Password" required>
                            </div>
                            <button class="btn sign-up-btn" type="submit">Login</button>
                        <div class="text-center" style="padding: 18px 0;">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--modal form--}}
    <div class="modal" id="myModal">
        <div class="modal-dialog" style="background-color: white;border-radius: 20px;">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button style="color: #0b2240 !important;" type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <form id="login-modal-form"  action="{{ route('password.email')}}" method="POST" class="text-center signup-form">
                       @csrf
                <div class="modal-body" style="padding: 0px 20px;">
                        <h3><i class="fa fa-lock fa-4x"></i></h3>
                        <h2 style="color: #0b2240 !important;font-size: 25px;" class="text-center forgot-pass">Forgot Password?</h2>
                        <p style="color: #0b2240 !important;">You can reset your password here.</p>
                        <div class="form-group emaill-input">
                            <input type="email" class="form-control" id="exampleInputEmail1" value="{{ $email ?? old('email') }}" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
                            <small style="color: #0b2240 !important;" id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                </div>
                    <button  style="margin: 0 auto;background-color: #ce171f !important;" class="btn btn-primary" type="submit">save</button>
                </form>
            </div>
        </div>
    </div>

{{--for mobile view--}}
    <section class="signup-section login-web-hide">
        <div class="main-section">
            <div class="row margin-0">
                <div class="col-md-6 padd-0">
                    <div class="user-signup-form-section">
                        <h1>Welcome Back</h1>
                        <span class="fill-info">Fill your information to Login</span>
                        <form id="login-form1" class="text-center signup-form">
                            <div class="form-group">
                                <input class="form-control cstm-control" placeholder="Phone Number" type="text" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control cstm-control" name="password" placeholder="Password" required>
                            </div>
                            <button class="btn sign-up-btn" type="submit">Login</button>
                            <div class="text-center" style="padding: 20px 0;">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 padd-0">
                    <div class="user-signup-section">
                        <div class="user-img-signup">
                            <img src="{{ asset('assets/images/mr-mahir.png') }}">
                        </div>
                        <h2>We Care About Your Home!</h2>
                        <a href="{{ url('register') }}" class="btn join-mr-mahir">Join Mr Mahir</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('web.partials.footer')

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $('#login-form, #login-form1').submit(function (e) {
            e.preventDefault();
            var $form = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('login') }}",
                data: $form,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    if(result.status){
                        toastr.success(result.message);
                        location.href = "{{ url('order') }}";
                    }else{
                        toastr.error(result.message);
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });
    </script>

@endsection

