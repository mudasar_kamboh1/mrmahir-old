<div class="order-track-section">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span style="color: white;" aria-hidden="true"><i class="fa fa-times"></i></span>
    </button>
    <h1><i id="rating-thumb-icon" class="fas fa-thumbs-up"></i></h1>
    <form action="{{route('ratingSubmit',$order->id)}}" method="post" class="order-form" id="mobile-form" novalidate="novalidate">
        @csrf

        <div class="row">
            <input type="hidden" name="order" value="{{ $order->id }}">
            <div class="row">
 {{--<input type="hidden" name="staff_id" id="" value="">--}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name" class="form-label">Service:</label>
                    <input type="text" id="textbox" class="form-control control num" placeholder="Service" name="Service" value="{{!is_null($order->subService) ?$order->subService->title:'N/A'}}" minlength="0" min="0" maxlength="11" readonly required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name" class="form-label">Sub-Service:</label>
                    <input type="text" id="textbox" class="form-control control num" placeholder="Sub Service" name="Sub service" value="{{ !is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}" minlength="0" min="0" maxlength="11" readonly required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="rate">
                <h6>Rate Your Experience</h6>
                <input type="radio" id="star5" name="rating" value="5" />
                <label for="star5" title="text">5 stars</label>
                <input type="radio" id="star4" name="rating" value="4" />
                <label for="star4" title="text">4 stars</label>
                <input type="radio" id="star3" name="rating" value="3" />
                <label for="star3" title="text">3 stars</label>
                <input type="radio" id="star2" name="rating" value="2" />
                <label for="star2" title="text">2 stars</label>
                <input type="radio" id="star1" name="rating" value="1" />
                <label for="star1" title="text">1 star</label>
            </div>
        </div>
        <button type="submit" class="btn text-capitalize" id="save-btn">Submit Feedback</button>
    </form>
</div>