@extends('web.layouts.banner-layout')

@section('content')
    <style>
        html{
            overflow-x: hidden;
        }
    </style>

<section class="top-header-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="text-left header-btn">
{{--                    @include('web.partials.drop-down')--}}
                    @guest
                    <button class="signup-signin">
                        <a style="color: #fff;font-weight: 300;" href="{{ url('register') }}">SignUp/ </a>
                        <a style="color: #fff;font-weight: 300;" href="{{url('login')}}"> SignIn</a>
                    </button>
                    @endguest
                    @auth
                    <div class="dropdown" id="drop" style="display: none;">
                        <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                        <div class="dropdown-content">
                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                            @if($user->hasRole('Staff'))
                                <a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>
                            @endif
                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </div>
                    @endauth
                    <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav cstm-nav">
                                @auth
                                <li class="nav-item">
                                    <div class="dropdown show" >
                                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                            @if($user->hasRole('Staff'))
                                                <a class="dropdown-item" href="{{url('staffOrder')}}">Order Detail</a>
                                            @endif
                                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                        </div>
                                    </div>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-center mahir-icon">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="text-right header-btn">
                    <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                </div>
            </div>
        </div>
    </div>
</section>

    <div class="container">
        <div class="order-track-detail">
            <div class="col-lg-12 col-sm-12">
                <div class="table-responsive" style="overflow-x: inherit;">
                    <table class="table table-hover table-bordered">
                    <thead>
                        <tr style="background: #0f3e7c;">
                            <th scope="col">Assign Date</th>
                            <th scope="col">Service</th>
                            <th scope="col">Sub Service</th>
                            <th scope="col">Staff Price</th>
                            <th scope="col">Rating</th>
                            <th scope="col">Status</th>
                            <th scope="col">Change Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                @if(count($orders) > 0)
              @foreach($orders as $order)
                    <tr>
                        <td>{{!is_null($order)?$order->pivot->assign_date:'-'}}</td>
                        <td>{{ !is_null($order->subService) && !is_null($order->subService->service) ? $order->subService->service->title:'N/A' }}</td>
                        <td>{{!is_null($order)?$order->Subservice->title:'-'}}</td>
                        <td>{{!is_null($order)?$order->pivot->staff_price:'-'}}</td>

                        <td>

                            <div class="row">
                                {{--<div class="row">--}}
                                    <div class="stars-img">
                                        <div class="stars-img">
                                            @php $rating = $order->pivot->rating; @endphp
                                            <div class="placeholder" style="color: lightgray;">
                                            </div>
                                            <div class="overlay" style="position: relative;top: -22px;">
                                                @foreach(range(1,5) as $i)
                                                    <span class="fa-stack" style="width:1em">
                                                   <i class="far fa-star fa-stack-1x"></i>
                                                        @if($rating >0)
                                                            @if($rating >0.5)
                                                                <i class="fas fa-star fa-stack-1x checked"></i>
                                                            @else
                                                                <i class="fas fa-star-half fa-stack-1x checked"></i>
                                                            @endif
                                                        @endif
                                                        @php $rating--; @endphp
                                                        </span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </td>
                        <td class="sorting_1 custom-card">
                        <button class="btn  btn-1">{{!is_null($order)?$order->pivot->status:'-'}}</button>
                        </td>
                        <td>
                            <form action="{{route('changeStatusOrder',$order->id)}}" method="post">
                                @csrf
                                {{--{{dd($order->id)}}--}}
                            <input type="hidden" name="order" value="{{ $order->id }}">
                                {{--<input type="hidden" name="staffID" value="{{!is_null($order)?$order->pivot->staff_id:'--'}}">--}}

                                <select name="status" class="change-status ">
                        <td>
                            <button  class="btn track-order profile-detail btn-info waves-effect waves-light assign"
                                    data-order-id="{{$order->id}}"  title="Detail">View Detail
                            </button>
                        </td>
                    </tr>
                @endforeach
                    @else
                    <tr><td colspan="6">No, Order Found!</td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

    <div class="modal fade" id="staff-detail-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal body -->
                <div class="modal-body" id="staff-detail-content">
                </div>
            </div>
        </div>
    </div>


@include('web.partials.footer')

@endsection
@section('script')
    <script>
        $(document).on('change','.change-status',function () {
            $(this).parent('form').submit();
        });
        $('.assign').click(function () {
            var orderID = $(this).data('order-id');
            $.ajax({
                url: "{{url('viewStaff')}}/"+ orderID,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result) {
                    if (result != '') {
                        $('#staff-detail-content').html(result);
                        $('#staff-detail-modal').modal('show');
//
                    } else {
                        alert('error');
                    }
                }
            });
        });
    </script>
@endsection