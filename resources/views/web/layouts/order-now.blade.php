@extends('web.layouts.banner-layout')
@section('meta')
    <title>Order Now | Home Maintenance and Repair Services | Mr Mahir</title>
    <meta  name="description" content="Order various home services like electrical, plumbing, painting, gardening and handymen online - 24 hours a day, 7 days a week - in the comfort of your home and get best competitive rates.">
@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}">
    <style>
        .top-header-section{
            margin-bottom: -87px;
        }
        .chosen-container-single .chosen-single{
            border: none !important;
            border-radius: 0px !important;
        }


        @media screen and (max-width:767px){
            .header-btn h4{
                top: 2px !important;
            }
            .request-form .form-group {
                display: block;
            }
            .header-btn .signup-signin {
                position: relative;
                top: 9px;
                width: 28% !important;
                font-size: 12px;
            }
            #drop img {
                position: relative;
                top: 5px;
                z-index: 999;
            }
            .dropdown-content {
                top: 56px;
            }
        }

        @media screen and (max-width:600px){
            .form-call{
                width: 90% !important;
            }
        }

        @media screen and (max-width:576px){
            .header-btn2 h4{
                font-size: 17px;
                font-weight: 600;
            }

            .header-btn2 h4 a{
                color: black !important;

            }
            .header-btn h4{
                display: none !important;
            }
            .header-btn {
                padding: 32px 0 !important;
            }
        }

        @media screen and (max-width:1200px){
            .header-btn h4{
                font-size: 18px;
            }

        }
    </style>
@endsection

@section('content')
    <main class="home-body order-body">
    <section class="top-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-left header-btn">
                        @include('web.partials.drop-down')
                        <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav cstm-nav">
                                    @auth
                                    <li class="nav-item">
                                        <div class="dropdown show">
                                            <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                                <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                                <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                            </div>
                                        </div>
                                    </li>
                                    @endauth
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="text-center mahir-icon">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/mahir-logo.png') }}" width="100px">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">

                </div>
            </div>
        </div>
    </section>
    <!-- order form section -->
    <section class="form-section">
        <div class="container">
            <div class="main-heading text-center">
                <h2>Book your <span class="redd">Service</span></h2>
            </div>
            <form class="order-form" id="instanceOrder">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group for-border">
                            <select data-placeholder="Select Your Service" class="chosen-select form-control cstm-control" tabindex="2" id="services" required>
                                <option value="">Select Service</option>
                                @foreach($services as $service)
                                    <option value="{{ $service->id }}" {{ $service->slug == app('request')->input('service') ? 'selected' : '' }}>{{ $service->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group for-border">
                            <select data-placeholder="Select Your SubService" class="chosen-select sub-services form-control cstm-control" name="subService" tabindex="2" id="sub_servics" required>
                                <option value="">Select Sub Service</option>
                                @if(count($sub_services))
                                    @foreach($sub_services as $sub_service)
                                        <option value="{{ $sub_service->id }}" {{ str_replace(' ','-', $sub_service->title)== str_replace(' ','-',app('request')->input('sub_service'))? 'selected' : '' }}>{{ $sub_service->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="controls input-append date form_datetime " data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input size="16" class="form-control cstm-input-field"  name="schedule_datetime" type="text"  placeholder="Select Time Slot" autocomplete="off" required>
                                <span class="add-on"><i class="icon-remove"></i></span>
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="cstm-input-field" name="mobile" type="text" maxlength="11" minlength="11" placeholder="Mobile Number" value="{{ Auth::check() ? Auth::user()->mobile : ''}}" title="Please add a valid Number" required="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group left-inner-addon for-border">
                            <select data-placeholder="Select Your area" class="chosen-select form-control cstm-control" name="area" id="area_id" tabindex="2" id="area" required>
                                <option value="">Select Area</option>
                                @foreach( $areas as $area)
                                    <option value="{{$area->id}}">{{$area->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group left-inner-addon">
                            <input type="text" name="address" class="form-control cstm-input-field" value="{{ Auth::check() ? Auth::user()->address : ''}}" placeholder="Street Address" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="cstm-input-field" type="text" name="problem_message" placeholder="Describe Your Problem" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if(!Auth::check() || !Auth::user()->name)
                            <div class="form-group">
                                <input class="cstm-input-field" name="name" type="text" placeholder="Full Name" maxlength="50" title="Name consist on letters only" required="">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        @if(!Auth::check() || !Auth::user()->email)
                            <div class="form-group">
                                <input class="cstm-input-field-order" name="email" type="text" placeholder="Email" required="">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12 submit-btn">
                        <button type="submit" class="btn btn-submit btn-md">Submit</button>
                    </div>
                </div>
            </form>
        </div>

        <section class="order-page-sec-call">
            {{--<div class="main-heading text-center">--}}
                {{--<h2><span class="redd">OR</span></h2>--}}
            {{--</div>--}}
            <div class="form-call">
                <h4>Call Us <strong class="phone-number">03096661919</strong> Or Request for a Call</h4>
                <form class="request-form" id="formsubmitbutton">
                    <div class="form-group">
                        <input class="cstm-input-control" type="text" value="" placeholder="Full Name" name="name" aria-describedby="name-format" required="" pattern="[a-zA-Z ]+" title="Name consist on Alphabets only">
                    </div>
                    <div class="form-group">
                        <input class="cstm-input-control" placeholder="Phone Number" type="text" value="" name="mobile" pattern="\d*" onkeypress="if(this.value.length == 11) return false;" title="E.g. 03001234567" maxlength="11" minlength="11" required="">
                    </div>
                    <div class="form-group">
                        <button class="button btn-Request-a-Call-back">Request a Call back</button>
                    </div>
                </form>
            </div>
        </section>
    </section>

        @include('web.partials.footer')
@endsection

@section('script')
    <script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                startDate: '-0d',
                todayBtn:  1,
                format: 'dd-mm-yyyy HH:iiP',
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            });
        });


            $('#services').on('change',function () {
            var service = $(this).val();
            $('.sub-services option').remove();
            $('.sub-services').trigger("chosen:updated");
            $('.sub-services').append('<option value="" disabled="disabled" selected>Select Sub Service</option>');
            $.ajax({
                type: 'GET',
                url: "{{ url('getSubServices') }}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        $('.sub-services').append('<option value="'+row.id+'" class="list">'+row.title+'</option>');
                    });
                    $('.sub-services').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });

    </script>
@endsection
