@extends('web.layouts.banner-layout')
@section('meta')
    <title>Reliable and Trusted Home Maintenane and Repair Services</title>
    <meta  name="description" content="We have taken the stress out of maintaining and improving your home. With Mr Mahir, home repair & maintenance services are just one call away.">
@endsection
@section('content')
{{--@include('web.partials.header-with-banner')--}}
<section class="top-header-ratelist-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="text-left header-btn">
                    @guest
                    <button class="signup-signin">
                        <a href="{{ url('register') }}">SignUp/ </a>
                        <a href="{{url('login')}}"> SignIn</a>
                    </button>
                    @endguest
                    @auth
                    <div class="dropdown" id="drop" style="display: none;">
                        <img src="{{ asset('assets/images/avatar.png') }}" alt="" style="width:50px;height:50px;">
                        <div class="dropdown-content">
                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </div>
                    @endauth

                    <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav cstm-nav">
                                @auth
                                <li class="nav-item">
                                    <div class="dropdown show">
                                        <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/images/avatar.png') }}" alt="" style="width:50px;height:50px;">
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                            <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                            <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                        </div>
                                    </div>
                                </li>
                                @endauth
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-center mahir-icon" id="logo-of-mahir">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="text-right header-btn" id="book-now-right">
                    <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                </div>
            </div>
        </div>
    </div>
</section>

    <section class="about-section">
        <div class="container">
            <div class="main-heading">
                <h2>How Can Mr. Mahir <span class="redd">Help You?</span></h2>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="about-description" id="conditions">
                        <p class="text-justify">Mr. Mahir is your very own online platform that provides you handyman services efficiently in a convenient way.
                        Our Mahirs are on the mission of saving you the hassle of getting robbed by inexperienced and money-making handymen who are only concerned about earning money.</p>
                        <p class="text-justify"> Mr. Mahir is here at your service to change the perception of a handyman as money snatchers to beautifiers who bring improvement to your home or workspace
                            by providing the best repair, home improvement, and maintenance services.</p>
                        <p class="text-justify">You don’t have to put your life on hold and risk to fix a door that no longer closes, dry your moist walls, or just fix any electronics. Our team is just a call away and arrives with all the equipment according to the job nature to provide reliable, efficient, and affordable handyman service.
                        </p>
                    </div>
                </div>

                @include('web.partials.quote')

                    </div>
                </div>
    </section>

    <!-- Expand  Business section -->
    <section class="expand-business-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Expand your service business with Mr. Mahir</h3>
            </div>
            <div class="row">
                <div id="business-slider" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Electricion</h4>
                            <p>Saleem is an experienced and knowledgeable electrician with more than 6 years of experience in the field.
                                He has been a strong asset of Mr. Mahir for 4 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Carpenters</h4>
                            <p>Meet Raheem who has an exceptional and broad experience of 3 years in carpentry and woodworking.
                                He is a strong resource for Mr. Mahir for more than a year.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Plumbers</h4>
                            <p>Jamil is a highly skilled plumber with 8 years’ experience in both industrial and commercial plumbing.
                                He has been serving our customers with quality work for 5 years now. </p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Handymen</h4>
                            <p>Our handyman Ahmed is a true professional with 2 years of experience in the field.
                                He has proven to be the best resource of Mr. Mahir with high customer rating.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>

                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>Painters</h4>
                            <p>Aleem offers years of experience on a wide variety of painting projects.
                                Our customers can’t help but to give him 5 star rating for his quality work.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="expand-business-desc text-center">
                            <img src="{{ asset('assets/images/human.png') }}" alt="">
                            <h4>A/C Technicians</h4>
                            <p>Faisal is an accomplished air conditioning technician with more than 8 years of experience working on commercial and residential AC units.
                                Known to paying close attention to our customers’ needs.</p>
                            <a href="{{ url('register') }}" target="_blank">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- counter section -->

    <!-- Clients section -->
    <section class="clients-section">
        <div class="container">
            <div class="main-heading">
                <h3 class="text-center">Customer Reviews</h3>
            </div>
            <div class="row">
                <div id="clients-slider" class="owl-carousel owl-theme">

                    @foreach($Facebook_review as $facebook)

                        <div class="item">
                            <div class="clients-testimonial card">
                                <div class="client-info">
                                    <div class="media">
                                        <img class="mr-3" src="{{ asset('fbImages/'.$facebook->image) }}" alt="">
                                        <div class="media-body">
                                            <h5 class="mt-0">{{$facebook->name}}</h5>
                                        </div>
                                    </div>

                                    <div class="pehra-of-rate">
                                        <p>{{$facebook->description}} </p>
                                        <div class="row">
                                            <div class="reviews-fb">
                                                <a target="_blank" href="https://www.facebook.com/pg/teammahir/reviews/?ref=page_internal">Reviewed on Facebook</a>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="stars-img">
                                            <div class="stars-img">
                                                @php $rating = $facebook->rating; @endphp
                                                <div class="placeholder" style="color: lightgray;">

                                                </div>

                                                <div class="overlay" style="position: relative;top: -22px;">

                                                    @foreach(range(1,5) as $i)
                                                        <span class="fa-stack" style="width:1em">
                                                   <i class="far fa-star fa-stack-1x"></i>

                                                            @if($rating >0)
                                                                @if($rating >0.5)
                                                                    <i class="fas fa-star fa-stack-1x checked"></i>
                                                                @else
                                                                    <i class="fas fa-star-half fa-stack-1x checked"></i>
                                                                @endif
                                                            @endif
                                                            @php $rating--; @endphp
                                                    </span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('web.partials.footer')
@endsection

</body>
