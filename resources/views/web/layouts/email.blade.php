<section class="top-header-section">
    <div class="container">
        <div class="mahir-icon" style="text-align: center;">
            <a href="{{ url('/') }}">
                <img src="{{ asset('assets/images/mahir-logo.png') }}" width="100px">
            </a>
        </div>
    </div>
</section>
<div style="margin:0;padding:60px;font-size:14px;font-family:Helvetica,Arial,sans-serif;line-height:19px;color:#333333;background-color:#ffffff">
    <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
        <tbody>
        <tr>
            <td style="padding-top:40px;padding-right:20px;padding-left:20px" align="left">
                <h2 style="margin: 30px 0 50px 0;font-weight:bold;font-size:18px;font-family:Helvetica,Arial,sans-serif;line-height:26px;margin:0">Hi ,{{ !is_null($user)? $user->name : 'N/A' }} </h2>
            </td>
        </tr>
        <tr>
            <td style="padding:20px 20px" align="left">
                <div style="font:normal 14px Helvetica,Arial,sans-serif;line-height:19px;color:#333333">
                    We've received a request to reset your password. If you didn't make the request , just ignore this email.<a href="#" target="_blank"></a>. <a style="text-decoration:underline;color:#00aff0;font-weight:bold" href="#" target="_blank">Visit your account</a> to review your changes.<br>
                    <br>
                    <div class="dsa" style="background: #0b2240;height: 40px;padding-top: 20px;">
                        <p class="MsoNormal" style="MARGIN:0in 0in 0pt 1in">
                        <span lang="EN-GB" style="font-size:17px;color:white;">
                            o<b>Your Password is</b>
                            <mark style="margin: 0 75px;">: {{ !is_null($user) && $user->password_hint? decrypt($user->password_hint) : ''}}</mark>
                        </span>
                        </p>
                    </div>
                    <br>
                    Didn’t request this change? Please contact <a style="text-decoration:underline;color:#00aff0;font-weight:bold" href="#" target="_blank">Customer Support</a> immediately as someone may have fraudulently gained access to your account.</div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
