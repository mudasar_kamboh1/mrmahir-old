@extends('web.layouts.banner-layout')
@section('meta')
    @if(isset($meta_service))
        <title>{{$meta_service->meta_title}}</title>
        <meta property="description" content="{{$meta_service->meta_description}}">
    @else
        <title>Home Maintenance and Repair Services in Lahore</title>
        <meta property="description" content="Mr. Mahir is your one-call solution for a wide range of home maintenance and repair needs. We arrive on time in uniform and a marked van with the tools to complete the job right.">
    @endif

@endsection
@section('content')
    <style>
        body::-webkit-scrollbar { width: 0 !important; }

        @media screen and (max-width:360px){
            .footer {
                padding: 20px 0px !important;
            }
        }
    </style>
    <section class="top-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-left header-btn">
                        @include('web.partials.drop-down')
                        <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav cstm-nav">
                                    @auth
                                    <li class="nav-item">
                                        <div class="dropdown show">
                                            <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <img alt="" src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                                <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                                <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                            </div>
                                        </div>
                                    </li>
                                    @endauth
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="text-center mahir-icon">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-right header-btn">
                        <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service-section">
        <div class="container">
            <div class="main-heading">
                <h2>How Can Mr. Mahir <span class="redd">Help You?</span></h2>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="about-description">
                        <ul class="nav nav-justified md-tabs price-tabs" >
                            @foreach($services as $k => $service)
                            <li class="nav-item">
                                <a class="nav-link {{ $service->slug == request()->route('slug') ? 'active' : '' }}" href="{{ url('services/'.$service->slug) }}"> {{$service->title}}</a>
                            </li>
                            @endforeach
                            </ul>

                        <div class="tab-content pt-5" id="myTabContentJust" style="padding-top: 0px !important;">
                            <div class="tab-pane fade show active" id="home-just">
                                <table class="table table-hover table-striped table-bordered table-sm dtBasicExample">
                                    <thead>
                                    <tr>
                                        <th class="img-hide-on-mobile" scope="col"></th>
                                        <th scope="col">Sub Service</th>
                                        <th scope="col">Rates in PKR</th>
                                        <th scope="col">Book Now</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($sub_services))
                                        @foreach($sub_services as $k => $sub_service)
                                            <tr>
                                                <td class="img-hide-on-mobile">
                                                    @if($sub_service->image)
                                                    <img src="{{ asset('thumbnailImg/'.$sub_service->image) }}" alt="{{$sub_service->alt}}" width="60px"  style="height: 40px;margin-top: 7px; border-radius: 10%;">
                                                   @else

                                                    @endif
                                                </td>
                                                <td class="text-table">
                                                    <strong class="text-capitalize" style="font-weight: 600;">{{$sub_service->title}}</strong>
                                                </td>
                                                <td><strong style="font-weight: 600;">{{$sub_service->price_comment}}</strong>-{{$sub_service->price}}</td>
                                                <td>

                                                    @php
                                                    $serviceSelected = !is_null($sub_service->service) ? $sub_service->service->slug : '-';
                                                    @endphp
                                                    <a href="{{ url('order-now?service='.$serviceSelected.'&sub_service='.$sub_service->title) }}">
                                                        <button class="btn order-now">Book Now</button></a></td>
                                            </tr>

                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @include('web.partials.quote')
            </div>
        </div>
    </section>
    @include('web.partials.footer')
@endsection
