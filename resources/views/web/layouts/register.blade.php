@extends('web.layouts.header-layout')
@section('meta')
    <title>Register Now | Home Maintenance and Repair Services | Mr Mahir</title>
    <meta  name="description" content="Try our reliable platform to find expert handymen today. Register yourself at Mr Mahir for hassle-free home maintenance services.">
@endsection
@section('content')
    <style>
        @media screen and (max-width: 767px) {
            .header-btn .signup-signin {
                position: relative;
                top: 74px;
                width: 28% !important;
                z-index: 99999;
            }
            .header-regist-btn .btn-register-order-now{
                width: 28% !important;
                position: relative;
                top: -24px;
                font-size: 12px;
            }
            .user-register-signup-form-section{
                position: relative;
                top: -167px;
            }
            .mahir-regist-icon img{
                margin-top: -66px !important;
                width: 85px !important;
            }
        }

        @media screen and (max-width: 400px){
            .user-register-signup-section h1{
                padding: 20px 0 !important;
            }
        }

    </style>
    <!-- Sign Up Section -->
    <section class="top-header-register-section" style="position: absolute;z-index: 999;left: 0;right: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-left header-btn">
                        @guest
                        <a href="{{url('login')}}" style="color: #fff;font-weight: 300;" > <button class="signup-signin">SignIn</button></a>
                        @endguest
                        @auth
                        <div class="dropdown" id="drop" style="display: none;">
                            <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                            </div>
                        </div>
                        @endauth
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="text-center mahir-regist-icon">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-right header-regist-btn">
                        <a href="{{ url('/order-now') }}"><button class="button btn-register-order-now">Book Now</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="signup-section">
        <div class="main-section">
            <div class="row margin-0">
                <div class="col-md-6 padd-0">
                    <div class="user-register-signup-section">
                        <h1>Sign up as User</h1>
                        <span class="fill-info">Fill your information to Sign up your account</span>
                        <form class="text-center signup-form">
                            <div class="form-group">
                                <input type="text" class="form-control cstm-control" name="name" placeholder="Name" title="Name consist on letters only" required="">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control cstm-control" name="email" placeholder="Email" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control cstm-control" name="mobile" placeholder="Mobile Number" maxlength="11" minlength="11" title="Please add a valid Number" onkeypress="if(this.value.length==11) return false;" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control cstm-control" name="password" placeholder="Password" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control cstm-control" name="password_confirmation" placeholder="Confirm Password" required="">
                            </div>
                            <!-- Sign in button -->
                            <button class="btn sign-up-btn my-4" type="submit">Sign up  me as user</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 padd-0">
                    <div class="user-register-signup-form-section">
                        <h1>Sign up as Professional</h1>
                        <span class="fill-info">Fill your information to Sign up your account</span>
                        <form class="text-center signup-form">
                            <input type="hidden" name="is_staff" value="1">
                            <div class="form-group">
                                <input type="text" class="form-control cstm-control" name="name" placeholder="Name" pattern="[a-zA-Z ]+" title="Name consist on letters only" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control cstm-control" name="email" placeholder="Email">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control cstm-control" name="mobile" onkeypress="if(this.value.length==11) return false;" placeholder="Mobile Number" maxlength="11" minlength="11" title="Please add a valid Number" required>
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group left-inner-addon">
                                        <select name="areas[]"  data-placeholder="Select Your area" class="chosen-select form-control cstm-control" required>
                                            <option value="">Select Area</option>
                                            @foreach($areas as $area)
                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select data-placeholder="Select Your Service" class="chosen-select form-control cstm-control" tabindex="2"  name="services" id="services" required>
                                        <option value="">Select Service</option>
                                        @foreach($services as $service)
                                            <option value="{{$service->id}}">{{$service->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group left-inner-addon">
                                        <select data-placeholder="Select Your SubService" class="chosen-select form-control cstm-control sub-services" multiple name="sub_service_id[]"  tabindex="2"  required>
                                            <option value="">Select Sub Service</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" class="form-control cstm-control" name="password" placeholder="Password" required>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <input type="password" class="form-control cstm-control" name="password_confirmation" placeholder="Confirm Password" required>
                                </div>
                                </div>
                                <!-- Sign in button -->
                                <button class="btn sign-up-btn my-4" type="submit">Sign up me as professional</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- footer section -->
    <section class="footer">
        <div class="container-fluid">
            <div class="footer-nav">
                <ul>
                    <li> <a href="{{ url('/') }}">Home</a></li>
                    @php $services = \App\Service::get(); @endphp
                    @if(count($services)  > 0)
                        @foreach( $services as $k=>$service)
                            @if($k==1)
                                @break
                            @endif
                            <li class=""><a href="{{ url('services?sub-service='.$service->slug)}}">Service</a></li>
                        @endforeach
                    @endif
                    <li> <a href="{{ url('rate-list') }}">Rate list</a></li>
                    <li> <a href="{{ url('about-us') }}">About Us</a></li>
                    <li> <a href="{{ url('portfolio') }}">Portfolio</a></li>
                </ul>
            </div>
        </div>
        <!--  Whatsapp live chat   -->
        <a class="whatsapp-link" href="https://api.whatsapp.com/send?phone=+923096661919&text=Hi Mr.Mahir!" target="_blank" style="position: fixed;right: 27px;bottom: 43px;"><img src="{{ asset('assets/images/whatsapp-512.png') }}" class="img-circle" width="60px"></a>
    </section>
    {{--Social fixed side bar--}}
    <div id="fixed-social">
        <div>
            <a href="https://www.facebook.com/teammahir/" class="fixed-facebook" target="_blank"><i class="fab fa-facebook-f"></i> <span>Facebook</span></a>
        </div>
        <div>
            <a href="https://twitter.com/MahirTeam" class="fixed-twitter" target="_blank"><i class="fab fa-twitter"></i> <span>Twitter</span></a>
        </div>
        <div>
            <a href="https://www.instagram.com/teammahir/" class="fixed-instagram" target="_blank"><i class="fab fa-instagram"></i> <span>Instagram</span></a>
        </div>
        <div>
            <a href="https://www.youtube.com/channel/UCICIsVRC6pNmqLRYilG5bNQ" class="fixed-youtube" target="_blank"><i class="fab fa-youtube"></i> <span>Youtube</span></a>
        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript">
        $('.signup-form').submit(function (e) {
            e.preventDefault();
            $this = $(this);
            var $form = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('register') }}",
                data: $form,

                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    toastr.success('Successfully Registered!');
                    if($this.find('input[name="is_staff"]').length > 0){
                        window.location = "{{ url('/profile') }}";
                    }
                    else{
                        window.location = "{{ url('/home') }}";
                    }
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });
        $('#services').on('change',function () {
            var service = $(this).val();
            $('.sub-services option').not('option:selected').remove();
            $('.sub-services').trigger("chosen:updated");
            $.ajax({
                type: 'GET',
                url: "{{ url('getSubServices') }}/" + service,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    $.each(result.data, function (i, row) {
                        $('.sub-services').append('<option value="'+row.id+'" class="list">'+row.title+'</option>');
                    });
                    $('.sub-services').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        });
    </script>

@endsection