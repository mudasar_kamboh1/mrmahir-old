@extends('web.layouts.header-layout')

@section('content')
    <section class="top-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-left header-btn">
                        @guest
                            <button class="signup-signin">
                                <a style="color: #fff;font-weight: 300;" href="{{ url('register') }}">SignUp/ </a>
                                <a style="color: #fff;font-weight: 300;" href="{{url('login')}}"> SignIn</a>
                            </button>
                        @endguest
                        @auth
                            <div class="dropdown" id="drop" style="display: none;">
                                <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                <div class="dropdown-content">
                                    <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                    <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                    <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                </div>
                            </div>
                        @endauth

                        <nav class="navbar custom-navbar navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav cstm-nav">
                                    @auth
                                        <li class="nav-item">
                                            <div class="dropdown show">
                                                <button class="btn cstm-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <img src="{{ asset('assets/images/avatar.png') }}" style="width:50px;height:50px;">
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="{{ url('order') }}">My Order</a>
                                                    <a class="dropdown-item" href="{{ url('profile') }}">My Profile</a>
                                                    <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                                </div>
                                            </div>
                                        </li>
                                    @endauth
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="text-center mahir-icon">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/mahir-logo.png') }}" width="100px">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-right header-btn">
                        <a href="{{ url('/order-now') }}"><button class="button btn-order-now">Book Now</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="notfound-section">
        <div class="container">
            <div id="notfound">
                <div class="notfound">
                    <div class="notfound-404">
                        <h1>Oops!</h1>
                    </div>
                    <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
                    <a href="{{ url('/') }}">Go To Homepage</a>
                </div>
            </div>
        </div>
    </section>
    @include('web.partials.footer')

@endsection