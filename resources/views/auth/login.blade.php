@extends('layouts.master')
<head>
    <style>

        body {
            font-family: 'Poppins', sans-serif;
            margin: 0;
            background-image: url(images/background.png);
            background-size: cover;
        }

        .login-form {
            width: 300px;
            box-sizing: border-box;
            margin: 30vh auto 0;
            text-align: center;
            border-radius: 5px;
            color: #fff;
            background: rgba(0, 0, 0, .3);
            position: relative;
            padding: 40px 40px 20px;
        }

        .login-form img {
            width: 100px;
            position: absolute;
            top: 0;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .input-box {
            position: relative;
        }

        .input-box input {
            display: block;
            width: 100%;
            border: 1px solid rgba(0, 0, 0, .4);
            background: rgba(0, 0, 0, .4);
            color: #FFF;
            padding: 5px 5px 5px 15px;
            margin-bottom: 15px;
            transition: .30s;
        }
        .input-box input:focus{
            border: 1px solid #3498db;
        }

        .input-box span {
            position: absolute;
            top: 0px;
            left: 4px;
        }
        .login-form input[type="submit"]{
            display: inline-block;
            width: 50%;
            background: rgba(0,0,0,.6);
            border: 1px solid rgba(0,0,0,.3);
            cursor: pointer;
            padding: 5px;
            color: #fff;
        }

        .login-form a{
            display: inline-block;
            text-decoration: none;
            color: #fff;
            font-weight: bold;
            margin-top: 15px;
        }

    </style>
</head>
@section('content')
<div class="login-box" >
    <div class="login-logo" style="font-weight: bold;">
        Mr Mahir
    </div>



    <div class="card">

        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ __('Login') }}</p>

            <form action="{{ route('adminLogin') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    <div class="input-group-append">
                        <span class="fa fa-envelope input-group-text"></span> @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span> @endif
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    <div class="input-group-append">
                        <span class="fa fa-lock input-group-text"></span> @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="checkbox icheck">
                            <label>
                <input type="checkbox"> Remember Me
              </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <div class="social-auth-links text-center mb-3">

        <!-- /.login-card-body -->
    </div>
</div>
    </div>
<!-- /.login-box -->
</div>
@endsection