<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content={{csrf_token()}}>


    <title>MrMahir</title>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/invoice.css')}}">

    <link rel="stylesheet" href="    {{ asset('dist/plugins/font-awesome/css/font-awesome.min.css') }}">
    {{--<link rel="stylesheet" href="{{asset('dist/css/DatePickerX.css')}}">--}}
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dist/js/pages/morris.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{ asset('assets/css/chosen.css') }}">

    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/summernote/summernote-bs4.css')}}">

    {{--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">--}}

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/images/mahir-logo-fav.png') }}">

    @toastr_css
    <style>
        .chosen-container{
            width: 100% !important;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini" style="background-image: url({{ asset('assets/images/back5.png') }});background-repeat: no-repeat;background-size: cover;">
    @guest @yield('content') @else
    <div class="wrapper" id="app">
        <!-- Header -->
    @include('layouts.header')
        <!-- Sidebar -->
    @include('layouts.sidebar') @yield('content')
        <!-- Footer -->
    @include('layouts.footer')
    </div>
    @endguest
    <!-- ./wrapper -->
    <script src="{{ asset('dist/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/css/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button)

    </script>
{{--    <script src="{{ asset('assets/css/plugins/chart.js/Chart.min.js') }}"></script>--}}

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>--}}
    {{--<script src="{{ asset('assets/css/dist/js/pages/morris.min.js') }}"></script>--}}
    <script src="{{ asset('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    {{--{!! $chart->script() !!}--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js"></script>--}}
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('assets/js/chosen.jquery.js') }}"></script>
{{--    <script src="{{ asset('assets/css/dist/js/pages/dashboard.js') }}"></script>--}}
{{--<script src="{{asset('dist/js/DatePickerX.js')}}"></script>--}}





    {{--<script src="{{asset('dist/js/DatePickerX.js')}}"></script>--}}

    @toastr_js
    @toastr_render




    @yield('javascript')
    <script>
        $(function(){
            @if(count($errors) > 0)
                   @foreach($errors->all() as $error)
toastr.error("{{ $error }}");
            @endforeach
            @endif
        });
    </script>



    <script>
        $('select').chosen({allow_single_deselect: true});
    </script>


</body>
</html>