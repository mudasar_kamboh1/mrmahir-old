<style>
    .main-sidebar {
        min-height: inherit !important;
    }
</style><!-- Main Sidebar Container -->



<aside class="main-sidebar sidebar-dark-primary elevation-4" style="overflow: hidden !important;">
<!-- Main Sidebar Container -->

    {{--<!-- Brand Logo -->--}}
    <a href="#" class="brand-link" style="border-bottom:0px solid #4b545c;background: #eaeaea;">
        <strong>
            <div class="text-center mahir-icon">
                    <img src="{{ asset('assets/images/mahir-logo.png') }}" alt="mahir-icon" width="100px">
            </div>
            {{--<span class="nav-link"></span>--}}
        </strong>

        <span class="brand-text font-weight-light"></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flexx">
            <div class="image">
                <img src="/img/profile.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info" style="margin: -17px 0;">
                <a href="#" class="d-block text-capitalize"> {{auth()->user()->name!=null ? auth()->user()->name : "Administrator"}} </a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{url('admin/dashboard')}}" class="nav-link">
{{--                    <a href="{{ route('admin/dashboard') }}" class="nav-link {!! classActiveSegment(2, 'admin/dashboard') !!}">--}}
                        <i class="fas fa-circle" style="font-size: 23px;padding-right: 10px;"></i>
                        <p>Dashboard </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/orders')}}" class="nav-link">
                        <i class="fa fa-first-order" style="font-size: 23px;padding-right: 10px;"></i>
                        <p>
                            Orders
                            <span class="right badge badge-danger"></span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a  href="{{url('admin/quotes')}}" class="nav-link">
                        <i class="fas fa-quote-right" style="font-size: 20px;padding-right: 12px;"></i>
                        <p>
                            Request
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/users')}}" class="nav-link">
                        <i class="fas fa-user" style="font-size: 23px;padding-right: 12px;"></i>
                        <p>
                            User
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>

                </li>
                <li class="nav-item">
                    <a href="{{url('admin/staffs')}}" class="nav-link">
                        <i class="fas fa-user-friends" style="font-size: 19px;padding-right: 9px;"></i>
                        <p>
                            Staff
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>


                </li>
                <li class="nav-item">
                    <a href="{{url('admin/Facebook')}}" class="nav-link">
                        <i class="fab fa-rev" style="font-size: 23px;padding-right: 9px;"></i>
                        <p>
                            Facebook Review
                            <span class="bs-tooltip-right"> </span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/services')}}" class="nav-link">
                        <i class="fab fa-servicestack" style="font-size: 23px;padding-right: 9px;"></i>
                        <p>
                            Service
                            <span class="bs-tooltip-right"> </span>
                        </p>
                    </a>

                </li>

                <li class="nav-item">
                    <a  href="{{url('admin/subServices')}}" class="nav-link">
                        <i class="fab fa-servicestack" style="font-size: 23px;padding-right: 12px;"></i>
                        <p>
                            Sub Service
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>

                </li>
                <li class="nav-item">
                    <a href="{{url('admin/trendingService')}}" class="nav-link">
                        <i class="fab fa-servicestack" style="font-size: 23px;padding-right: 9px;"></i>
                        <p>
                            Trending Service
                            <span class="bs-tooltip-right"> </span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/areas')}}" class="nav-link">
                        <i class="fas fa-map-marked-alt" style="font-size: 21px;padding-right: 9px;"></i>
                        <p>
                            Area
                            <span class="right badge badge-danger"></span>
                        </p>
                    </a>
                </li>


                <li class="nav-item">
                    <a  href="{{url('admin/galleries')}}" class="nav-link">
                        <i class="fas fa-images" style="font-size: 23px;padding-right: 12px;"></i>
                        <p>Gallery
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a  href="{{url('admin/categories')}}" class="nav-link">
                        <i class="fas fa-cube" style="font-size: 23px;padding-right: 12px;"></i>
                        <p>Category
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a  href="{{url('admin/sub_categories')}}" class="nav-link">
                        <i class="fas fa-cubes" style="font-size: 23px;padding-right: 12px;"></i>
                        <p>
                            SubCategory
                            <span class="bs-tooltip-right"></span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off"style="font-size: 21px;padding-right: 12px;"></i>
                        <P>
                            {{ __('Logout') }}</P>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
@yield('content')

<!-- /.sidebar -->
</aside>

