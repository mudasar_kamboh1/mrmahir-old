<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
           //<-----login and register api----->
Route::post('register', 'Api\RegisterController@register');
Route::post('login','Api\LoginController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('userDetail', 'Api\UserController@details');
    Route::post('logout','Api\LoginController@logout');
    Route::resource('order','Api\OrderController');
    Route::get('index','Api\OrderController@index');

    Route::get('Test','Api\UserController@Test');
    Route::get('order-detail','Api\OrderController@orderDetail');
    Route::post('order/cancel/{id}','Api\OrderController@cancelOrder');
//    Route::post('order/store/{id}','Api\OrderController@store');
    Route::post('update-profile','Api\UserController@updateProfile');
//    Route::PUT('add-Address/{id}','Api\OrderController@addAddress')->name('add-Address');
    Route::PATCH('orderUpdate/{id}','Api\OrderController@orderUpdate')->name('orderUpdate');
    Route::PATCH('add-message/{id}','Api\OrderController@addMessage')->name('add-message');
    Route::post('trendingOrder','Api\OrderController@trendingOrder')->name('trendingOrder');




});
//<----------------My first api---------->
//Route::apiResource('order','Api\OrderController');
//Route::get('/order','Api\OrderController@index');
//Route::get('area','Api\AreaController@index')->name('area');
Route::apiResource('area','Api\AreaController');
Route::apiResource('subService','Api\SubserviceController');
//Route::post('order/delete/{id}','Api\OrerController@destroy');
Route::get('service','Api\ServiceController@index')->name('services');
Route::get('TrendingServices','Api\TrendingServiceController@index')->name('TrendingServices');
//Route::post('trendingOrder','Api\OrderController@trendingOrder')->name('trendingOrder');









