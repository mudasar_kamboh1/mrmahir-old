<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', 'Web\PageController@home');
Route::get('/login', 'Auth\LoginController@index');
Route::get('/register', 'Auth\RegisterController@index');
Route::get('getSubServices/{id}','Auth\RegisterController@getSubServices');
Route::get('admin-login', 'Auth\LoginController@adminLoginView');



Route::get('email','Web\PageController@email');
Route::get('/home', 'Web\PageController@home');
Route::get('/services', 'Web\PageController@services');
Route::get('/services/{slug}', 'Web\PageController@services');
//Route::get('/service/{slug}', 'Web\PageController@serviceDetail');

Route::get('/rate-list', 'Web\PageController@rateList');
Route::get('/about-us', 'Web\PageController@aboutUs');
Route::get('/terms', 'Web\PageController@terms');
Route::get('/portfolio', 'Web\PageController@portfolio');
Route::get('/order-now', 'Web\PageController@orderNow');
Route::get('getSubServices/{id}', 'Web\PageController@getSubServices');
Route::get('getSubServices/{id}', 'Web\UserController@getSubServices');


Route::get('change-status/{id}','Web\OrderController@changeStatus');
Route::get('check-otp-valid', 'OtpController@checkOtpValid')->name('checkOtpValid');
Route::get('resend-otp', 'OtpController@reSendOtp');
Route::get('check-user-password-valid', 'Web\UserController@checkUserPasswordValid')->name('checkUserPasswordValid');
Route::get('forgot-password', 'Web\UserController@forgotPassword')->name('forgotPassword');

Route::post('instance-order', 'Web\OrderController@instanceOrder')->name('instanceOrder');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('admin-login', 'Auth\LoginController@adminLogin')->name('adminLogin');
Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::resource('quote','Web\QuoteController');
ROute::post('quoteSave','Web\QuoteController@store')->name('quoteSave');
Route::get('email/{id}','Web\PageController@email');


Route::prefix('admin')->middleware(['auth','Admin'])->group(function () {
    Route::resource('services','Admin\ServiceController');
    Route::resource('staffs','Admin\StaffController');
    Route::resource('users','Admin\UserController');
    Route::resource('orders','Admin\OrderController');
    Route::resource('areas','Admin\AreaController');
    Route::resource('dashboard','Admin\DashboardController');

    Route::resource('subServices','Admin\SubServiceController');
    Route::resource('trendingService','Admin\TrendingServiceController');
    Route::resource('quotes','Admin\QuoteController');
    Route::resource('Facebook','Admin\FacebookController');
    Route::get('pdfDownload/{id}','Admin\OrderController@pdfDownload')->name('pdfDownload');
    Route::get('get-Sub-Services/{id}','Admin\StaffController@getSubServices');
    Route::get('printView/{id}','Admin\OrderController@printView')->name('printView');
    Route::get('assign/{id}','Admin\OrderController@assign')->name('assign');
    Route::post('assignOrder/{id}','Admin\OrderController@assignOrder')->name('assignOrder');

    Route::resource('invoices','Admin\InvoiceController');
    Route::get('print/{id}','Admin\InvoiceController@printView')->name('print');

    Route::resource('galleries', 'Admin\GalleryController');
    Route::resource('categories', 'Admin\CategoriesController');
    Route::resource('sub_categories', 'Admin\SubCategoryController');
    Route::get('sub_categorie/{id}', 'Admin\SubCategoryController@getCats');

    Route::get('index','Admin\OrderController@index')->name('home');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('order-detail/{id}','Admin\OrderController@orderDetail');
    Route::get('staff-detail/{id}','Admin\StaffController@staffDetail');
    Route::post('change-status','Admin\OrderController@changeStatus');
    Route::post('order-update/{id}','Admin\OrderController@orderUpdate');
    Route::get('invoice-detail/{id}','Admin\OrderController@invoice');
    Route::get('pdfView/{id}','Admin\OrderController@pdfView')->name('pdfView');
    Route::get('printView/{id}','Admin\OrderController@printView')->name('printView');
    Route::get('userChart','Admin\DashboardController@userChart')->name('userChart');




//    Route::get('pdfDownload/{id}','Admin\InvoiceController@pdf')->name('pdfDownload');


});

Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::post('password/email', 'Auth\ResetPasswordController@sendResetLink')->name('password.email');
//Route::get('password/email', 'Auth\ResetPasswordController@sendResetLink')->name('password.email');



Route::middleware(['auth'])->group(function () {
    Route::resource('order','Web\OrderController');
    Route::resource('user','Web\UserController');
    Route::post('update-user-info','Web\UserController@update')->name('setUserPassword');
    Route::post('update-profile','Web\UserController@updateProfile')->name('updateProfile');
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('/profile','Web\UserController@index');
    Route::get('order-detail/{id}','Web\OrderController@orderDetail');
    Route::post('addMobile/{id}','Web\OrderController@addMobile')->name('addMobile');
    Route::post('addNote/{id}','Web\OrderController@addNote')->name('addNote');
    Route::post('addAddress/{id}','Web\OrderController@addAddress')->name('addAddress');
    Route::post('addTimeSlot/{id}','Web\OrderController@addTImeSlot')->name('addTimeSlot');
    Route::delete('deleteImage/{id}','Web\UserController@deleteImage')->name('deleteImage');
    Route::get('staffOrder','Web\UserController@staffOrder')->name('staffOrder');
    Route::get('viewStaff/{id}','Web\UserController@viewStaff')->name('viewStaff');
    Route::get('rating/{id}','Web\OrderController@rating')->name('rating');
    Route::post('ratingSubmit/{id}','Web\OrderController@ratingSubmit')->name('ratingSubmit');
    Route::post('changeStatusOrder/{id}','Web\OrderController@changeStatusOrder')->name('changeStatusOrder');
});




