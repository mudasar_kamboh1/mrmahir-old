<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToOrderStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_staff', function (Blueprint $table) {
            $table->string('mahir_status')->nullable();
            $table->string('staff_status')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_staff', function (Blueprint $table) {
            $table->dropColumn('mahir_status');
            $table->dropColumn('staff_status');
            $table->dropColumn('status');
        });
    }
}
