<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('subservice_id')->nullable();
            $table->unsignedInteger('area_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();
            $table->string('token','10')->nullable();
            $table->string('address')->nullable();
            $table->string('problem_message')->nullable();
            $table->boolean('confirmed')->default(0)->comment('after authentication it will be 1 otherwise 0');
            $table->string('mobile')->nullable();
            $table->dateTime('schedule_datetime')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
