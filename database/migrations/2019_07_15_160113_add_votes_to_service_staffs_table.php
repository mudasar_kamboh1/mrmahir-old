<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVotesToServiceStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_staffs', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->unsignedInteger('sub_service_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_staffs', function (Blueprint $table) {
            $table->unsignedInteger('service_id')->nullable();
            $table->dropColumn('sub_service_id');
        });
    }
}
