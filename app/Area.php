<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
     protected $table ='areas';
     protected $fillable = [
         'name',
     ];
    public function orders()
    {
        return $this->hasMany('App\Order','area_id');
    }
    public function staffs()
    {
        return $this->belongsToMany('App\Staff','area_staff')->withPivot('staff_id','area_id');
    }
    public function service()
    {
        return $this->belongsToMany('App\Service');
    }
}
