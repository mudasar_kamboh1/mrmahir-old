<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SubService extends Model
{
    protected $guarded = [];

    public function service()
    {
        return $this->belongsto('App\Service','service_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order','subservice_id');
    }
    public function TrendingServices()
    {
        return $this->hasMany('App\TrendingService','subService_id');
    }
    public function staffs()
    {
        return $this->belongsToMany('App\Staff','service_staffs','sub_service_id')->withPivot('staff_id','sub_service_id');
    }
}
