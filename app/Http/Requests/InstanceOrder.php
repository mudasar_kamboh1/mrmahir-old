<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstanceOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required|numeric|digits:11',
            'subService' => 'required|exists:sub_services,id',
            'area' => 'required|exists:areas,id',
            'schedule_datetime' =>  'nullable|date_format:"d-m-Y h:iA"',
            'name' =>  'nullable|regex:/^[\pL\s\-]+$/u|min:3',
            'email' =>  'nullable|email',
            'problem_message' =>  'nullable|string',
        ];
    }
}
