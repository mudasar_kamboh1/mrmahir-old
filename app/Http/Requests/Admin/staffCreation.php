<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class staffCreation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>  'required|regex:/^[\pL\s\-]+$/u|min:3',
            'email'=>'required|email',
            'password' => 'required|min:6',
            'mobile' => 'required|unique:users|digits:11',
            'address'=>'required|max:250|string'
        ];
    }
}
