<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class serviceValid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'meta_title' => 'required|string|max:255',
            'icon_alt'=>'required|string|max:25',
            'image_alt'=>'required|string|max:25',
            'image'=>'required|mimes:jpeg,jpg,png,|max:4096',
            'icon'=>'required|mimes:jpeg,jpg,png,|max:4096',
            'description'=>'required|max:400|regex:/^[\pL\s\-]+$/u',
            'meta_description'=>'required|max:500|regex:/^[\pL\s\-]+$/u'

        ];
    }
}
