<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class quoteValid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|min:3|max:50',
            'mobile' => 'required|numeric|digits:11',

        ];
    }
}
