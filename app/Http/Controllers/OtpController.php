<?php

namespace App\Http\Controllers;

use App\Http\Requests\checkOtpValid;
use App\Otp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OtpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkOtpValid(checkOtpValid $request){
        $user = User::where('mobile',$request->mobile)->first();
        if(!is_null($user)){
            $otp = $user->otp()->where('code',$request->code)->orderBy('created_at','desc')->first();
            if(!is_null($otp)){
                $order = $user->orders()->orderBy('created_at','desc')->first();
                $user->update(['mobile_verified_at' => Carbon::now()->format('Y-m-d H:i:s')]);
                // Logged In User
                Auth::login($user);
                // Logged In User
                if(!is_null($order)){
                    $order->update(['confirmed' => true]);
                    return response()->json([
                        'message' => 'Otp successfully verified!',
                        'status' => true
                    ]);
                }
            }else{
                return response()->json([
                    'message' => 'Invalid Otp!',
                    'status' => false
                ]);
            }
        }else{
            return response()->json([
                'message' => 'Invalid Mobile Number!',
                'status' => false
            ]);
        }
    }

    public function reSendOtp(Request $request){
        $this->validate($request, [
           'mobile' =>  'required|exists:users|max:15'
        ]);
        $user = User::where('mobile',$request->mobile)->first();
        if(!is_null($user)){
            $otp = $user->otp()->where('created_at', '>=', Carbon::now()->subMinutes(1)->toDateTimeString())->orderBy('created_at','desc')->first();
            if(is_null($otp)){
                $otp = uniqueOtp(rand(100000, 999999));
                $user->otp()->delete();
                $otp = Otp::create(['code' => $otp]);
                $otp->user()->associate($user)->save();
                return response()->json([
                    'message' => 'OTP successfully sent to your mobile!',
                    'status' => true
                ]);
            }else{
                return response()->json([
                    'message' => 'You can try after one minute to resend!',
                    'status' => false
                ]);
            }
        }else{
            return response()->json([
                'message' => 'Invalid Mobile Number!',
                'status' => false
            ]);
        }
    }
}
