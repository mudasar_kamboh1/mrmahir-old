<?php

namespace App\Http\Controllers\Auth;

use App\Staff;
use App\SubService;
use App\User;
use App\Area;
use App\Service;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Zend\Diactoros\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '\home';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(){
        $areas = Area::get();
        $services = Service::with('subServices')->get();
        $sub_services =SubService::get();
//        $services = Service::get();
        return view('web.layouts.register',compact('areas','services','sub_services'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required||regex:/^[\pL\s\-]+$/u|min:3|max:50',
            'email' => 'required|string|email|max:150',
            'mobile' => 'required|numeric|unique:users|digits:11',
            'address'=> 'nullable|max:250',
            'password' => 'required|string|min:6|max:16|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'mobile'=>$data['mobile'],
            'password'=>$data['password'],
//            'address'=> isset($data['address']) ? $data['address'] : '',
        ]);


        if(isset($data['is_staff']) && $data['is_staff'] == 1){
            $staff = Staff::create([]);
//            dd($staff);
            $staff->user()->associate($user)->save();
            $staff->areas()->attach($data['areas']);
            $staff->subServices()->attach($data['sub_service_id']);
//            $staff->services()->attach($data['services']);
            $user->assignRole('Staff');

//            return redirect('/profile');


        }else{
            $user->assignRole('User');

        }
        return $user;
    }
    public function getSubServices($id){
        $service = Service::find($id);
        if(!is_null($service)){
            $subServices = $service->subServices()->select('id','title')->get();
            return response()->json([
                'status' => true,
                'msg' => 'Service Successfully Retrieved!',
                'data' => $subServices
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Service Not Found!'
            ]);
        }
    }


}
