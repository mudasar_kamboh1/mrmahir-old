<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Otp;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
//
//    protected function authenticated(Request $request, $user) {
//        if ($user->role_id == 2) {
//            return redirect('/home');
//        } else if ($user->role_id == 3) {
//            return redirect('/profile');
//        } else {
//            return redirect('/homr');
//        }
//    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){

        return view('web.layouts.login');
    }

    public function adminLoginView(){
        return view('auth.login');
    }

    public function login(Request $request)
    {
        // Check validation
        $this->validate($request, [
            'password' => 'required|min:6',
            'mobile' => 'required|numeric|exists:users',
        ]);
        $user = User::whereMobile($request->get('mobile'))->first();
        if(!is_null($user)){
            if (Auth::attempt(['mobile'=> $request->get('mobile'),'password' => $request->get('password')])){
                return response()->json([
                    'status' => true,
                    'message' => 'Successfully Logged In!',
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Something invalid Mobile or Password',
                ]);
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Your Mobile Invalid!',
            ]);
        }


    }


    public function adminLogin(Request $request)
    {
        // Check validation
        $this->validate($request, [
            'password' => 'required|min:6',
            'email' => 'required|email|exists:users',
        ]);


        $user = User::whereEmail($request->get('email'))->first();
        if(!is_null($user) && $user->hasRole('Admin')){
            if (Auth::attempt(['email'=> request('email'),'password'=> request('password')])){
                toastr()->success('Successfully Logged In!');
                return redirect('admin/dashboard');
            }else{
                toastr()->error('Invalid email or password');
                return redirect('admin-login');
            }
        }else{
            toastr()->error('You do not have permission!');
            return redirect('admin-login');
        }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/home');
    }

}
