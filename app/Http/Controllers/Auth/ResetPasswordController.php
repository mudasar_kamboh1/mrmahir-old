<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\Email;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Jobs\SendEmailJob;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function sendResetLink(Request $request)
    {
        $this->validate($request, [
//            'password' => 'required|min:6',
            'email' => 'required|email|exists:users',
        ]);
        $user = User::whereEmail($request->get('email'))->first();
        if (!is_null($user)) {
            $details['email'] = 'your_email@gmail.com';
            SendEmailJob::dispatch($user);
//            Mail::send('emails.test', $request->get('email'), function ($message) {
//                $message->subject('Maaju');
//            });
            return  redirect()->back();
        }

    }

}
