<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Order;
use App\Repositories\OrderRepository;
class DashboardOldController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository){
        $this->orderRepository = $orderRepository;
    }

    public function index( )
    {
        $order =  $this->orderRepository->paginate('9');
        return view('dashboard.home.index',compact('order'));
    }public function versionone()
{
    dd('sa');
    return view('dashboard.v1');
}

    public function versiontwo()
    {
        return view('dashboard.v2');
    }
    public function versionthree()
    {
        return view('dashboard.v3');
    }
}
