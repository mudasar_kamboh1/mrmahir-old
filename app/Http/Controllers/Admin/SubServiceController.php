<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\subServiceValid;
use App\Service;
use Illuminate\Http\Request;
use App\Repositories\SubserviceRepository;
use App\Http\Controllers\Controller;
use App\SubService;
use Image;
//use Intervention\Image\Image;
class SubServiceController extends Controller
{
    protected $subserviceRepository;
     public function __construct( SubserviceRepository $subserviceRepository)
     {
         $this->subserviceRepository = $subserviceRepository;
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->get('search')){
            $subServices = SubService::where('title','like','%'.$request->get('search').'%')->paginate(6);
        }else{
            $subServices = $this->subserviceRepository->paginate('10');
        }
        $services = Service::all();
    return view('dashboard.subservice.index',compact('subServices','services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(subServiceValid $request)
    {
       $service = Service::find($request->get('service_id'));
         $input = $request->all();
        //         image resize section
         if($image = $request->file('image')){
             $input['image'] = time().'.'.$image->getClientOriginalExtension();
             $destinationPath = public_path('/thumbnailImg');
             $img = Image::make($image->getRealPath());
             $img->resize(100, 100, function ($constraint) {
                 $constraint->aspectRatio();
             })->save($destinationPath.'/'.$input['image']);
             $destinationPath = public_path('/subServiceImg');
             $image->move($destinationPath, $input['image']);

         }

//         if ($originalImage= $request->file('image')){
//             $thumbnailImage = Image::make($originalImage);
//
//             $thumbnailPath = public_path().'/thumbnailImg/';
//             $originalPath = public_path().'/subServiceImg/';
//             $thumbnailImage->resize(150,150);
////             $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName());
//             $input['image'] = imageUpload($request->file('image'),$originalPath,$thumbnailPath);
//
//         }

       $subService = SubService::create($input);
//        dd($subService);
        $subService->service()->associate($service)->save();
        if($subService){
             toastr()->success('SubService Added Successfully!');
       }else{
           toastr()->error('Something went wrong!');
       }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subService =$this->subserviceRepository->find($id);
        if ($subService)
        {
            toastr()->info('Edit SubService');
        }else{
            toastr()->error('Something went wrong!');
        }
        return view('dashboard.subservice.edit',compact('subService'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd('s');
        $input = $request->all();
        $service =SubService::find($id);

        if($image = $request->file('image')){
            $input['image'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/thumbnailImg');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['image']);
            $destinationPath = public_path('/subServiceImg');
            $image->move($destinationPath, $input['image']);
        }

        $update = $this->subserviceRepository->update($input,$id);
        if ($update){
            toastr()->success(' SubServices Updated Successfully!');
        }else{
            toastr()->error('Something went wrong!');
        }
        return redirect('admin/subServices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subService = SubService::findorfail($id);
        if ($subService) {
//            $service = Service::findorFail($id);
            $images = [
                $image_path = public_path() . '/subServiceImg/' . $subService->image,
                $icon_path = public_path() . '/thumbnailImg/' . $subService->image,
            ];
            foreach ($images as $image) {
                if (file_exists($image)) {
                    unlink($image);
                }
            }
//            dd($subService);
            $subService->delete();
            if ($subService) {
                toastr()->warning('SubService Deleted Successfully!');
            } else {
                toastr()->error('Something went wrong!');
            }
            return redirect()->back();
        }
   }
}
