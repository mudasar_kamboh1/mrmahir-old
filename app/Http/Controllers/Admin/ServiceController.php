<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\serviceValid;
use App\Service;
use Illuminate\Http\Request;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Testing\File;

class ServiceController extends Controller
{
    protected $serviceRepository;
    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->middleware('auth');
        $this->serviceRepository= $serviceRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if ($request->get('search')){
            $services = Service::where('title','like','%'.$request->get('search').'%')->paginate(6);
        }else{
            $services = $this->serviceRepository->paginate('10');
        }
        return view('dashboard.Service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(serviceValid $request)
    {
        $service = $this->serviceRepository->storeService($request);
        if($service){
            toastr()->success('Service Successfully added');
            return redirect()->back();
        }else{
            toastr()->error('Something went wrong!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = $this->serviceRepository->find($id);
        if ($service){
            toastr()->info('Edit Service');
        }
         return view('dashboard.Service.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
   $this->validate($request,[
        'title'=>'nullable|string',
       'icon_alt'=>'nullable|string|max:25',
       'image_alt'=>'nullable|string|max:25',
       'image'=>'nullable|mimes:jpeg,jpg,png, |max:4096',
       'icon'=>'nullable|mimes:jpeg,jpg,png,|max:4096'
          ]);
        $input = $request->all();
        $service =Service::find($id);
        if ($request->hasFile('image')) {
                if ($request->hasFile('image')) {
                    $usersImage = public_path("serviceImg/{$service->image}"); // get previous image from folder
                    if (file_exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                }
            $input['image'] = imageUpload($request->file('image'), public_path('/serviceImg'));
        }
        if ($request->hasFile('icon')){
            if ($request->hasFile('icon')) {
                $usersImage = public_path("serviceIcon/{$service->icon}"); // get previous image from folder
                if (file_exists($usersImage)) { // unlink or remove previous image from folder
                    unlink($usersImage);
                }
            }
            $input ['icon'] = imageUpload($request->file('icon'), public_path('/serviceIcon'));
        }
        $update = $this->serviceRepository->update($input,$id);
        if($update){
            toastr()->success('Service Updated Successfully');
        }else{
            toastr()->error('Something went wrong');
        }
        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $service = $this->serviceRepository->destroyService($id);
            if ($service){
            toastr()->success('Service Deleted Successfully!');
            return redirect()->back();
        }else{
            toastr()->error('Something Went Wrong');
        }
        return redirect()->back();

    }
}
