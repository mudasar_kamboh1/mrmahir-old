<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\userCreation;
use App\Staff;
use App\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $userRepository;

    public function  __construct(UserRepository $userRepository)
    {
//        $this->middleware('auth');
        $this->userRepository = $userRepository;

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::whereHas('roles',function ($query){
            $query->where('name','User')
            ;
        });
        $search = $request->get('search');
        if($search){
            $users = $users->where('name','like','%'.$search.'%')
                ->orWhere('mobile', 'like', '%' . $search . '%')
                ->orWhere('address', 'like', '%' . $search . '%');
        }
        $users = $users->paginate('10');
        return view('dashboard.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(userCreation $request)
    {
        $input=$request->all();
        if($request->get('password')){
            $input['password'] = $request->get('password');
            $input['password'] =  $request->get('password');
            $input['password_hint'] = encrypt( $request->get('password'));
        }
//        if($request->password){
//            $input['password'] = $request->password;
//        }
        $user = $this->userRepository->create($input);
        $user->assignRole('User');
//        dd($user->assignRole('User'));

        if ($user){
            toastr()->success('User successfully added');
        }else{
            toastr()->error('Something went wrong');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        if ($user){
            toastr()->info('Edit User! ');
        }else{
            toastr()->error('Something went wrong!');
        }
        return view('dashboard.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' =>  'nullable|regex:/^[\pL\s\-]+$/u|min:3',
            'email' =>  'nullable|email',
            'address' => 'nullable|string|max:250',
            'mobile' => 'nullable|numeric|digits:11',
        ]);

         $input = $request->all();
         $user = User::findorfail($id);
        if($request->get('password')){
            $input['password'] = $request->get('password');
            $input['password'] =  $request->get('password');
            $input['password_hint'] = encrypt( $request->get('password'));
        }

         $update = $user->update($input);
         if ($update)
         {
             toastr()->success('User updated successfully!');

         }else{
             toastr()->error('Something went wrong');
         }
         return redirect('admin/users');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (!is_null($user)){
            if(!$user->hasRole('Admin')){
                $user->delete();
                toastr()->success('User Deleted Successfully!');
            }else{
                toastr()->error('You can not delete Admin user!');
            }
        }else{
            toastr()->error('Something went wrong!');
        }
        return redirect()->back();

    }

}
