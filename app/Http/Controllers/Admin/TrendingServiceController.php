<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service;
use App\SubService;
use App\TrendingService;
use Illuminate\Http\Request;

class TrendingServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::get();
        $sub_services = SubService::get();
        $trendingServices = TrendingService::with('SubServices')->paginate(10);
      return view('dashboard.trendingService.index',compact('sub_services','services','trendingServices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $subService = SubService::find($request->get('subService_id'));
        //not save double values
        $same = TrendingService::where('subService_id', $request->subService_id);
        $same =$same->get();
        if(count($same) > 0){
            toastr()->info('TrendingServices Already Exist!');
            return back();
        }
        $service = Service::find($request->get('service_id'));
        $trendingService = TrendingService::create($request->except('service_id'));
        $trendingService->SubServices()->associate($subService)->save();
        if($trendingService){
            toastr()->success('TrendingServices Added Successfully!');
        }else{

            toastr()->error('Something went wrong!');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrendingService  $trendingService
     * @return \Illuminate\Http\Response
     */
    public function show(TrendingService $trendingService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrendingService  $trendingService
     * @return \Illuminate\Http\Response
     */
    public function edit(TrendingService $trendingService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrendingService  $trendingService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrendingService $trendingService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrendingService  $trendingService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trendingService = TrendingService::findorfail($id);
        $trendingService->delete();
        if ($trendingService){
            toastr()->warning('TrendingService Deleted Successfully!');
        }else{
            toastr()->error('Something went wrong!');
        }
        return redirect()->back();
    }
}
