<?php

namespace App\Http\Controllers\Admin;

use App\Facebook;
use App\Order;
use App\Staff;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =User::all();
        $user1 = User::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) );
        $user_today = User::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 days')) );
        $user1 = User::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) );
        $staff_week = Staff::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')));
        $staff_today = Staff::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 days')) );
        $staff = Staff::all();
        $order_week = Order::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) );
        $order_today = Order::all()->where('created_at', '>=', date('Y-m-d H:i:s',strtotime('-1 days')) );
        $order = Order::all();
        return view('dashboard.MainDashboard.v1',compact('user','user_today','order','user1','order_week','order_today','staff_week','staff_today','staff'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
    public function userChart(Request $request, $id = null)
    {
        $staff = Staff::selectRaw('year(created_at) year, monthname(created_at) month, count(*) data')
            ->groupBy('year', 'month')
            ->orderBy('year', 'asc')
            ->get();

        if(count($staff) > 0){
            foreach ($staff as $k => $result) {
                $staff1[$k]['value']  = (int)$result->data;
                $staff1[$k]['month'] = $result->month;
            }

        }else{
            $staff1 = [];
        }

        $staff = $staff1;
//        dd($staff);
        $social_users= User::selectRaw('year(created_at) year, monthname(created_at) month, count(*) data')
            ->groupBy('year', 'month')
            ->orderBy('year', 'asc')
            ->get();
//        dd($social_users);
        if(count($social_users) > 0){
            foreach ($social_users as $k => $result) {
                $social_users1[$k]['value']  = (int)$result->data;
                $social_users1[$k]['month'] = $result->month;
            }

        }else{
            $social_users1 = [];
        }

        $social_users = $social_users1;
//        dd( $social_users1[$k]['month']);
        $order = Order::selectRaw('year(created_at) year, monthname(created_at) month, count(*) data')
            ->groupBy('year', 'month')
            ->orderBy('year', 'asc')
            ->get();

        if(count($order) > 0){
            foreach ($order as $k => $result) {
                $order1[$k]['value']  = (int)$result->data;
                $order1[$k]['month'] = $result->month;
            }

        }else{
            $order1 = [];
        }

        $order = $order1;
//        facebook data chart
        $facebook = Facebook::selectRaw('year(created_at) year, monthname(created_at) month, count(*) data')
            ->groupBy('year', 'month')
            ->orderBy('year', 'asc')
            ->get();
        if(count($facebook) > 0){
            foreach ($facebook as $k => $result) {
                $facebook1[$k]['value']  = (int)$result->data;
                $facebook1[$k]['month'] = $result->month;
            }

        }else{
            $facebook1 = [];
        }
        $facebook = $facebook1;

//       $status= $order_status ->whereStatus('pending')->first();
//        dd($status->status->toArray());
        $status =Order::where('status_id',3)->count();
//        dd($status );
        if($status){
            $order_price = Order::selectRaw('year(created_at) year, monthname(created_at) month, sum(order_price) as sum')
                ->groupBy('year', 'month')
                ->orderBy('year', 'asc')
                ->get();
//           dd($order_price);
            if(count($order_price) > 0){
                foreach ($order_price as $k => $result) {
                    $order_price1[$k]['value']  = (int)$result->sum;
                    $order_price1[$k]['month'] = $result->month;
                }

            }else{
                $order_price1 = [];
            }

            $order_price = $order_price1;
        }else{
            return back();
        }

        return view('dashboard.MainDashboard.user-chart',compact('social_users','staff','order','facebook','order_price'))
           ;

    }


}
