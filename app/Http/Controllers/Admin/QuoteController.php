<?php

namespace App\Http\Controllers\Admin;

use App\QuoteModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $search =$request->get('search');
        if ($search){
            $quotes= QuoteModel::where('name','like','%'.$search.'%')->
                orWhere('mobile','like','%'.$search.'%')->paginate(5);
        }else{
        $quotes =QuoteModel::orderBy('id','desc')-> paginate('10');
    }
        return view('dashboard.quote.index',compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $quote = QuoteModel::find($id)->delete($id);
         if ($quote){
             toastr()->success('Quote Deleted Successfully!');
         }else{
             toastr()->error('ajax call not working');

         }
         return back();
//        return response()->json([
//            'success' => 'Record deleted successfully!'
//        ]);
    }
}
