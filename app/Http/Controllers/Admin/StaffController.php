<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\staffCreation;
use App\Service;
use App\Staff;
use App\Status;
use App\SubService;
use App\User;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Repositories\StaffRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use App\Repositories\UserRepository;
use function Sodium\compare;

//use Session;


class StaffController extends Controller
{
    protected $staffRepository;


    public function __construct(StaffRepository $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }
    protected $userRepository;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::whereHas('staff');

        $search =$request->get('search');
        if($search){
            $staffs = $users->where('name','like','%'.$search.'%');
        }
        $users = $users->paginate('10');
        $areas = Area::orderBy('name')->get();
        $services = Service::with('subServices')->get();
//        $subService= SubService::get();
        return view('dashboard.staff.index',compact('users','areas','services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(staffCreation $request)
    {
   //    accociating data save staff table = user tables
        $user = User::create($request->except('areas','services','subservice'));
        $user->assignRole('Staff');
        $staff = Staff::create([]);
        $staff->user()->associate($user)->save();
        $staff->areas()->attach($request->get('areas'));
        $staff->subServices()->attach($request->get('subservice'));
        if ($staff){
            toastr()->success('Staff  save Successfully ');
        }else{
            toastr()->error('Something went wrong!');
        }
        return redirect()->back();

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(Staff $staff)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $staff = $user->staff;
        $areas = Area::get();
        $services = Service::with('subServices')->get();
        $sub_services= SubService::get();
        if ($user){
            toastr()->info('Edited Staff');
        }else{
            toastr()->error('Something went wrong!');
        }
        return view('dashboard.staff.edit',compact('user','areas','services','sub_services','staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' =>  'nullable|regex:/^[\pL\s\-]+$/u|min:3',
            'email' =>  'nullable|email',
            'address' => 'nullable|string|max:250',
            'mobile' => 'nullable|numeric|digits:11',
            'card_id'=>'nullable|string|max:15',
            'expire_date'=>'nullable|date_format:"m/d/Y"',
        ]);

        $user = User::findorfail($id);
        $staff= $user->staff;
        if (!is_null($staff)){
            $userInfo= $request->only('card_id');
            if($request->expire_date){
                $userInfo['expire_date'] = Carbon::parse($request->get('expire_date'))->format('Y-m-d');
            }
            $images = [];
        if ( $files = $request->file('images')) {
            if($files = $request->file('images')){
                foreach($files as $k => $file){
                    $name = time() .$k. '.' . $file->getClientOriginalExtension();
                    $file->move('assets/staff',$name);
                    $images[$k] = $name;
                }
            }
            $userInfo['images'] =implode(',',$images);
        }

            $update =$staff->update($userInfo);
        }
            $input = $request->only('name','email','mobile','address');
//        dd($input);
        $staff->areas()->sync($request->get('areas'));
        $staff->subServices()->sync($request->get('sub_services'));
        $update = $user->update($input);
//        dd($update);
        if ($update){
            toastr()->success('Staff Updated Successfully ');
        }else{
            toastr()->error('Something went wrong!');
        }
        return redirect('admin/staffs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $staff = $user->staff;
        if (!is_null($user)){
            if($user->hasRole('Staff')){

                $staff->areas()->detach();
                $staff->subServices()->detach();
                $user->staff()->delete();
                toastr()->success('Staff Deleted Successfully!');
            }else{
                toastr()->error('You can not delete Admin user!');
            }
        }else{
            toastr()->error('Something went wrong!');
        }
        return redirect()->back();

    }
    public function getSubServices($id){

        $service = Service::find($id);
        if(!is_null($service)){
            $subServices = $service->subServices()->select('id','title')->get();
//            dd($subServices);
            return response()->json([
                'status' => true,
                'msg' => 'Service Successfully Retrieved!',
                'data' => $subServices
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Service Not Found!'
            ]);
        }
    }
    public function staffDetail($id)
    {
//        dd($id);
        $user = User::find($id);
//        dd($user);
        $staff = $user->staff;
        $areas = Area::get();
        $services = Service::with('subServices')->get();
        $sub_services= SubService::get();
        if(!is_null($user)){
            return view('dashboard.staff.staff-detail', compact('user', 'staff','sub_services','services','areas'));
        }else{
            return '';
        }
    }

}
