<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\Http\Controllers\Controller;
use App\Order;
use App\Service;
use App\Staff;
use App\Status;
use App\SubService;
use App\User;

use Carbon\CarbonInterval;
use Faker\Provider\DateTime;
use function foo\func;
use PDF;
use Carbon\Carbon;
//use function foo\func;
use Illuminate\Http\Request;
use App\Repositories\OrderRepository;


class OrderController extends Controller
{

    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $orders =  Order::where('confirmed', 1);
        if ($search) {
            $orders = $orders->whereHas('user',function ($query) use ($search){
                $query->where('name', 'like', '%' . $search . '%')
                        ->orWhere('mobile', 'like', '%' . $search . '%')
                        ->orWhere('address', 'like', '%' . $search . '%');
                })->orWhere('token','like',$search)
                ->orWhere('mobile', 'like', '%' . $search . '%')
                ->orWhere('address', 'like', '%' . $search . '%')
                ->orderBy('created_at','desc')->get();
        }else{
            $orders = $orders->orderBy('created_at','desc')->paginate('10');
        }


        $statuses = Status::all();
        return view('dashboard/order/index',compact('orders','statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request ,$id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->delete($id);
        if ($order) {
            toastr()->success('successfully deleted order');
        }else{
            toastr()->error('Something Went Wrong!');
        }
        return back();
    }
    public function changeStatus(Request $request)
    {

        $status = Status::find($request->status);
        $order = Order::find($request->order);
        $order->status()->associate($status)->save();
        toastr()->success('Status successfully changed!');
        return back();
    }

    public function orderDetail($id)
    {
        $statuses = Status::all();
        $order = $this->orderRepository->find($id);
        $services = Service::get();
        $sub_services = SubService::get();
        $areas = Area::get();
        if(!is_null($order)){
            return view('dashboard.order.order-detail-popup', compact('order', 'statuses','sub_services','services','areas'));
        }else{
            return '';
        }
    }
    public function orderUpdate(Request $request,$id)
    {
        $this->validate($request, [
            'mobile' => 'nullable|numeric|min:11',
            'address'=>'nullable|string|max:250',
            'problem_message'=>'nullable|string|max:500',
            'schedule_datetime'=>'nullable|date_format:"d-m-Y h:iA"',
            'created_at'=>'nullable|date_format:"d-m-Y h:iA"',
            'order_price'=>'nullable|numeric',
        ]);
        $order = Order::find($request->get('order'));
//        dd($order);
        if(is_null($order)){
            toastr()->error('Order Not Found!');
            return back();
        }
        $subService = SubService::find($request->get('subservice_id'));
//        if ($request->get('price')&& !is_null($order->subService)){
//            $price["price"] =$request->get("price");
//            $order->subService()->update($price);
//        }
        //service asscoiate subservice
        $order->subService()->associate($subService)->save();
        //area asscoiate order
        $area = Area::find($request->get('area_id'));
        $order->area()->associate($area)->save();
         //status change
        $status = Status::find($request->get('status'));
        $order->status()->associate($status)->save();
        $input = $request->only('mobile','address','problem_message','order_price');
//        timeformate
        $input['schedule_datetime'] = Carbon::parse($request->get('schedule_datetime'))->format('Y-m-d H:i:s');
        $input['created_at'] = Carbon::parse($request->get('created_at'))->format('Y-m-d H:i:s');
//       dd($input['created_at']);
        if($request->get('password') && !is_null($order->user)){
            $userInfo['password_hint'] = encrypt($request->get('password'));
            $userInfo['password'] = $request->get('password');
            $userInfo['address'] = $request->get('address2');
            $order->user()->update($userInfo);
        }
        $update = $order->update($input);
        if($update){
            toastr()->success('Order successfully updated!!');
            return back();
        }else{
            toastr()->error('Something went wrong!');
            return back();
        }

    }
    public function invoice($id)
    {
        $order = $this->orderRepository->find($id);

        if (!is_null($order)){
            return view('dashboard.order.invoice', compact('order','services','sub_services','cartTotal'))->render();
        }else{
            return 'Invoice Not Found!';
        }

    }
    public function pdfDownload($id){

        $order =$this->orderRepository->find($id);
        $pdf = PDF::loadView('dashboard.order.pdf', compact('order'));
        return $pdf->download('MrMahir.pdf');

    }

    public function printView($id){
        $order =$this->orderRepository->find($id);
        return view('dashboard.order.invoice',compact('order'));

    }
    public function pdfView($id){
        $order = $this->orderRepository->find($id);
        return view('dashboard.order.pdf',compact('order'));
    }
    public function assign($id){
        $order =$this->orderRepository->find($id);
//        dd($order);
//          $staffs = Staff::with('user')->get();
        $staffs = Staff::whereHas('areas' , function ($q) use ($order){
            $q->where('area_id', $order->area->id);
        })->whereHas('subServices' , function ($q) use ($order){
            $q->where('sub_service_id', $order->subService->id);
        })->with(['areas','subServices'])->get();
//        dd($staffs);

//        $staffs = Staff::with('user')->get();
        if(!is_null($order)){
            return view('dashboard.order.assign', compact('order', 'staffs'));
        }else{
            return '';
        }
    }
    public  function assignOrder(Request $request,$id)
    {
        $this->validate($request, [
            'status' => 'nullable|string|',
            'staff_status' => 'nullable|string|max:250',
            'mahir_status' => 'nullable|string|max:500',
            'assign_date' => 'nullable|date_format:"d-m-Y h:iA"',
            'done_date' => 'nullable|date_format:"d-m-Y h:iA"',
            'staff_price' => 'nullable|numeric',
            'mahir_price' => 'nullable|numeric',
        ]);
        $order = Order::find($id);
        if (is_null($order)) {
            toastr()->error('Order Not Found!');
            return back();
        }
        $staff = Staff::find($request->get('staff_id'));

//        dd($staff->toArray());
        $status = $staff->orders()->where('status', '=', 'pending')->get();

        foreach ($status as $order){
            if ($order->pivot->status == 'pending') {
//          dd($order->pivot->status == 'pending');
                $orderAssignTime = Carbon::parse($request->get('assign_date'))->format('Y-m-d H:i:s');  // End time
                $orderAssignEndTime = Carbon::parse($orderAssignTime)->addHour($request->duration)->format('Y-m-d H:i:s');  // your start time

                $orderStartTime = Carbon::parse($order->schedule_datetime)->format('Y-m-d H:i:s');  // your start time
                $orderEndTime = Carbon::parse($orderStartTime)->addHour($order->pivot->duration)->format('Y-m-d H:i:s');  // your start time

//              dd($orderStartTime,$orderEndTime,$orderAssignTime,$orderAssignEndTime);
                if($orderAssignTime < $orderStartTime || $orderAssignEndTime < $orderStartTime){

                    toastr()->success('assign order done');
                }
                if($orderAssignEndTime > $orderEndTime){
//                    dd(3);
                    toastr()->success('assign order done');
                }else{
                    dd(1);

                    toastr()->warning('slot is not free');
                }

            }else{
                dd(2);
                toastr()->warning('slot is not free');
            }
//            return back();

        }



        if(is_null($staff)){
            toastr()->error('Staff Not Found!');
            return back();
        }
        $input =$request->only('order_price');
        $update = $order->update($input);
        $mahir_price =$request->only('mahir_price','staff_price','mahir_status','staff_status','status','duration');
        $mahir_price['assign_date'] = Carbon::parse($request->get('assign_date'))->format('Y-m-d H:i:s');
        $mahir_price['done_date'] = Carbon::parse($request->get('done_date'))->format('Y-m-d H:i:s');
        $order->staffs()->attach($request->get('staff_id'),$mahir_price);
        return redirect()->back();
    }




}
