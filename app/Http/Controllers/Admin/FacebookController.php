<?php

namespace App\Http\Controllers\Admin;

use App\Facebook;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FacebookRepository;

class FacebookController extends Controller
{
    protected $facebookRepository;
    public function __construct(FacebookRepository $facebookRepository)
    {
        $this->middleware('auth');
        $this->facebookRepository= $facebookRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Facebook = $this->facebookRepository->get();
        return view('dashboard.facebook.index',compact('Facebook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = $this->facebookRepository->storeService($request);
        if($service){
            toastr()->success('Facebook Review Successfully added');
            return redirect()->back();
        }else{
            toastr()->error('Something went wrong!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Facebook  $facebook
     * @return \Illuminate\Http\Response
     */
    public function show(Facebook $facebook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Facebook  $facebook
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facebook = $this->facebookRepository->find($id);
        if ($facebook){
            toastr()->info('Facebook review Edited');
        }
        return view('dashboard.facebook.edit',compact('facebook'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facebook  $facebook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'nullable|string',
//            'image'=>'nullable|mimes:jpeg,jpg,png, |max:4096',
//            'icon'=>'nullable|mimes:jpeg,jpg,png,|max:4096'
        ]);
        $input = $request->all();
        $facebook =Facebook::find($id);
        if ($request->hasFile('image')) {
            if ($request->hasFile('image')) {
                $usersImage = public_path("fbImages/{$facebook->image}"); // get previous image from folder
                if (file_exists($usersImage)) { // unlink or remove previous image from folder
                    unlink($usersImage);
                }
            }
            $input['image'] = imageUpload($request->file('image'), public_path('/fbImages'));
        }

        $update = $this->facebookRepository->update($input,$id);
        if($update){
            toastr()->success('Facebook Reviews Updated Successfully');
        }else{
            toastr()->error('Something went wrong');
        }
        return redirect('admin/Facebook');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facebook  $facebook
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = $this->facebookRepository->destroyService($id);
        if ($service){
            toastr()->success('facebookReview Deleted Successfully!');
            return redirect()->back();
        }else{
            toastr()->error('Something Went Wrong');
        }
        return redirect()->back();

    }
}
