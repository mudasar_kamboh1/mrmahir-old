<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\areaValid;

use Illuminate\Cache\Repository;
use Illuminate\Http\Request;
use App\Repositories\AreaRepository;
use Mockery\Exception;

class AreaController extends Controller
{
    protected $areaRepository;

    public function __construct(AreaRepository $areaRepository)
    {
        $this->middleware('auth');
        $this->areaRepository = $areaRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->get('search')){

            $area= Area::where('name','like','%'.$request->get('search').'%')->paginate(5);

        }else{
            $area =$this->areaRepository->paginate('9');
        }
        return view('dashboard.Area.index',compact('area'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(areaValid $request)
    {
        $area = $this->areaRepository->create($request->only('name'));
        if ($area){
            toastr()->success('Area  Added Successfully! ');

        }else{
            toastr()->error('Something went Wrong');
        }
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $area = $this->areaRepository->find($id);
        if ($area){
            toastr()->info('Edit your Area!');

        }else{
            toastr()->warning('Somthinf Went Wrong!');
        }
       return view('dashboard.Area.edit',compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
         $this->validate($request,[
         'name'=>'nullable|string'
          ]);
        $input = $request->all();
        $update = $this->areaRepository->update($input,$id);
        if ($update){
            toastr()->success('Area Updated Successfully ');
        }else{
            toastr()->error('Something Went Wrong');
        }
        return redirect('admin/areas');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $areas = $this->areaRepository->delete($id);
        if ($areas){
            toastr()->success('Area Deleted Successfully!');
        }else{
            toastr()->error('Something Went Wrong');
        }
        return redirect()->back();
    }
}
