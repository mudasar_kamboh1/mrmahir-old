<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\InstanceOrder;
use App\Repositories\OrderRepository;
use App\Staff;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Status;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class
OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $orders = $user->orders()->where('confirmed', 1)->orderBy('created_at','desc')->paginate(5);
        $statuses = Status::all();
        return view('web.layouts.order', compact('orders','statuses','user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('order');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('order');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $order = $user->orders()->whereId($id)->first();
        $order = $order->delete();
        if($order){
            toastr()->success('Order Deleted successfully!');
        }else{
            toastr()->warning('Something went wrong!');
        }

        return back();
    }

    public  function orderDetail($id)
    {
        $user = Auth::user();
        $order = $user->orders()->whereId($id)->first();

        if (!is_null($order)){
            return view('web.layouts.order-detail',compact('order','user'));
        }else{
            toastr()->warning('Order Not Found!');
            return redirect('order');
        }
    }

    public function instanceOrder(InstanceOrder $request){
        $response = $this->orderRepository->saveInstanceOrder($request);
        if($response){
            return response()->json([
                'status' => true,
                'message' => 'Our team will call you soon!',
                'redirect' => Auth::check() ? true : false,
            ]);
        }else {
            return response()->json([
                'status' => false,
                'message' => 'Something went wrong. please refresh and try again!',
                'redirect' => Auth::check() ? true : false,
            ]);
        }

    }
    public function changeStatus($id)
    {
        $user = Auth::user();
        $status = Status::whereTitle('Cancel')->first();
        $order = $user->orders()->whereId($id)->first();
        if(!is_null($status) &&  !is_null($order) && $order->status->title == 'Pending'){
            toastr()->success('Order cancelled successfully');
            $order->status()->associate($status)->save();
        }else{
            toastr()->error('Order Status can not be changed!');
        }
        return back();
    }

    public function addMobile( Request $request ,$id)
    {
        $this->validate($request, [
            'mobile' => 'required|numeric|digits:11',
        ]);
       $user = Auth::user();
       $order = $user->orders()->whereId($id)->update($request->only('mobile'));

       if ($order){
           toastr()->success('Additional number added');
       }else{
           toastr()->error('Order Not Found!');
       }
        return back();
    }

    public function addNote( Request $request ,$id)
   {
       $this->validate($request, [
           'problem_message' => 'required|max:500',
       ]);

       $user = Auth::user();
       $order = $user->orders()->whereId($id)->update($request->only('problem_message'));
       if ($order){
           toastr()->success('Problem Message Successfully Updated!');
       }else{
           toastr()->error('Order Not Found!');
       }
       return back();

   }
   public function addAddress(Request $request, $id)
   {
       $this->validate($request, [
           'address' => 'required|max:250',
       ]);
        $user = Auth::user();
        $order = $user->orders()->whereId($id)->update($request->only('address'));
        if ($order){
            toastr()->success('Address added Successfully updated!');
        }else{
            toastr()->error('Address Not Found!');
        }
        return back();
   }


   public function addTimeSlot(Request $request ,$id)
   {
       $this->validate($request, [
           'schedule_datetime' =>  'required|date_format:"d-m-Y h:iA"',
       ]);
        $user = Auth::user();
        $timeSlot = Carbon::parse($request->get('schedule_datetime'))->format('Y-m-d H:i:s');
        $order = $user->orders()->whereId($id)->update(['schedule_datetime' => $timeSlot]);
        if ($order){
            toastr()->success('Time and slot Added Successfully updated! ');
        }else{
            toastr()->error('Time and slot not Found!');
        }
        return back();
   }
   public function rating(Request $request,$id){
//        dd($id);
        $order =$this->orderRepository->find($id);

//       $staff= Staff::find($request->get('staff_id'));;
       $staffs = Staff::with('orders')->get();
//       dd($staffs);
       if(!is_null($order)){
           return view('web.layouts.rating', compact('order','staffs','staff'));
       }else{
           return '';
       }
   }
   public function ratingSubmit(Request $request,$id){
//       dd('rating');
       $order = Order::find($request->order);
       $order = Order::findorfail($id);
//dd($order);
       if(is_null($order)){
           toastr()->error('Order Not Found!');
           return back();
       }
//       $staff =$order->staffs;
//      dd($staff);
//dd($request->get('staff_id'));
       $staff = Staff::find($request->staff_id);
       dd($staff);
//        $staffs =$staff->orders;

       if(is_null($staff)){
           toastr()->error('Staff Not Found!');
           return back();
       }
       $mahir_price =$request->only('rating');
//       dd($order->staffs()->updateExistingPivot($staff,$mahir_price));
       $order->staffs()->updateExistingPivot($request->staffID,$mahir_price);
           return redirect()->back();
   }
    public function changeStatusOrder(Request $request,$id){
//        dd($request);
        $order = Order::find($request->order);
//        dd($order);
        $staff =$order->staffs;
//        $staff = Staff::find($request->staffID);
        if (is_null($staff)){
            toastr()->error('Something Went wrong');
        }
//        dd($staff);
       $input =$request->only('status');
//        dd($order->staffs()->updateExistingPivot($staff,$input));
        $order->staffs()->updateExistingPivot($staff,$input);
        toastr()->success('Status successfully changed!');

        return redirect()->back();
//       $input =$request->only('order_price');
//       $update = $order->update($input);
//       $update =$staffs->update($mahir_price);
//

   }

}
