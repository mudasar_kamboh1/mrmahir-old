<?php

namespace App\Http\Controllers\Web;

use App\Area;
use App\Category;
use App\Facebook;
use App\Gallery;
use App\Service;
use App\SubCategory;
use App\SubService;
use App\TrendingService;
use App\User;
use function foo\func;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{

    public function home(){
        $services = Service::get();
        $trendingServices = TrendingService::with('SubServices')->get();
        $areas = Area::get();
        $videos = Gallery::whereHas('sub_category.category', function ($query) {
            $query->where('slug', '=', 'youtube');
        })->get();

        $posts = Gallery::whereHas('sub_category.category', function ($query) {
            $query->where('slug', '=', 'banner');
        })->orderBy('sort','asc')->orderBy('created_at','desc')->get();
        //trending services
       $sub_services = SubService::with('service')->get();
        $meta_title ="Mr.Mahir| home Repair &maintenance";
        return view('web.layouts.home', compact('services','areas','videos','trendingServices','sub_services','posts','meta_title'));
    }


    public function getSubServices($id){
        $service = Service::find($id);
        if(!is_null($service)){
            $subServices = $service->subServices()->select('id','title')->get();
            return response()->json([
                'status' => true,
                'msg' => 'Service Successfully Retrieved!',
                'data' => $subServices
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Service Not Found!'
            ]);
        }
    }

    public function serviceDetail($slug){
        $Facebook_review = Facebook::get();
        $service = Service::with('subServices')->whereSlug($slug)->first();
        if(!is_null($service)){
            return view('web.layouts.serviceDetail', compact('service','Facebook_review'));
        }else{
            toastr()->error('Service Not Found!');
            return redirect('services');
        }
    }


    public function rateList(){
        $services = Service::with('subServices')->limit(4)->get();
        $Facebook_review = Facebook::get();
        return view('web.layouts.rate-list',compact('services','Facebook_review'));
    }

    public function aboutUs(){
        $Facebook_review = Facebook::get();
        $services = Service::with('subServices')->limit(4)->get();

        return view('web.layouts.about-us',compact('services','Facebook_review'));
    }
    public function terms(){
        return view('web.layouts.terms');
    }

    public function portfolio(){
        $Facebook_review = Facebook::get();
        $posts = Gallery::whereHas('sub_category.category', function ($query) {
                $query->where('slug', '=', 'gallery');
        })->orderBy('created_at','desc')->get();

        $categories = SubCategory::whereHas('category',function ($q){
            $q->where('slug', '=', 'gallery');
        })->get();
        $services = Service::with('subServices')->limit(4)->get();
        return view('web.layouts.portfolio',compact('categories','posts','services','Facebook_review'));
    }

    public function profile(){

        return view('web.layouts.profile');
    }

    public function orderNow(Request $request){
        $sub_services = [];
        if($request->get('service')){
            $service = Service::whereSlug($request->get('service'))->first();
            if(!is_null($service)){
                $sub_services = $service->subServices;
            }
        }
        $services = Service::get();
        $areas = Area::get();
        return view('web.layouts.order-now', compact('services','areas','sub_services'));
    }

    public function services($slug = null){
        $services = Service::get();
//        dd($services->toArray());
        if(!is_null($slug)){
            $sub_services = SubService::whereHas('service',function ($q) use ($slug) {
                $q->where('slug', $slug);
            })->get();
        }else{
            $sub_services = SubService::get();
        }
        $Facebook_review = Facebook::get();
        $meta_service=Service::whereSlug($slug)->first();
//        dd($meta_service);

        return view('web.layouts.services', compact('services','sub_services','Facebook_review','meta_service'));
    }

    public function email($id)
    {
        $user = Auth::user();
//        dd($user);
//        $user = User::find($id);
        return view('web.layouts.email', compact('user'));
    }


}
