<?php

namespace App\Http\Controllers\Web;

use App\Order;
use App\Otp;
use App\Staff;
use App\SubService;
use App\User;
use  App\Area;
use App\Service;
use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    protected $userRepository;
     public  function  __construct(userRepository $userRepository){
         $this->userRepository =$userRepository;
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $staff = $user->staff;
//        dd($staff);
        $areas = Area::get();
        $services = Service::with('subServices')->get();
        $sub_services= SubService::get();
        return view('web.layouts.profile',compact('user','areas','services','staff','sub_services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $request->validate([
            'name' =>  'nullable|regex:/^[\pL\s\-]+$/u|min:3',
            'password' => 'required|confirmed|min:6',
            'email' => 'nullable|email|unique:users',
            'address' => 'nullable|string',
        ]);

        $user = Auth::user();

        $user = $user->update($request->only('name','password','email','address'));

        if($user){
            return response()->json([
                'status' => true,
                'message' => 'Your Info Successfully Updated!'
            ]);
        }else{
            return response()->json([
                'status' => true,
                'message' => 'Something went wrong!'
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkUserPasswordValid(Request $request){
        $request->validate([
            'mobile' => 'required|exists:users',
            'password' => 'required|min:6',
        ]);

        if (Auth::attempt(['mobile'=> request('mobile'),'password'=> request('password')])){
            //Logged In user
            $user = Auth::user();
            // Order Valid
            $order = $user->orders()->orderBy('created_at','desc')->first();
            $order->update(['confirmed' => true]);

            return response()->json([
                'status' => true,
                'message' => 'Successfully Verified'
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'You entered invalid  Password!'
            ]);
        }

    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' =>  'nullable|regex:/^[\pL\s\-]+$/u|min:3',
            'password' => 'nullable|confirmed|min:6',
            'email' => 'nullable|email',
            'address' => 'nullable|string',
            'card_id'=>'nullable|string',
            'expire_date'=>'nullable|date_format:"m/d/Y"',
            'images'=>'nullable',

        ]);

        $user = Auth::user();
        if($user->hasRole('Staff')) {
            $staff= $user->staff;
//            dd($input);
            $staff->areas()->sync($request->get('areas'));
            $staff->subServices()->sync($request->get('sub_services'));
            $userInfo=$request->only('card_id');
            $userInfo['expire_date'] = Carbon::parse($request->get('expire_date'))->format('Y-m-d');
            $images = [];
            if ( $files=$request->file('images')) {
                if($files=$request->file('images')){
                        foreach($files as $k => $file){
                            $name = time() .$k. '.' . $file->getClientOriginalExtension();
                            $file->move('assets/staff',$name);
                            $images[$k] = $name;
                        }
                    }
                    $userInfo['images'] =implode(',',$images);
            }
            $update =$staff->update($userInfo);
        }
        $input = $request->only('name','email','address');
        if($request->get('password')){
            $input['password'] = $request->get('password');
        }
        $update = $user->update($input);
        if($update){
            toastr()->success('your profile successfully updated!');
        }else{
            toastr()->error('Something went wrong!');
        }
        return back();
    }


    public function forgotPassword(Request $request){
        $this->validate($request, [
            'mobile' =>  'required|exists:users|max:15'
        ]);
        $user = User::where('mobile',$request->mobile)->first();
        if(!is_null($user)){
            $check = $user->otp()->where('updated_at', '>=', Carbon::now()->subMinutes(1)->toDateTimeString())->first();
            if(is_null($check)){
                $code = uniqueOtp(rand(100000, 999999));
                $user->otp()->delete();
                $otp = Otp::create(['code' => $code]);
                $otp->user()->associate($user)->save();
                if($otp){
                    $user->update(['password' => $code]);
                    return response()->json([
                        'message' => 'OTP successfully sent to your mobile!',
                        'status' => true
                    ]);
                }else{
                    return response()->json([
                        'message' => 'Something went wrong. You can try later!',
                        'status' => false
                    ]);
                }
            }else{
                return response()->json([
                    'message' => 'You can try after one minute to resend!',
                    'status' => false
                ]);
            }
        }else{
            return response()->json([
                'message' => 'Invalid Mobile Number!',
                'status' => false
            ]);
        }
    }
    public function getSubServices($id){

        $service = Service::find($id);
        if(!is_null($service)){
            $subServices = $service->subServices()->select('id','title')->get();
//            dd($subServices);
            return response()->json([
                'status' => true,
                'msg' => 'Service Successfully Retrieved!',
                'data' => $subServices
            ]);
        }else{
            return response()->json([
                'status' => false,
                'msg' => 'Service Not Found!'
            ]);
        }
    }
    public function deleteImage(Request $request, $id){
        $user = Auth::user();
        $staff= $user->staff;
        $staffA= $staff->where($id)->where('images', 'like', '%dd%');

        $staffA->delete(['images' => null]);
        return redirect();
    }
    public function staffOrder($id = null)
    {
        $user = Auth::user();
        $staff = $user->staff;
        if(is_null($staff)){
            toastr()->error('Order Not Found!');
            return back();
        }
        $areas = Area::get();
        $services = Service::with('subServices')->get();
        $sub_services= SubService::get();
        $orders =$staff->orders;
//        dd(action('Web\UserController@deleteImage'));
        if($orders->count() == 0) {
            toastr()->error('Order Not Found!');
        }
        return view('web.layouts.staff-order', compact('staff', 'services', 'orders', 'areas', 'user', 'sub_services'));

    }

    public function viewStaff(Request $request,$id)
    {
        $user = Auth::user();
        $staff =$user->staff;
        $orders= $staff->orders;
        $order = Order::find($id);
//        dd($order);
//        $order =$orders->staffs;
//        dd($order);
        if(!is_null($order)){
            return view('web.layouts.staff-detail', compact('order','orders','staff','statuses','sub_services','services','areas'));
        }else{
            return '';
        }
    }

}
