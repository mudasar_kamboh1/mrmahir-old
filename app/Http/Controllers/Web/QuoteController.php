<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\quoteValid;
use foo\bar;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\QuoteModel;
use App\Http\Controllers\Controller;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.layouts.about-us');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(quoteValid $request)
    {
        $input = $request->all();
        $quote = QuoteModel::create($input);
        if($quote){
            return response()->json([
                'status' => true,
                'message' => 'Your call request send successfully.!',
            ]);
        }else {
            return response()->json([
                'status' => false,
                'message' => 'Something went wrong. please refresh and try again!',
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
