<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\TrendingServiceTransformer;
use Illuminate\Http\Request;
use App\Repositories\TrendingServiceRepository;

class TrendingServiceController extends Controller
{
    protected $trendingServiceRepository;

    public function __construct( TrendingServiceRepository $trendingServiceRepository)
    {
        $this->trendingServiceRepository = $trendingServiceRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $trendingService = $this->trendingServiceRepository->get();
//            dd($trendingService);
            $trendingServices = fractal($trendingService,new TrendingServiceTransformer())->parseIncludes('SubServices')->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
//          dd($trendingServices);
            return response()->json([
                'success' => true,
                'message'=>'Service received Successfully',
                'services' => $trendingServices,

            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
