<?php

namespace App\Http\Controllers\Api;

use App\Transformers\AreaTransformer;
use Illuminate\Http\Request;
use App\Transformers\ServiceTransformer;
use App\Repositories\ServiceRepository;
use App\Http\Controllers\Controller;
use App\Service;
use Mockery\Exception;
use Spatie\Fractal\Fractal;

class ServiceController extends Controller
{
    protected $serviceRepository;

    public function __construct( ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( )
    {

        try{
            $allServices = $this->serviceRepository->get();

            $services = fractal($allServices,new ServiceTransformer())->parseIncludes('SubServices')->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
            return response()->json([
                'success' => true,
                'message'=>'Service received Successfully',
                'services' => $services,

            ]);
        }catch (Exception $e){
            return response()->json([
                'success' => false,
            ]);
        }
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function get(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( )
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
