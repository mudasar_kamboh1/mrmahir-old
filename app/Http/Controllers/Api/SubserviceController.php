<?php

namespace App\Http\Controllers\Api;

use App\Service;
use App\Transformers\SubserviceTransformer;
use Illuminate\Http\Request;
use App\Repositories\SubserviceRepository;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class SubServiceController extends Controller
{
    protected $subserviceRepository;

    public function __construct( SubserviceRepository $subserviceRepository)
    {
        $this->subserviceRepository=$subserviceRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
//            $subservices  = $this->subserviceRepository->all();
//            $services = Service::all();
            $service = Service::find($request->service_id);
            $subServices =  fractal($service->subServices, new SubserviceTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());

            return response()->json([
                'success'=>true,
                'message'=>'subService received Successfully',
                'data' => $subServices,

            ]);
        }catch (Exception $e){

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
