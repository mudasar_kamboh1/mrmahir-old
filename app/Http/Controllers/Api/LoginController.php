<?php

namespace App\Http\Controllers\Api;

use App\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function  login (Request $request)
    {
        $this->validate($request, [
            'mobile' => 'required|numeric|exists:users',
            'password' => 'required|min:6',
    ]);
        try{
            if (Auth::attempt(['mobile'=> request('mobile'),'password'=> request('password') ]))
            {
                $user = Auth::user();
//                dd($user);
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                return response()->json([
                    'success' =>true,
                    'message'=>'Successfully Login',
                    'data'=>$success
                ], $this->successStatus);
            }
            else{
                return response()->json(['error'=>'Unauthorised'], 401);
            }
        }catch (Exception $e){
            return response()->json(['error','Something Went Wrong']);
        }
    }

    public function logout(Request $request)
    {
        try{

            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }catch (Exception $e){
            return response()->json(['error','Something went Wrong']);

        }

    }


        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
