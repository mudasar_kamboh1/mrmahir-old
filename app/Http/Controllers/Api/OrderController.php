<?php

namespace App\Http\Controllers\Api;

use App\Area;
use App\Http\Requests\InstanceOrder;
use App\Order;
use League\Fractal\Pagination\PhalconFrameworkPaginatorAdapter;
use Illuminate\Support\Facades\Validator;

use App\Repositories\OrderRepository;
use App\Status;
use App\SubService;
use App\Transformers\OrderTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;

class OrderController extends BaseController
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {

        $this->orderRepository = $orderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::guard('api')->user();
        if($request->get('status_id')){
            $status = $request->status_id;
            $orders = $user->orders()->whereHas('status', function ($query) use ($status){
                $query->whereId($status);
            });
        }else{
            $allOrder =$user->orders;
            $orders = fractal($allOrder,new OrderTransformer())->transformWith(new OrderTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
        }

        return response()->json([
            'success' => true,
            'message' => 'Successfully retrieved orders list',
            'data' => $orders
        ]);
    }

    public function cancelOrder(Request $request, $id)
    {
      try{

         $user = Auth::guard('api')->user();
//         dd($user->orders()->whereId($id)->first());
         $order = $user->orders()->whereId($id)->first();
//          $orders = Order::find($request->order);
//          dd($orders);
         if (!is_null($order)){
             $status = Status::whereTitle('Cancel')->first();
             if (!is_null($status)){
                 if($order->status->title == 'Pending'){
                     $order->status()->associate($status)->save();
                     return response()->json([
                         'status' => true,
                         'message' => 'Order successfully cancelled!'
                     ]);
                 }else{
                     return response()->json([
                         'status' => true,
                         'message' => "You can't be cancel!"
                     ]);
                 }
             }else{
                 return response()->json([
                     'status' => false,
                     'message' => 'Something went wrong!'
                 ]);
             }
         }else{
             return response()->json([
                 'status' => false,
                 'message' => 'Order Not Found!'
             ]);
         }

      }catch (Exception $e){
          return response()->json([
             'status' => false,
             'message' => 'Something went wrong!'
          ]);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(),[
                'subservice_id'=>'required|exists:sub_services,id',
                'area_id'=>'required|exists:areas,id',
                'mobile' => 'required|numeric|digits:11',
                'schedule_datetime' =>'nullable|date_format:"d-m-Y h:iA"',
                'name' =>'nullable|regex:/^[\pL\s\-]+$/u|min:3',
                'email' =>'nullable|email',
                'address'=>'required|string|max:250',
                'problem_message' =>  'required|string',
            ]);

            if ($validator->fails()){
                return $this->sendError('Validation Error.',$validator->errors());
            }
            $user = Auth::guard('api')->user();
            $order = $this->orderRepository->saveOrder($request);
            return response()->json([
                'status' => true,
                'message' => 'Our team will call you soon!',
                'data' => $order
            ]);
        }catch (Exception $e){
            return response()->json([
                'status' => false,
                'message' => 'Something went wrong!'
            ]);
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
           $user = Auth::guard('api')->user();
           $order =Order::findorFail($id);
            if (empty($order)) {

                return $this->sendError('order not found');
            }
            $order->delete();
            return response()->json([
                'data'=>true,
                'message'=>'Order deleted successfully!'
            ]);
       }catch (Exception $e){
           dd($e);
              return $this->sendError('There is something wrong, please try again.');
       }
    }
    public function orderDetail()
    {
        try{
            $user = Auth::guard('api')->user();
            $allOrder =$user->orders;

//            dd($allOrder->whereHas->orderBy('created_at','desc')->get());
            $orders = fractal($allOrder,new OrderTransformer())->transformWith(new OrderTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
            return response()->json([
                'status' => true,
                'message' => 'User orders show detail!',
                'data' => $orders
            ]);
        }catch (Exception $e){
        return response([
           'status'=>false,
            'message'=>'something went wrong',
        ]);
        }
    }
    public function orderUpdate(Request $request,$id){
        $this->validate($request, [
            'address' => 'nullable|max:250',
            'schedule_datetime' =>'nullable|date_format:"d-m-Y h:iA"',
            'problem_message'=>'nullable|string|max:500',
            'mobile'=>'nullable|digits:11'

        ]);
        try{
            $user = Auth::guard('api')->user();
            $order = Order::find($id);
            $order['schedule_datetime']= Carbon::parse($request->schedule_datetime)->format('Y-m-d H:i:s');
            $order->update($request->only('address','problem_message','mobile'));
//            dd($order['schedule_datetime']);
            $orders = fractal($order,new OrderTransformer())->transformWith(new OrderTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());

//            $order = $user->orders()->whereId($id)->update($request->all());
            return response()->json([
                'status' => true,
                'data' => $orders,
                'message' => 'User Updated Successfully!',
            ]);

        }catch (Exception $e){
            return response([
                'status'=>false,
                'message'=>'something went wrong',
                ]);
        }

    }
//    public function orderUpdate(Request $request,$id){
//        $this->validate($request, [
//            'problem_message' => 'required|max:250',
//        ]);
//        try{
//            $user = Auth::guard('api')->user();
//            $order = Order::find($id);
//            $order->update($request->only('problem_message'));
//               return response()->json([
//                'status' => true,
//                   'data' => $order,
//                'message' => 'User updated problem Message!',
//            ]);
//
//        }catch (Exception $e){
//            return response([
//                'status'=>false,
//                'message'=>'something went wrong',
//            ]);
//        }
//
//    }

 public function trendingOrder(Request $request){
     try {
         $validator = Validator::make($request->all(),[
             'subservice_id'=>'required|exists:sub_services,id',
             'area_id'=>'required|exists:areas,id',
             'schedule_datetime' =>'nullable|date_format:"d-m-Y h:iA"',
             'name' =>'nullable|regex:/^[\pL\s\-]+$/u|min:3',
             'email' =>'nullable|email',
             'mobile'=>'nullable|digits:11',
             'address'=>'nullable|string|max:250',
             'problem_message'=>'nullable|string',
         ]);

         if ($validator->fails()){
             return $this->sendError('Validation Error.',$validator->errors());
         }
         $user = Auth::guard('api')->user();
         $order = $this->orderRepository->saveOrder($request);
         return response()->json([
             'status' => true,
             'message' => 'Our team will call you soon!',
             'data' => $order
         ]);
     }catch (Exception $e){
         return response()->json([
             'status' => false,
             'message' => 'Something went wrong!'
         ]);
     }
 }


}
