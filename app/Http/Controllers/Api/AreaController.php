<?php

namespace App\Http\Controllers\Api;
use App\Transformers\AreaTransformer;
use Illuminate\Http\Request;
use App\Repositories\AreaRepository;
use Mockery\Exception;


class AreaController extends BaseController
{
    protected $areaRepository;
     public function __construct( AreaRepository $areaRepository)
     {
         $this->areaRepository= $areaRepository;
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $areas = $this->areaRepository->all();
            $areas = fractal($areas, new AreaTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());

            return response()->json([
                'success' => true,
                'message'=>'Area received Successfully',
                'data' => $areas,
            ]);
        }catch (Exception $e){
            return response()->json(['error','area not found']);
        }

//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      try{

          $area = $this->areaRepository->find($id);
          return response()->json(['success',' area show successfully']);
      }catch (Exception $e){
          return response()->json(['error','are\ not found']);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
