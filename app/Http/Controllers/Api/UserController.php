<?php

namespace App\Http\Controllers\Api;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Transformers\UserTransformer;
use Mockery\Exception;

class UserController extends BaseController
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function details(Request $request)
    {
        try{

            $user = Auth::guard('api')->user();
            $data = fractal($user, new UserTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
            return response()->json([
                'status'=> true,
                'message'=> 'Successfully Retrieved!',
                'data' => $data
            ],$this->successStatus);

        }catch (Exception $e){
            return response()->json([
                'status'=>false,
                'message'=>'Something went wrong',
            ]);
        }


    }


    public function updateProfile(Request $request)
    {

        try{

            $validator = Validator::make($request->all(),[
                'name' => 'nullable|min:3|max:50',
                'email' => 'nullable|email|:users',
                'address'=>'nullable|string|max:250',
//                'mobile' => 'required|digits:11',
                'password' => 'nullable|min:6|max:16',
            ]);

            if ($validator->fails()){
                return $this->sendError('Validation Error.',$validator->errors());
            }
            $user = Auth::guard('api')->user();
            $userShow = fractal($user, new UserTransformer())->serializeWith(new \Spatie\Fractalistic\ArraySerializer());
            $input =$request->only('name','email','address');
            if ($request->get('password')) {
                $input['password'] = $request->get('password');
            }
            $update =$user->update($input);
            return response()->json([
                'status'=> true,
                'message'=> 'your profile successfully updated',
                'data' => $userShow
            ],$this->successStatus);

        }catch (Exception $e){
            return response()->json([
                'status'=>false,
                'message'=>'Something went wrong',
                ]);
        }

    }
}
