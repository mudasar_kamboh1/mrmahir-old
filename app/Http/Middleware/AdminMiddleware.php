<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user =User::find(Auth::user()->id);
            if (!$user->hasRole('Admin')){
            toastr()->warning('You do not have permission to access!');
            return redirect('/home');
            }


        }
        return $next($request);
    }
}
