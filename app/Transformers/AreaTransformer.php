<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

use App\Area;

/**
 * Class AreaTransformer.
 *
 * @package namespace App\Transformers;
 */
class AreaTransformer extends TransformerAbstract
{
    /**
     * Transform the Area entity.
     *
     * @param \App\Area $model
     *
     * @return array
     */
    protected $availableIncludes = [];

    public function transform(Area $area)
    {

        return [
            'id' => (int) $area->id,
            'name'=> $area->name
        ];
    }
}
