<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Order;

class OrderTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Order $order)
    {

        return [
            'id' => (int) $order->id,
            'token'=>$order->token,
            'address'=>$order->address,
            'problem_message'=>$order->problem_message,
            'mobile'=>$order->mobile,
            'schedule_datetime'=>$order->schedule_datetime,
            'area_name'=>$order->area ? $order->area->name : null,
            'title_service'=>$order->subService && $order->subService->service  ? $order->subService->service->title:null,
            'title'=>$order->subService ? $order->subService->title: null,
            'status_id'=>$order->status ? $order->status->title:null,
            'created_at'=> $order->created_at ? Carbon::parse($order->created_at)->format('h:s A Y-m-d') : '-- -- --',
        ];
    }
    public function includeArea(Order $order)
    {
        $order = $order->area;
        if (!is_null($order)) {
            return $this->item($order, new OrderTransformer(), 'area_id');
        } else {
            return $this->collection($this->null(), new OrderTransformer());
        }
    }


}
