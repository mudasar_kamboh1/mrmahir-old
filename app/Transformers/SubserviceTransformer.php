<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Subservice;

/**
 * Class SubserviceTransformer.
 *
 * @package namespace App\Transformers;
 */
class SubserviceTransformer extends TransformerAbstract
{
    /**
     * Transform the Subservice entity.
     *
     * @param \App\Subservice $model
     *
     * @return array
     */
    public function transform(Subservice $subservice)
    {
        $image  = $subservice ? $subservice->image : null;
        $path = public_path('thumbnailImg').'/'.$image;
//        dd($image);
        return [
            'id'   => (int) $subservice->id,
            'title'=>$subservice->title,
            'price'=>$subservice->price,
            'price_comment'=>$subservice->price_comment,
            'image'=> file_exists($path) && !is_null($image) ? base64_encode(file_get_contents($path)) : '',

        ];

    }
}
