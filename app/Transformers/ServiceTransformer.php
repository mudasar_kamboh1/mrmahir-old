<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Service;

/**
 * Class ServiceTransformer.
 *
 * @package namespace App\Transformers;
 */

class ServiceTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['SubServices'];
    /**
     * Transform the Service entity.
     *
     * @param \App\Service $model
     *
     * @return array
     */
    public function transform(Service $service)
    {
        $image  = $service ? $service->icon : null;
        $path = public_path('serviceIcon').'/'.$image;
        return [
            'id' => (int) $service->id,
            'title' => $service->title,
//            'icon'=> base64_encode(file_get_contents(public_path().'/serviceIcon/'.$service->icon)),
            'icon'=> file_exists($path) && !is_null($image) ? base64_encode(file_get_contents($path)) : '',

//            'image'=> base64_encode(file_get_contents(public_path().'/serviceImg/'.$service->image))

        ];
    }

    public function includeSubServices(Service $service)
    {
        return $this->collection($service->subServices, new SubserviceTransformer());
    }
}
