<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the User entity.
     *
     * @param \App\User $model
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'  => (int) $user->id,
            'name'=>$user->name,
            'email'=>$user->email,
            'mobile'=>$user->mobile,
            'address'=>$user->address,


            /* place your other model properties here */

        ];
    }
}
