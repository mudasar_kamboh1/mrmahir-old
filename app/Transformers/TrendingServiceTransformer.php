<?php

namespace App\Transformers;
use App\TrendingService;
use App\Service;
use App\SubService;
use League\Fractal\TransformerAbstract;

class TrendingServiceTransformer extends TransformerAbstract
{
//    protected $availableIncludes = ['SubServices'];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TrendingService $trendingService)
    {
        $image  = $trendingService->SubServices->service ? $trendingService->SubServices->service->icon : null;
        $path = public_path('serviceIcon').'/'.$image;
//        dd($path);
        return [
            'subservice_id'=>$trendingService->SubServices() ?$trendingService->SubServices->id: null ,
            'TrendingService_title'=>$trendingService->SubServices ? $trendingService->SubServices->title : null,
            'price'=>$trendingService->SubServices ? $trendingService->SubServices->price : null,
              'price_comment'=> $trendingService->SubServices ? $trendingService->SubServices->price_comment : null,
           'image'=> file_exists($path) && !is_null($image) ? base64_encode(file_get_contents($path)) : '',
        ];
    }
    public function includeSubServices(TrendingService $trendingService)
    {
        return $this->collection($trendingService->subServices, new SubserviceTransformer());
    }
}
