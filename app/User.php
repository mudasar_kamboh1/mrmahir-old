<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // For Default Password Encryption
    Public function setPasswordAttribute($password) {
        return $this->attributes['password'] = bcrypt($password);
    }


    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function staff()
    {
        return $this->hasOne('App\Staff','user_id');
    }

    public function otp(){
        return $this->hasMany('App\Otp');
    }


}
