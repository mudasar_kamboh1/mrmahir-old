<?php

namespace App;
use App\User;
use Prettus\Repository\Contracts\Transformable;
use App\Repositories\Eloquent\OrderRepositoryEloquent;
use Prettus\Repository\Traits\TransformableTrait;



use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    public function  user()
    {
        return $this->belongsTo('App\User');
    }
    public function area()
     {
       return $this->belongsTo('App\Area','area_id');
     }
    public function subService()
    {
        return $this->belongsTo('App\SubService','subservice_id');
    }
    public  function status()
    {
       return $this->belongsTo('App\Status');
    }
    public function staffs()
    {
        return $this->belongsToMany('App\Staff','order_staff','order_id')->withPivot('staff_id','order_id','mahir_price','staff_price','done_date','assign_date','mahir_status','staff_status','status','rating','duration');
    }


}
