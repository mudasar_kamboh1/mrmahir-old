<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected  $table ='services';
    protected $fillable =
        [
         'title','image','icon','slug','description','image_alt','icon_alt','meta_title','meta_description'
        ];

    public function staffs()
    {
        return $this->belongsToMany('App\Staff','service_staffs');
    }
    public function area()
    {
        return $this->belongsToMany('App\Service');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    public function subServices()
    {
        return $this->hasMany('App\SubService','service_id');
    }

}
