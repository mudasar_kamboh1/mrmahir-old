<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrendingService extends Model
{
    protected $guarded = [];

    public function SubServices()
    {
        return $this->belongsTo('App\SubService','subService_id');
    }
}
