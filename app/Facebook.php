<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facebook extends Model
{
    protected $table = 'facebooks';
    protected $fillable = [
        'name','image','rating','description','alt'
    ];


}
