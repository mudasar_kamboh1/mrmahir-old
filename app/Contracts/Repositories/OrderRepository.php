<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface OrderRepository extends RepositoryInterface
{
    //
}
