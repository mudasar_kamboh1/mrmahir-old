<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FacebookRepositoryRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface FacebookRepositoryRepository extends RepositoryInterface
{
    //
}
