<?php

namespace App\Contracts\Repositories\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AreaRepository.
 *
 * @package namespace App\Contracts\Repositories\Repositories;
 */
interface AreaRepository extends RepositoryInterface
{
    //
}
