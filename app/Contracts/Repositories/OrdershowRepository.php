<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdershowRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface OrdershowRepository extends RepositoryInterface
{
    //
}
