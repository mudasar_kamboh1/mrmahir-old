<?php
/**
 * Created by PhpStorm.
 * User: Ch Walayat Khan
 * Date: 22/02/2019
 * Time: 12:09
 */

namespace App\Repositories;


use App\Facebook;
use Illuminate\Http\Request;
use Prettus\Repository\Eloquent\BaseRepository;

class FacebookRepository extends BaseRepository

{

    function model()
    {
        return "App\Facebook";

    }
    public function storeService(Request $request)
    {
        try{

            $input = $request->all();
            if ($request->hasFile('image')) {
                $input['image'] = imageUpload($request->file('image'), public_path('/fbImages'));
            }
//            dd(imageUpload($request->file('image'),public_path('/fbImages')) );

            $facebook = Facebook::create($input);
            return true;
        }catch (Exception $e){
            return false;
        }

    }
    public function destroyService($id)
    {

        $service =Facebook::all();

        if ($service){
            $service = Facebook::findorFail($id);
            $images = [
                $image_path = public_path().'/fbImages/'.$service->image,
            ];
            foreach ($images as $image){
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $service = Facebook::destroy($id);

            toastr()->success('Service Deleted Successfully!');
            return redirect()->back();
        }else{
            toastr()->error('Something Went Wrong!');
        }
        return redirect()->back();

    }
}