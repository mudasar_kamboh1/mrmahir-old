<?php
/**
 * Created by PhpStorm.
 * User: Ch Walayat Khan
 * Date: 22/02/2019
 * Time: 12:09
 */

namespace App\Repositories;


use Prettus\Repository\Eloquent\BaseRepository;

class AreaRepository extends BaseRepository

{

    function model()
    {
      return "App\Area";

    }

}