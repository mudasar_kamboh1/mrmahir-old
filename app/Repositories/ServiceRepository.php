<?php

namespace App\Repositories;

use App\Service;
use Illuminate\Http\Request;
use Mockery\Exception;
use Prettus\Repository\Eloquent\BaseRepository;

class ServiceRepository extends BaseRepository {

    function model()
    {
        return 'App\Service';
    }

//    public function getOrderByStatus(){
//        return $this->paginate('2');
//    }

    public function storeService(Request $request)
    {
       try{

           $input = $request->all();
           if ($request->hasFile('image')) {

               $input['image'] = imageUpload($request->file('image'), public_path('/serviceImg'));
           }
           if ($request->hasFile('icon')){
               $input ['icon'] = imageUpload($request->file('icon'), public_path('/serviceIcon'));
           }
           $input['slug'] = makeSlug($input['title'], new Service());
           $service =Service::create($input);
           return true;
       }catch (Exception $e){
           return false;
       }

    }
    public function destroyService($id)
    {

        $service =Service::all();
        if ($service){
            $service = Service::findorFail($id);
            $images = [
                $image_path = public_path().'/serviceImg/'.$service->image,
                $icon_path = public_path().'/serviceIcon/'.$service->icon,
            ];
            foreach ($images as $image){
                if (file_exists($image)){
                    unlink($image);
                }
            }
            $service = Service::destroy($id);
            toastr()->success('Service Deleted Successfully!');
            return redirect()->back();
        }else{
            toastr()->error('Something Went Wrong!');
        }
        return redirect()->back();

    }

}