<?php

namespace App\Repositories;

use App\Area;
use App\Order;
use App\Otp;
use App\Service;
use App\Status;
use App\SubService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderRepository extends BaseRepository {

    function model()
    {
        return "App\Order";
    }

    function modelObj() {
        return $this->model;
    }

    public function getOrderByStatus(){
        return $this->paginate(10);
    }

    public function saveInstanceOrder($request){
        try {
            $input = $request->only('mobile','name','email');
            $order = [];
//            dd($order);
            $order['confirmed'] = 1;
            if(Auth::check()){
                $user = Auth::user();
                $order['mobile'] = $request->mobile;
            }else{
                $user = User::whereMobile($request->mobile)->first();
                //If user doesn't exist
                if (is_null($user)) {
                    $password = rand(100000, 999999);
                    $input['password'] = $password;
                    $input['password_hint'] = encrypt($password);
                    $user = User::create($input);
                    $user->assignRole('User');
                }else{
                    $order['mobile'] = $request->mobile;
                }
            }

            $area_id = $request->area;
            $subServiceID = $request->subService;
            $order['token'] = uniqueOrderToken(rand(100000, 999999));
            if($request->schedule_datetime && $request->problem_message){
                $order['problem_message'] = $request->problem_message;
                $order['address'] = $request->address;
                $order['schedule_datetime'] = Carbon::parse($request->schedule_datetime)->format('Y-m-d H:i:s');
            }
            $order = $this->create($order);
            $order->user()->associate($user)->save();

            //Associating Area with Order
            if ($area_id) {
                $area = Area::find($area_id);
                $order->area()->associate($area)->save();
            }

            //Associating Sub Service with Order
            if ($subServiceID) {
                $subService = SubService::find($subServiceID);
                $order->subService()->associate($subService)->save();

            }

            //Associating Status with Order
            $status = Status::first();
            $order->status()->associate($status)->save();

            return true;

        }catch (\Exception $e){
            return false;
        }
    }
    public function saveOrder($request){
        try {
            $input = $request->only('mobile','name','email');
            $order = [];
//            dd($order);
            $order['confirmed'] = 1;
            if(Auth::check()){
                $user = Auth::user();
                $order['mobile'] = $request->mobile;
            }else{
                $user = User::whereMobile($request->mobile)->first();
                //If user doesn't exist
                if (is_null($user)) {
                    $password = rand(100000, 999999);
                    $input['password'] = $password;
                    $input['password_hint'] = encrypt($password);
                    $user = User::create($input);
                    $user->assignRole('User');
                }else{
                    $order['mobile'] = $request->mobile;
                }
            }

            $area_id = $request->area_id;
            $subServiceID = $request->subservice_id;
            $order['token'] = uniqueOrderToken(rand(100000, 999999));
            $order['address'] = $request->address;
            $order['problem_message'] = $request->problem_message;
//            $order['schedule_datetime'] = Carbon::parse($request->schedule_datetime)->format('datetime:Y-m-d H:00 ');
             $order['schedule_datetime']= Carbon::parse($request->schedule_datetime)->format('Y-m-d H:i:s');
//             dd($order['schedule_datetime']);

            $order = $this->create($order);
            $order->user()->associate($user)->save();

            //Associating Area with Order
            if ($area_id) {
                $area = Area::find($request->get('area_id'));
                $order->area()->associate($area)->save();
            }

            //Associating Sub Service with Order
            if ($subServiceID) {
                $subService = SubService::find($request->get('subservice_id'));
                $order->subService()->associate($subService)->save();

            }

            //Associating Status with Order
            $status = Status::first();
            $order->status()->associate($status)->save();

            return true;

        }catch (\Exception $e){
            return false;
        }
    }

}