<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteModel extends Model
{
    protected $table ='quotes';
    protected  $fillable = [
      'name','mobile'
    ];
}
