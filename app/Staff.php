<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
         protected $table ='staff';


         protected $fillable =[
           'user_id','expire_date','card_id','images'
         ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
//    public function services()
//    {
//        return $this->belongsToMany('App\Service','service_staffs');
//    }
    public function subServices()
    {
        return $this->belongsToMany('App\SubService','service_staffs','staff_id')->withPivot('staff_id','sub_service_id');
    }
    public function areas()
    {
        return $this->belongsToMany('App\Area','area_staff')->withPivot('area_id','staff_id');
    }
    public function orders()
    {
        return$this->belongsToMany('App\Order','order_staff','staff_id')->withPivot('staff_id','order_id','mahir_price','staff_price','assign_date','done_date','mahir_status','staff_status','status','rating','duration')->withTimestamps();
    }


}
