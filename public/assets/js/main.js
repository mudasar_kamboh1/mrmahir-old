/*********************************************************************************

	Template Name: Karigor - Minimalist Bootstrap4 Portfolio Template
	Description: A perfect minimal template to build beautiful and unique portfolio websites. It comes with nice and clean design.
	Version: 1.0

	Note: This is main js.

**********************************************************************************/

/**************************************************************
	
	STYLESHEET INDEXING
	|
	|
	|___ Sticky Header
	|___ Testimonial Slider Active
	|___ Bootstrap4 Tooltip Active
	|___ Portfolio Filter & Popup Active
	|___ Header Menu Effect
	|___ Mobile Menu
	|___ Boxed Layout
	|___ Counter Active
	|___ Radial Progress
	|___ Blog Item Height
	|___ Sticky Sidebar
	|
	|
	|___ END STYLESHEET INDEXING

***************************************************************/


(function ($) {
	'use strict';



	


	









	/* Portfolio Filter & Popup Active */
	function portfolioFilterLightgallery(){
		var $gallery = $('.portfolios-wrapper');
		var $boxes = $('.portfolio-item');
		$boxes.hide();

		$gallery.imagesLoaded({
			background: true
		}, function () {
			$boxes.fadeIn();
			$gallery.isotope({
				itemSelector: '.portfolio-item',
				layoutMode: 'masonry',
				masonry: {
					columnWidth: 1,
				}
			});
		});

		$('.portfolio-filters button').on('click', function () {
			
			var filterValue = $(this).attr('data-filter');
			$gallery.isotope({
				filter: filterValue
			});
			$gallery.data('lightGallery').destroy(true);
			$('.portfolios-wrapper').lightGallery({
				selector: filterValue.replace('*', '') + ' .portfolio-zoom-button',
			});

			$('.portfolio-filters button').removeClass('is-checked');
			$(this).addClass('is-checked');

		});

		$('.portfolios-wrapper').lightGallery({
			selector: '.portfolio-item .portfolio-zoom-button'
		});
	}
	portfolioFilterLightgallery();




	



	



	




	


	




})(jQuery);